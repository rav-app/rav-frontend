
[ -n "$1" ] && [ "$1" = "clean" ] && shift && echo "cleaning existing dependencies..." && rm -rf ~/.m2/repository/org/iotc/rav

green=`tput setaf 2`
reset=`tput sgr0`

printf "${green}loading cli...\\r" && mvn initialize -Pinstall-dev-runtime -q $@ && printf "${green}loading cli...done.\n"

source runtime/dev/cli.sh

mvn clean -q
