import fs from 'fs';
import { dirname } from 'path';
import { fileURLToPath } from 'url';


const enabledLocales = ['en-US', 'fr'];
const filePath = `${ dirname(fileURLToPath(import.meta.url)) }/node_modules/date-fns/esm/locale/index.js`;
const data = fs.readFileSync(filePath, 'utf-8');
const rows = data.split('\n');
const filtered = rows.filter(row => !!enabledLocales.find(e => row.includes(`"./${ e }/`)));

fs.writeFileSync(filePath, filtered.join('\n'));

//console.info('[+]DATE-FNS PATCHED![+]');


/*
const modduleLocalesPaths = [
//  '/date-fns/locale', '/antd/es/locale', '/antd/es/locale-provider', '/antd/lib/locale', '/antd/lib/locale-provider', '/rc-picker/lib/locale', '/rc-picker/es/locale'
'/date-fns/locale', '/date-fns/esm/locale'
];
const libDeps = ['_lib', 'style', 'default.js', 'context.js', 'index.js', 'LocaleReceiver.js'];
const rmOpts = { recursive: true, force: true };

modduleLocalesPaths.forEach(async m => {
  const dir = `${ dirname(fileURLToPath(import.meta.url)) }/node_modules${ m }`;
  const files = await fs.readdir(dir);

  if (m.includes('/antd/')) {
    files.forEach(f =>
      enabledLocales.includes(f)  ||
      libDeps.includes(f)         ||
      fs.unlink(`${ dir }/${ f }`)
    );
  } else {
    files.forEach(f =>
      enabledLocales.includes(f)  ||
      f.includes('.')             ||
      libDeps.includes(f)         ||
      fs.rm(`${ dir }/${ f }`, rmOpts)
    );
  }
});
*/