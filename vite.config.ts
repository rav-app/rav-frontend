import react from '@vitejs/plugin-react'
import { readdirSync } from 'fs'
import { PluginOption, defineConfig } from 'vite'
import checker from 'vite-plugin-checker'

const libs = `${__dirname}/src/lib/`
const proxy = { target: 'https://localhost:8443', changeOrigin: true, secure: false, headers: { "Connection": "keep-alive" } }
const wsproxy = { target: proxy.target, ws: true, changeOrigin: true, secure: false }

const autochunked = ['date-fns', 'react-quill', 'quill', 'xlsx', 'react-color']

const deferCss = (): PluginOption => ({
  name: "defer-css",
  apply: 'build',
  transformIndexHtml: html => html.replace(/<link rel="stylesheet"/g, `<link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'"`)
})

export default defineConfig(({ mode }) => {

  const proxycfg = {

    '/fe/config.json': proxy,
    '/domain': proxy,
    '/admin': proxy,
    '/testmail': proxy,
    '/oauth2': proxy,
    '/export': proxy,
    '/event': wsproxy
  }

 
  // this is to support yarn serve, where we want in-cluster configs.
  // during development, we want configs in public folder, with the latest changes.
  if (mode === 'production')
    proxycfg['/locale'] = proxy


  return {

    base: "/fe/",

    server: {

      port: 3000,
      host: true,

      proxy: proxycfg

    },

    resolve: {
      preserveSymlinks: true,
      alias: [
        ...readdirSync(libs).map(name => ({ find: name, replacement: `${libs}/${name}` })),
        { find: /^#(.*)$/, replacement: `${__dirname}/src/$1` },

      ]
    }

    ,

    build: {

      sourcemap: true,
      rollupOptions: {

        output: {

          manualChunks: id => {

            if (autochunked.some(c => id.includes(c)))
              return

            if (id.includes('nats.ws'))
              return 'natsws'

            if (id.includes('node_modules'))
              return 'vendor'

          }

        }
      }
    },

    plugins: [

      deferCss(),

      react(),

      checker({
        overlay: {
          initialIsOpen: false,
          position: 'br'
        },
        typescript: true,
        eslint: {
          lintCommand: 'eslint "src/**/*{.ts,.tsx}"'
        }
      })
    ]
  }
})
