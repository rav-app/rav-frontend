import { authorizationType } from '#authorization/constants'
import { detailsType } from '#details/constants'
import { SlotType } from '#record/model'
import { IoBoat } from 'react-icons/io5'

export const BaseComboIcon = IoBoat

export const patchedTypes : SlotType[]  = [detailsType,authorizationType]