import { AuthorizationPatch } from '#authorization/model';
import { DetailsPatch } from '#details/model';
import { PhotographsPatch } from '#photographs/model';

export const baseComboType = 'base'

export type BaseComboPatch = DetailsPatch & AuthorizationPatch & PhotographsPatch
