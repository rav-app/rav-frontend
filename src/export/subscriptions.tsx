import { useT } from 'apprise-frontend-core/intl/language';
import { useLogged } from 'apprise-frontend-iam/login/logged';

import { EventSubscription } from 'apprise-frontend-events/subscriptions';
import { useFeedback } from 'apprise-ui/utils/feedback';
import { useExporter } from './exporter';


import { Tip } from 'apprise-ui/tooltip/tip';
import { AllertIcon } from 'apprise-ui/utils/icons';
import { ReportStatus } from './model';

type ExportCompletedEvent = ReportStatus

type ExportFailureEvent = {

    user: string
    start: string
    error: string
}

export const useExportSubscriptions = (): EventSubscription[] => {

    const t = useT()
    const fb = useFeedback()
    const logged = useLogged()
    const exporter = useExporter()

    return [{

        topic: 'export_success',
        onEvent: (ev: ExportCompletedEvent) => {

            exporter.updateStats(ev)

            if (logged.username === ev.user)
                fb.showNotification(t('appsettings.export_trigger_done'), { duration: false })


        }
    },

    {
        topic: 'export_failure',
        onEvent: (ev: ExportFailureEvent) => {

            if (logged.username === ev.user)
                fb.showNotification(t('appsettings.export_general_error'), {

                    icon: <Tip style={{ zIndex: 10000 }} tipType='error' tipPlacement='left' tip={ev.error}>
                        <AllertIcon />
                    </Tip>,
                    duration: false
                })

        }

    }]

}