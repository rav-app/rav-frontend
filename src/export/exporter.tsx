
import { useCall } from 'apprise-frontend-core/client/call'
import { useLogged } from 'apprise-frontend-iam/login/logged'
import { ReportStatus } from './model'


export const exportApi = "/export"

export const useExporter = () => {


    const call = useCall()
    const logged = useLogged()

    const intern = (stats: ReportStatus) => {

        return {

            lastRefreshed: stats.start

        }

    }

    const self = {

        trigger: (mode:'all'|'current') => call.atPath(exportApi).post<{ status: number }>({ user: logged.username, mode }, { raw: true })

        ,

        fetchStats: () => call.atPath(`${exportApi}/status`).get<ReportStatus>().then(intern)

        ,

        updateStats: (_: ReportStatus) => undefined

    }

    return self;
}
