import { ModuleInitialiser } from '#modules/initialiser'
import { Records } from '#record/records'
import { RavScaffold } from '#scaffold'
import { Submissions } from '#submission/submissions'
import { Users } from 'apprise-frontend-iam/user/users'
import { Mail } from 'apprise-frontend-mail/mail'
import { ParseProvider } from 'apprise-frontend-parse/context'
import { Streams } from 'apprise-frontend-streams/streams'


export default function RavLogged() {

    return <Users>
        <Streams>
            <Mail>
                <ModuleInitialiser>
                    <Records>
                        <Submissions>
                            <ParseProvider>
                                <RavScaffold />
                            </ParseProvider>
                        </Submissions>
                    </Records>
                </ModuleInitialiser>
            </Mail>
        </Streams>
    </Users>
}