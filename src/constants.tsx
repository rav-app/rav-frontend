import { IoMdBoat } from "react-icons/io"
import { RiHome7Line } from "react-icons/ri"

export const appColor = '#00ADBA'

export const AppIcon = IoMdBoat

export const HomeIcon = RiHome7Line
