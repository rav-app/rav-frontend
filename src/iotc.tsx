import { useLanguage } from 'apprise-frontend-core/intl/language'



export const IOTCLogo = <img alt="iotc" src={new URL('/images/iotc.png', import.meta.url).href } height={22} />


export const IOTCLink = () => {

    const currentLanguage = useLanguage().current()

    const link = `https://www.iotc.org${currentLanguage === 'en' ? '' : `/${currentLanguage}`}`

    return <a href={link} target='_blank' rel="noreferrer">
            {IOTCLogo}
        </a>
}