import { useRavRecordMocks } from '#record/mockery'
import { useSubmissionMocks } from '#submission/mockery'
import { MockHorizon } from 'apprise-frontend-core/client/mockhorizon'
import { useClientSession } from 'apprise-frontend-core/client/session'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { useRavSettings } from './settings'
import { useRavTags } from './tags'
import { useRavTenants } from './tenants'
import { useRavUsers } from './users'


export const Stager = (props: React.PropsWithChildren) => {

    const session = useClientSession()

    const settings = useRavSettings()
    const tags = useRavTags()

    const tenants = useRavTenants()
    const users = useRavUsers()
    const records = useRavRecordMocks()
    const submissions = useSubmissionMocks()

    const stage = () => {

        if (session.alreadyExists())
            return

        console.log("staging application mocks...")

        settings.stage()
        tags.stage()
        tenants.stage()
        users.stage()

        records.stage()
        submissions.stage()

    }

    const { content } = useRenderGuard({

        render: props.children,
        orRun: stage

    })

    return <MockHorizon>
        {content}
    </MockHorizon>
}

