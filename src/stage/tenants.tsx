import { useMocks } from 'apprise-frontend-core/client/mocks'
import { useTenantMocks } from 'apprise-frontend-iam/tenant/mockery'
import { TenantDto } from 'apprise-frontend-iam/tenant/model'
import { TenantCacheContext } from 'apprise-frontend-iam/tenant/provider'
import { useTagMocks } from 'apprise-frontend-tags/tag/mockery'
import { useContext } from 'react'
import { useRavTags } from './tags'

export const useRavTenants = () => {

    const clientstore = useContext(TenantCacheContext)?.get()

    const mockutils = useMocks()
    const tagmocks = useTagMocks()
    const ravtags = useRavTags()
    const tenantmocks = useTenantMocks()



    const self = {


        stage : () =>{

            clientstore || console.log("staging tenants")

            const tags = tagmocks.tagStore().all().filter(t => t.category === ravtags.categories.tenantype.id)
            
            const randomTenantTags = (u: TenantDto) => ({ ...u, tags: [mockutils.randomIn(tags).id] })
    
            const mockTenants = [
                    
                ...Object.values(tenantmocks.mockTenants()).map(randomTenantTags)
            ,
        
                ...Array.from({ length: 30 }).map((_, i) => tenantmocks.mockTenant(`Party-${i}`)).map(randomTenantTags)
            
            ] 

            tenantmocks.tenantStore().addMany(...clientstore?.all ?? mockTenants)

        }
    }

    return self

}

