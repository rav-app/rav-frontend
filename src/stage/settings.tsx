import { systemSettingsModule } from '#modules/settings'
import { useRavTags } from '#stage/tags'
import { useSettingsMocks } from 'apprise-frontend-streams/settings/mockery'
import { SettingsCacheContext } from 'apprise-frontend-streams/settings/provider'
import { useContext } from 'react'




export const useRavSettings = () => {

    const clientstore = useContext(SettingsCacheContext)?.get()

    const settings = useSettingsMocks()
    const { tags } = useRavTags()


    const stage = () => {

       if (clientstore) 
            settings.replace(clientstore.all)
            
        
        else {

            console.log("staging settings...")


            settings.update(systemSettingsModule.type, {

                primaryScheme: tags.iotcnumber.id,
                secondaryScheme: tags.imo.id,
                approveCycle: ['details'],
                dateFormat: 'short',

            })

        }



    }

    return { stage }

}