import { useMocks } from 'apprise-frontend-core/client/mocks'
import { useTenantMocks } from 'apprise-frontend-iam/tenant/mockery'
import { useUserMocks } from 'apprise-frontend-iam/user/mockery'
import { UserDto } from 'apprise-frontend-iam/user/model'
import { UserCacheContext } from 'apprise-frontend-iam/user/provider'
import { useTagMocks } from 'apprise-frontend-tags/tag/mockery'
import { useContext } from 'react'
import { useRavTags } from './tags'



export const useRavUsers = () => {

    const clientstore = useContext(UserCacheContext)?.get()

    const mockutils = useMocks()
    const tagmocks = useTagMocks()
    const ravtags = useRavTags()
    const usermocks = useUserMocks()
    const tenantMocks = useTenantMocks()
   
   
    const stage = () => {

        clientstore || console.log('staging users...')
        
        const profiletag = mockutils.randomIn( tagmocks.tagStore().all().filter(t => t.category === ravtags.categories.userprofile.id))
        const grouptag = mockutils.randomIn( tagmocks.tagStore().all().filter(t => t.category === ravtags.categories.usergroup.id))
        
        const randomTags = (u: UserDto) => ({ ...u, tags: [profiletag.id, grouptag.id] })

        const randomUsers : UserDto[] = [

            ...Object.values(usermocks.mockCoordinators()).map(randomTags)
    
            ,

            ...tenantMocks.tenantStore().all().flatMap(t => usermocks.mockTeam(t))

        ]
    
        usermocks.userStore().addMany(...clientstore?.all ?? randomUsers)
    }

    return { stage }

}