import { delistingType } from '#delisting/constants';
import { detailsType } from '#details/constants';
import { photographType } from '#photographs/constants';
import { trxauthzType } from '#trxauth/constants';
import { TaggedModule } from 'apprise-frontend-tags/tag/modules';


export const detailsTagModule: TaggedModule = ({

    type: detailsType,
    singular: `details.singular`,
    plural: `details.singular`

})

export const delistingTagModule: TaggedModule = ({

    type: delistingType,
    singular: `delisting.singular`,
    plural: `delisting.plural`

})

export const trxAuthzModule: TaggedModule = ({

    type: trxauthzType,
    singular: `trxauthz.tag_type`,
    plural: `trxauthz.tag_type`

})

export const photographTagModule: TaggedModule = ({

    type: photographType,
    singular: `photograph.singular`,
    plural: `photograph.plural`

})

// export const vessselKindProp = 'vessel-kind'


// export const useCustomPropertiesTypes = () => {

//     const cats = useCategoryStore()

//     const vesselKind = cats.lookupCategory(vesselKindCategory)

//     const vessselKindProperty: CustomProperty<string, SingleTagBoxProps > = {

//         id: vessselKindProp,
//         name: vessselKindProp,

//         Column: () => <TagColumn<Tag> width={200} defaultLayout name="kind" tagOf={t => t.properties.custom?.[vessselKindProp]} render={t => <TagLabel noIcon bare tag={t.properties.custom?.[vessselKindProp] || fishingKindTag} /> }
//                 title={vesselKind?.properties.field?.title ? <Label noIcon bare title={vesselKind?.properties.field?.title} /> : <CategoryLabel noIcon bare category={vesselKind} />} />,

//         Icon: VesselTypeIcon,
  
//         prototype: <TagRefBox category={vesselKind} placeholder={<TagLabel bare tag={fishingKindTag} />} placeholderAsOption
//             label={vesselKind?.properties.field?.title} 
//             msg={vesselKind?.properties.field?.message} 
//             help={vesselKind?.properties.field?.help ?? vesselKind?.description}/>,

//         ValueBox: TagRefBox

//     }

//     return [vessselKindProperty]

// }