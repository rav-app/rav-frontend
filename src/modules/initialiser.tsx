

import { useExportSubscriptions } from '#export/subscriptions'
import { searchSettingsModule, systemSettingsModule, useDevSettings, useValidationSettings } from '#modules/settings'
import { delistingTagModule, detailsTagModule, photographTagModule, trxAuthzModule } from '#modules/tag'
import { tenantPreferencesModule } from '#modules/tenants'
import { submissionActions } from '#submission/oracle'
import { useActionRegistry } from 'apprise-frontend-core/authz/action'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { EventSubscriber } from 'apprise-frontend-events/subscriptions'
import { useIAMSettingsModule } from 'apprise-frontend-iam/settings'
import { useTenantPreferencesModules } from 'apprise-frontend-iam/tenant/modules'
import { tenantTagPropertiesModule } from 'apprise-frontend-iam/tenant/tagprops'
import { useUserPermissionModules, useUserPreferencesModules } from 'apprise-frontend-iam/user/modules'
import { useMailModules } from 'apprise-frontend-mail/modules'
import { mailSettingsModule } from 'apprise-frontend-mail/settings'
import { useSettingsModules } from 'apprise-frontend-streams/settings/api'
import { useCustomTagProperties } from 'apprise-frontend-tags/customprop/api'
import { useTagModules } from 'apprise-frontend-tags/tag/modules'
import { PropsWithChildren } from 'react'
import { ravMailModule } from './mail'
import { useSubmissionPermissionModule, userPreferencesModule } from './users'





export type InitialiserProps = PropsWithChildren

export const ModuleInitialiser = (props: InitialiserProps) => {

    const { children } = props

    const settingsModules = useSettingsModules()

    const devSettingsModule = useDevSettings()
    const validationSettingsModule = useValidationSettings()


    const tagModules = useTagModules()
    const tagProperties = useCustomTagProperties()

    // const customTagPropertyTypes = useCustomPropertiesTypes()

    const tenantPreferencesModules = useTenantPreferencesModules()

    const userPreferencesModules = useUserPreferencesModules()

    const userPermissionModules = useUserPermissionModules()
    const submissionPermissionModule = useSubmissionPermissionModule()

    const actionRegistry = useActionRegistry()

    const exportSubscriptions = useExportSubscriptions()

    const mailModules = useMailModules()

    const iamSettingsModule = useIAMSettingsModule()

    const activate = () => {


        settingsModules.register(systemSettingsModule)
        settingsModules.register(searchSettingsModule)
        settingsModules.register(iamSettingsModule
            
        )
        settingsModules.register(validationSettingsModule)
        settingsModules.register(mailSettingsModule)
        settingsModules.register(devSettingsModule)

        tagModules.register(detailsTagModule)
        tagModules.register(delistingTagModule)
        tagModules.register(photographTagModule)
        tagModules.register(trxAuthzModule)

        tagProperties.register(tenantTagPropertiesModule)
        // tagProperties.register(...customTagPropertyTypes)

        tenantPreferencesModules.register(tenantPreferencesModule)

        userPreferencesModules.register(userPreferencesModule)
        userPermissionModules.register(submissionPermissionModule)

        actionRegistry.register(Object.values(submissionActions))

        mailModules.register(ravMailModule)


    }

    const { content } = useRenderGuard({

        render: <EventSubscriber name='export completion' subscriptions={exportSubscriptions}>
            {children}
        </EventSubscriber>,
        orRun: activate
    })

    return content
}