import { submissionActions, submissionAdminActions } from '#submission/oracle';
import { MailTopic } from 'apprise-frontend-mail/model';
import { MailModule } from 'apprise-frontend-mail/modules';


export const subchangeAllTopic: MailTopic = { key: 'mail.submission_all', value: 'submission', audience: [submissionActions.manage] }
export const subchangePendingTopic: MailTopic = { key: 'mail.submission_pending', value: 'submission.pending', audience: [submissionActions.approve] }
export const subchangeSubmittedTopic: MailTopic = { key: 'mail.submission_submitted', value: 'submission.submitted', audience: [submissionAdminActions.publish] }
export const subchangeRejectedTopic: MailTopic = { key: 'mail.submission_rejected', value: 'submission.rejected' }
export const subchangePublishedTopic: MailTopic = { key: 'mail.submission_published', value: 'submission.published' }
export const autodelistedTopic: MailTopic = { key: 'mail.vessel_autodelisted', value: 'record.autodelist' }

export const ravMailModule: MailModule = {

    name: 'rav',
    topics: [subchangeAllTopic, subchangePendingTopic, subchangeSubmittedTopic, subchangePublishedTopic, subchangeRejectedTopic, autodelistedTopic],

}