import { Asset, AssetReference } from '#submission/model';
import { BytestreamRef } from 'apprise-frontend-streams/model';
import { GenericRecord } from './model';


export type AssetResolver<T extends GenericRecord> = {
    
    resolve: (records: T[], assets: Record<AssetReference,Asset>) => void

    matches: (records: T[]) =>  Record<BytestreamRef, GenericRecord>

    restore: (records: T[], assets: Record<AssetReference,Asset>) => void

}