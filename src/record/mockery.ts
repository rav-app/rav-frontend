import { authorizationType } from '#authorization/constants'
import { useAuthorizationMocks } from '#authorization/mockery'
import { baseComboType } from '#basecombo/mode'
import { detailsType } from '#details/constants'
import { useDetailsMocks } from '#details/mockery'
import { useExtensions } from '#extension/state'
import { systemSettingsModule } from '#modules/settings'
import { photographType } from '#photographs/constants'
import { usePhotographsMocks } from '#photographs/mockery'
import { ResultBatch, SearchDto } from '#search/model'
import { AppSettings } from '#settings/settings'
import { Asset, SubmissionType, submissionTypes } from '#submission/model'
import { VidExtension } from '#vid/ext'
import { useVIDUtils, Vid } from '#vid/model'
import { useMocks } from 'apprise-frontend-core/client/mocks'
import { useService } from 'apprise-frontend-core/client/service'
import { utils } from 'apprise-frontend-core/utils/common'
import { useLifecycleMocks } from 'apprise-frontend-core/utils/lifecycle'
import { mocks } from 'apprise-frontend-core/utils/mock'
import { useTenancyRule, useTenantMocks } from 'apprise-frontend-iam/tenant/mockery'
import { TenantReference } from 'apprise-frontend-iam/tenant/model'
import { useStreamMocks } from 'apprise-frontend-streams/mockery'
import { useSettingsMocks } from 'apprise-frontend-streams/settings/mockery'
import MockAdapter from 'axios-mock-adapter/types'
import { Buffer } from 'buffer'
import addMonths from "date-fns/addMonths"
import getYear from "date-fns/getYear"
import { recordApi } from './calls'
import { recordType } from './constants'
import { GenericRecord, RavRecord, RecordPatch, RecordState, SlotType } from './model'


export const useRecordMockery = () => {

    const service = useService()

    const svc = service.get(recordType)
    const api = `${svc.prefix}${recordApi}`

    const records = useRavRecordMocks()
    const tenants = useTenantMocks()

    const forTenancy = useTenancyRule()

    return (mock: MockAdapter) => {

        console.log("mocking records...")

        mock.onPost(`${api}/search`).reply(({ data }) => {

            const { conditions, cursor } = JSON.parse(data) as SearchDto

            const { page = 1, pageSize } = cursor as any

            const uvis = conditions.find(c => c.ids)?.ids ?? []
            const tenant = conditions.find(c => c.tenant)?.tenant 
            
            
            const results = records.findRecords().current().forAll(...uvis).now().filter(forTenancy).sort()

            //console.log({conditions,uvis,tenant,results})
            
            const start = pageSize ? Math.min(Math.max(0, page - 1) * pageSize, results.length - 1) : 0
            const end = pageSize ? Math.min(start + pageSize, results.length) : results.length

            const batch: ResultBatch = { results: results.slice(start, end), total: results.length }

            const resultUvis = results.map(r => r.uvi)

            // currently, use tenant only to synthesise mock histories for records that we haven't mocked.
            const synthetic = uvis.filter(uvi => !resultUvis.includes(uvi)).map(uvi => {

                const history = records.mockHistory(
                    {
                        historyLength: 3,
                        tenant: tenant ?? mocks.randomIn(Object.values(tenants.mockTenants())).id,
                        uvi
                    })

                records.store().addMany(...history)

                return history[history.length - 1]
            })

            synthetic.length && console.log(`synthesised histories for ${synthetic.length} vessels`)

            return [200, { ...batch, results: [...batch.results, ...synthetic] }]
        })


    }


}


export const boatStream = 'sampleboat'

export const useRavRecordMocks = () => {

    const mocks = useMocks()
    const tenants = useTenantMocks()
    const ext = useExtensions<VidExtension>().get()
    const vid = useVIDUtils()
    const { pastLifecycle } = useLifecycleMocks()

    const settings = useSettingsMocks()
    const details = useDetailsMocks()
    const authorization = useAuthorizationMocks()
    const photographs = usePhotographsMocks()

    const streams = useStreamMocks()

    const self = {


        store: () => mocks.getOrCreateStore<RavRecord>('records', {

            callbacks: {

                onChange: (records => {

                    records.map(r => r.details).map(details.store().add)
                    records.map(r => r.authorization!).filter(t => t!).map(authorization.store().add)

                    // TODO add further slots
                })
            }
        })



        ,

        generate: (props: Partial<{ 
            
            tenantsMin: number, 
            tenantsMax: number,
            recordMin: number,
            recordMax: number,
            historyMin: number,
            historyMax: number
        
        }> = {}) => {

            const { tenantsMin=5, tenantsMax=15, recordMin = 10, recordMax= 20, historyMin=1, historyMax=5} = props

            const sampleTenants = mocks.randomSlice(tenants.tenantStore().all(), tenantsMin, tenantsMax)

            const id = boatStream
            const asset: Asset = {
                id,
                name: `image-${id}`,
                size: mocks.randomNumberBetween(500, 10000),
                type: `image/webp`,
                lifecycle: pastLifecycle(),
                properties: {}
            }

            streams.streamStore().addOrUpdate(asset)
            streams.blobStore().addOrUpdate({
                id: asset.id, data: new Blob([Buffer.from(boatImage, 'base64')], { type: 'image/webp' })
            })

            return sampleTenants.flatMap(t =>

                mocks.randomArray(recordMin, recordMax).flatMap(_ =>

                    self.mockHistory({
                        historyLength: mocks.randomNumberBetween(historyMin, historyMax),
                        tenant: t.id
                    })

                )
            )



        }

        ,

        stage: () => {

            const time = performance.now()

            const records = self.generate()

            self.store().addMany(...records)

            console.log(`staged ${records.length} records in ${performance.now() - time} ms...`)

        }

        ,

        findRecords: () => {

            type Filter = (all: RavRecord[]) => RavRecord[]

            const currentQuery = (filter: Filter) =>

                utils().group([...filter(self.store().all())])
                    .by(r => r.uvi, (u1, u2) => u1.localeCompare(u2))
                    .map(g => g.group.sort((r1, r2) => Date.parse(r2.timestamp) - Date.parse(r1.timestamp))[0])
                    .sort((r1, r2) => Date.parse(r2.timestamp) - Date.parse(r1.timestamp))

            const historicalQuery = (filter: Filter) =>

                filter([...self.store().all()]).sort((r1, r2) => Date.parse(r2.timestamp) - Date.parse(r1.timestamp))


            const apiBasedOn = (basequery: (_: Filter) => RavRecord[]) => {

                const self = (filter: (all: RavRecord[]) => RavRecord[] = t => t) => ({


                    now: () => basequery(filter)

                    ,

                    at: (timestamp: number) => self(all => filter(all).filter(r => new Date(r.timestamp) <= new Date(timestamp)))

                    ,

                    for: (uvi: Vid) => self(all => filter(all).filter(r => r.uvi === uvi))

                    ,

                    forAll: (...uvis: Vid[]) => self(all => uvis.length === 0 ? all : filter(all).filter(r => uvis.includes(r.uvi)))

                    ,

                    forTenant: (tenant: TenantReference) => self(all => filter(all).filter(r => r.tenant === tenant))


                })

                return self
            }


            return { current: apiBasedOn(currentQuery), historical: apiBasedOn(historicalQuery) }

        }


        ,

        mockGeneric: (props: { uvi: Vid, tenant: TenantReference, state: RecordState, patched: SlotType[] }): GenericRecord => {

            const {
                uvi,
                tenant,
                state,
                patched } = props


            const lifecycle = pastLifecycle<RecordState>(state)
            const timestamp = lifecycle.lastModified!

            return {

                id: utils().mint('R'),
                uvi,
                tenant,
                lifecycle,
                timestamp: new Date(timestamp).toISOString(),
                patchedSlots: patched,
                aliases:[]

            }


        }

        ,

        mockVessel: (uvi?: Vid) => {

            const primaryScheme = settings.get<AppSettings>(systemSettingsModule.type).primaryScheme

            return {
                uvi: uvi ?? vid.stringify(primaryScheme, ext.mintPrimaryUvi()),
                name: `Vessel ${mocks.randomNumberBetween(1, 10000)}`
            }

        }


        ,

        // takes a base record - full or generic - and returns a version of it patched "on" a type.
        // optionally, takes also a current record so that slots can change something and leaves other things as they are currently.
        // if omitted, the base double as the current record (used when mocking full records).
        mockPatch: <T extends GenericRecord>(props: { noChange?: boolean, base: T, current?: RavRecord, timestamp: string, name: string, type?: SubmissionType }): T => {

            const { noChange, base, current = base, timestamp, name, type } = props

            const patchType: SubmissionType = type ?? (mocks.randomBoolean(.5) ? baseComboType : mocks.randomIn(submissionTypes))

            let patchedSlots: SlotType[] = [];
            let patched: Partial<Record<SlotType, any>> = {}

            switch (patchType) {

                case baseComboType: patchedSlots = [detailsType, authorizationType, photographType]; break
                default: patchedSlots = [patchType]

            }

            // simulates a patch brings no changes.
            if (noChange)
                patched = patchedSlots.reduce((acc, slot) => ({ ...acc, [slot]: current[slot] }), {})

            else switch (patchType) {

                case detailsType:

                    patched = {
                        [detailsType]: details.mockSlot({ current: current[detailsType], timestamp, tenant: base.tenant, name })
                    }

                    break

                case authorizationType:

                    patched = {
                        [authorizationType]: authorization.mockSlot({ current: current[authorizationType], timestamp })
                    }

                    break


                case photographType:

                    patched = {
                        [photographType]: noChange ? current[photographType] : photographs.mockSlot({ current: current[photographType], timestamp, vid: current.uvi })
                    }

                    break

                case baseComboType:

                    patched = {

                        [detailsType]: details.mockSlot({ current: current[detailsType], timestamp, tenant: base.tenant, name }),
                        [authorizationType]: authorization.mockSlot({ current: current[authorizationType], timestamp }),
                        [photographType]: photographs.mockSlot({ current: current[photographType], timestamp, vid: current.uvi })
                    }

                    if (getYear(Date.parse(timestamp)) <= getYear(Date.parse(current.timestamp)))
                        patched[authorizationType] = current[authorizationType]

                    break


            }

            return {

                ...base,

                id: utils().mint('R')

                ,

                patchedSlots,
                timestamp,    // simulate time has passed
                lifecycle: { ...base.lifecycle, created: Date.parse(timestamp), lastModified: Date.parse(timestamp) }, //align lifecycle

                ...patched
            }
        }

        ,

        mockNewPatchesFor: <T extends SlotType>(props: { tenant: TenantReference, type: T }): RecordPatch<T>[] => {

            const { tenant, type } = props

            const currentRecords: RavRecord[] = self.findRecords().current().forTenant(tenant).now()

            return mocks.randomSlice(currentRecords, 3, currentRecords.length).map(current => {

                let { uvi, name } = mocks.randomBoolean(.8) ? { uvi: current.uvi, name: current.details.name } : { ...self.mockVessel(), uvi: undefined! }

                // occasionally changes name
                if (uvi !== undefined && mocks.randomBoolean(.2))
                    name = self.mockVessel().name

                const generic = self.mockGeneric({
                    uvi,
                    state: 'uploaded',
                    tenant: current.tenant,
                    patched: []
                })


                return self.mockPatch({ base: generic as RecordPatch<T>, current, name, timestamp: new Date().toISOString(), type })
            })

        }
        ,


        mockHistory: (props: { historyLength: number, tenant: TenantReference, uvi?: Vid }) => {
            

            const { historyLength, tenant, uvi: clientuvi } = props

            const { uvi, name } = self.mockVessel(clientuvi)

            const recgen = (length: number, current?: RavRecord): RavRecord[] => {

                // base case
                if (length === 0)
                    return []

                // compute new current record
                let next: RavRecord = undefined!

                const maxDistanceBetweenRecordsInMonths = 4

                if (current) {

                    // occasionally changes name
                    const name = mocks.randomBoolean(.2) ? self.mockVessel().name : current.details.name

                    const timestamp = addMonths(Date.parse(current.timestamp), mocks.randomNumberBetween(maxDistanceBetweenRecordsInMonths - 2, maxDistanceBetweenRecordsInMonths)).toISOString()

                    const type = mocks.randomBoolean(.6) ? baseComboType : mocks.randomIn(submissionTypes)

                    next = self.mockPatch({ base: current, timestamp, name, type })

                }
                else {

                    const first = self.mockGeneric({ uvi, tenant, state: 'published', patched: [detailsType, authorizationType, photographType] })

                    // we go back as many year from now as required to keep the entire historyin the past.
                    const firstDetails = details.mockSlot({ tenant, name, timestamp: addMonths(Date.now(), - historyLength * maxDistanceBetweenRecordsInMonths).toISOString() })

                    const timestamp = firstDetails.timestamp

                    const firstAuthz = authorization.mockSlot({ timestamp })

                    const firstPhotos = photographs.mockSlot({ timestamp, vid: uvi })



                    next = {
                        ...first,
                        origin: undefined!,
                        lifecycle: { ...first.lifecycle, created: Date.parse(timestamp), lastModified: Date.parse(timestamp) },
                        timestamp,
                        [detailsType]: firstDetails,
                        [authorizationType]: firstAuthz,
                        [photographType]: firstPhotos
                    }

                }

                // recursive chaining
                return [next, ...recgen(length - 1, next)]



            }

            return recgen(historyLength)

        }

    }

    return self;


}


const boatImage = `UklGRkxWAABXRUJQVlA4WAoAAAAgAAAAcQIAcQIASUNDUAwCAAAAAAIMbGNtcwIQAABtbnRyUkdCIFhZWiAH3AABABkAAwApADlhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApkZXNjAAAA/AAAAF5jcHJ0AAABXAAAAAt3dHB0AAABaAAAABRia3B0AAABfAAAABRyWFlaAAABkAAAABRnWFlaAAABpAAAABRiWFlaAAABuAAAABRyVFJDAAABzAAAAEBnVFJDAAABzAAAAEBiVFJDAAABzAAAAEBkZXNjAAAAAAAAAANjMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB0ZXh0AAAAAEZCAABYWVogAAAAAAAA9tYAAQAAAADTLVhZWiAAAAAAAAADFgAAAzMAAAKkWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPY3VydgAAAAAAAAAaAAAAywHJA2MFkghrC/YQPxVRGzQh8SmQMhg7kkYFUXdd7WtwegWJsZp8rGm/fdPD6TD//1ZQOCAaVAAA0EgBnQEqcgJyAj4tFopDoaEhEUm0oBgCxLK3f//TSttA9DjvDZ2UaqmSx/kP7fs6ap9ofj/8J+0v5H/O3zj3P+WO/390/93+j/BL/D11fI/9vypOaf9T/ef3E/xP//+vP+9/3f+S/bX5K/2r+9f6P3BP07/wH+G/yH+w/xf//+c//q/673Xf3j/qf9n9t/gF/L/6t/n/8X+7H7////8rf91/4v9L7mf8v/u/+9/rf8V8gH9M/r/+3/PD45fYz/wv/e///uC/y3++/8j2lv+h/5P9j/wv/n9J39h/23/a/0P7y/Qv/Nv7b/xvz1+QD/8eoB/7vUA9Tfsz/fe37/Jfl16O/kX1z97/Jf+9e5D/c+QzsLzT/lf3R/Hf279w/8T+5Pz1/3P8r4r/KD/T/MD4CPyr+Xf4X8vv7z+4P1+fUf9furNO/3X/a9QX2S+i/5z+7fuT/efjo+k/13o59mv9X7gP8//rH+Q/v37uf3n//9Ez+E/5f7UfAH/HP6f/tf75/iv3T+nH+e/8H+U/037Ke6r85/xX/N/0H+S/av7D/5T/Vf9p/ev8p/7/89////f96vs0/bD/z+6z+tP+7/O8VuoIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6aKG9cL8MUL85vgR0ocG2XoABrhJRdn1BD6gh87eGaNyP5D8D1wjBpUcpaQiGxiOuu8YgWAvFdn1BD6gh9QQ+oIfUEPqCHQfqO695n/MzkTT7mEQDOKc7Y3iJrZ+d7js/SPZegAGuElF2fUEPqCH1ANO3KQjUUsHVUATamRPhv1fTJrbpdwWAjgeEP8if9Ew++WV04aIk1wkouz6gh9QFiYlEub7VjPYHu8w9abL+kFsxi3yr8AFVrBca+pVpuZ4kUbRUgxuvVqLs+oIfUEPqCH00+YP6IWC0MqVS0kibJcekzeHjQbvuH4F/VAamNhqm45naU9EUeqMxikJ9UtNeyuSS1zDREmuElF2fUB60hl5QrMAdP7Ly4KS117Tgr6rbhaM4fOwHDW/c2a9UsATQmgMWTT/0R/EnQADXCSi7PqASJXBCdsgC+o9UL4x1vsJkztnFE7pUiqoM2QlA/KTIVNNylYJgrY8P+cUIN6zkS6OgfJQzSKLIDdjjBtl6AAa3lMe586BC42lwXtkYalNxSfBEjEVSRUh9SifARN0EEFjYY742U8EG8bGTrIwUd483pOY3zDWbIjYm2j5HhHjA3i6IShwbZefqFsKo6RP5Nb/8tp0x4/igK6a9NNlo8XHWQ+Zm+vDYUJ4psa2pbj7TBpu9jbnT1LWblDAd5NUza5y1FvOh39sW6tRdn1BDrVZubZsi5mr/vfuIl0DA3TNnefVMixobMoSvlMBmV78GuiNZf3WANMuZ5JTPtbsYNhf9noXqC7AEt7+CelDg2y8/9A2arXNmVfIfa6xQKpLW7Xl6v0ZP/u+MLWHDx1zVLbDTldhPTu9vqCZV66eDbtGFCrH3BnCzyirt9zGqvw1Rmw8XOOos3ya1F2c0BBEPyd2G73bBgVnDQrDUn0ZRNLskz/9v3IXPTuSn3sBHbPDFOS25Bbx0xIfcPwWm4ikZvIQh/3Dg2y8/8avZeSjiM2xOYF5uczTD8QoE4DTJsshrVLknSuUFH1jwAqo2sut3IMzl1sYXaW7RmT+tAGhnHH9NBGo/hPMGhsO5OhEMLRn0HevrRC/dYrRHp0SqNeVKbVbgKapNESa4SSzu/Xro2gsfponlJOjlVdcCqhbEKHXYdCtBvwrLXzssOsqaFaCWl+mj7Qh7jSji/pDjmC5KLtunSfwyk4ds7ENqIIQwon1BdlNRSXVzAHyZ3OlKTSeXiEl4GRBQRj3XxymHzIwvCNu1BT5YEviytB5nQagEoKWYiHXA9aey9PZoWfT/vIAiAPYHYSUXZ9QA7jkMVq204gmqhww7dazVZPHCjv/jSXuzjZdh0dtYX3jZn1WHIIxgF6irZ+LqPF1+SExnNVx7GGRA93+ElyE+44YSVyQtx5XO+LfZDw525zraBTBGC+shFvExV5rumH/I4Wy4kkUG3CzMos5MNYz+gZabVMenhzHrbPBQp1hwvD//O5r2J5OSXgHoJIgGuEi/Xh8H/fN6HJLjqin8TsdaJcu4wcPUfJxLFFVCu6/wE8vLU8JgpeUI4R/yZaCpn8NABSxn1wnM6KWmAPSOGEMqGZR8uAip8z5UNYFBKHY4iPrfPpf7KUxyetLPdm8M5noBvyUPhIXl8QhxYWuaSR4hvfs7ofUqAbrF+UfZCMVTpI3gIfUEPqAVzLbFIn6t73/QA5CF5OTrblF9Qk7bMXK2wpau/+NAEHZwMF6D2Z2Rs0hMNnC8FuGQ1IW8fhY3XwFziB+/Fgv1nJdTm0VI0msVK4f5V+vxGnIOPhATisYDAskhFi79MJ2yWdS3z6gh9QQ5zHnEwhHWNmUCYFk93cjvwnGzDzy5DibHj2QVEL2byeYseWtranBad8tJkGcra+efPID1ywbGXtI8T2aEnmkq7uwnNThkDVPVc2cbp7l6Z1ZvRaKYlu8jiJg4GKbwwyQvgtEf6890xHxQOegn0VqXCSi7PqAWYJdk4RznPHnwE6O3t0lybyen8DhDDxjYFCMcrzLL0+Tcf8Wbmxl90BQSzF4ekxYT0LCQaxuah65giwvMe3rRqHulS5lgmQmx5Oy6/XW83zk5wL8DgturXoABrhJRbnw38bsNhuN3C9ErjUILu4vmjBxq7096As7NsaPnlfvashxfpow+9si3sK3SHWICP/ePzWHQuDgvKW94deL25G3rtq9XlRU031GP6hFRrvYiV77yaMbPg3Y4vd0rz18WB+dU3n5PDPZIURo9xdQ4pwADXCSi7Pk1cl5ELlazEp1T2i5YDnbvvoyFmItGj0KFtjGfsFQwMebTO4DpADhnWyJcrUbAgoeJO9nnwlHujcHB6iLIvQ29TMxYyN1qLs+oIfTRK0K1nTqQwD0lNAQIh183Ox9vwK089LcQdfKgnyPxUN3M4114DVs5KdZyusAPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfMl6vQBoYlJbEVSVVAHVmqDc4ExsMXB+IxtqAhNegAGuElF2erkefH2ReuFts3a3ZAQJXAWkFHCNXJ+dgCiJv916hwi2fdMDglxKvZ3yqlCI5HZiEA4cq8J4aIk1wkouz6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BD6gh9QQ+oIfUEPqCH1BDkAAA/v/ixZ6LKF2Ca5cKY4AAAAAAAAAAAAAAAAAAAAAAAOFxQmZimrSM3I9IpaRFDnlHf/UVtLp1wQzkKnrVo07zNI1LqIn7L27LIFMy6ERa6NXVEN2OKxIiz9SYi/gysHeAuPW1qIc37p3sw2fk0OmUMyUbMOzHtLpP86Ptf3O1j1r0QgzREqarqVv1o1P30CVJfZsl89Fe0Hr7JrZguGyvH6rrZcpJocLVuofo2V3D4EnsQAAQgEdsrtm0MnTMPv+vesEPOeuwXol4pOCTG02LYcjghUwS3Ow6CDdMyQatY4eNsLgkel3RJmaZmtk1zeFBafv16hKgd4+Gx1ffVI1SgYUTElmAZ3iIYi8EuwNbsM+4V4kbPa7mI/cgcRdQZfPDZ7xWvki1pxgnP/9WHQRCLv8sOkK3KCSrgt9y057ddPRD5Q04wOzGW44Q12/f9KBf5eOHYKV8nczpWSqXwKLiQDO/0E3cEs+kvqOK6kqRcHbcK3PcSnVBI/3PvhCqoI1psJmW5isGuCIdHHEXIzJFo6io5Uo+1hecwURBVyfrI4F+rTE8m1iqjvuplJB10+GAyPfkk8LLWMUMn9pCyAjchXRj0qbBgj8EeeQbfoKtE+xgdFE5qvty4lHzJuT4ePTWN51+KpcT+0oF2CDxfwz9HX5IwrJKjTJZYeRgVEiiXNG6Rgubo7WYwrgnMztvx6TdHQAG7OVu0LIg7p0RpX3/5L5oxTrJr6jy6dpBK+LF+ikLlPJfSIe1C6C2tHgpJ9K8gc0i8V/IqQBkLsJCzWtoFrLxol4CN4lGICkpPU8OC4SI7tZIF+YSqWzN0NoCBU8MvdyDeV3wgUNa1gzCdc4KTW35iSkX7sU8XpE6T2TAgwnvzvdb9flCH/LzvBL+TJsOKo25SPTU+k3bckGg+Hp13pWEJUgT/lBcSQ3P5WHATm+3lT3mQthfL9tmuOejJokZ8epgkzmZ+VpV/ZhNTmBfftcp72jjhYELdqC6M0gAZg0yZnAOMj2OVnai1hRCRIIh1eBaW4qAanS74xKcYSrEz6HRImFM1Vcb70QfI/T7oN0akkNwfD7WRmk8qhRQXBNeuotcAA3iw57QM0+1ceGYhC1RP2C7YASFYlniD0Bj2Imz4gNrfxW4CsNAth/k2CBCM0A7OMhfZrVAIbGm/dEyq5FnuwDeFHoSc8/SXZ68q1bzItod9ZhlTiM9zhOD/YeUlT2xDTt6YZBT1JE7dkTevo3/YPux4yIIT3whnGmzqnOf/Th0aRU9gsJFBai9/IQrgu+8KMNth97htbcU77+0201uD2kArmqsel+IMqi0uQxZ4c7Lmbh9EiNwtWE6snO9tq2SFI38KlWJbHE8aKW1y99yHMJ/az5T/Lr52E1u4Ua7Q5JijDb5FoEjBc6dC3nfSRWnjceGC+/i+RMuPg9XEJWSI+h6M7BnfGqzC9mb8i2UXLn3n6ByiE/h7dqDZMDwN+YZPQbH2g9qFPMNloOOsESTdMzOIDFkkm/2zXHPwFi1QIicRATClLXBA6XWKK942zLgnUH8AKiDySGzaEIgRP8rivBD00FwxqOvcgIaabl6VkF/8+Ck9u5otqxun/BmBJJDMUCGwzOSB5ebIDfOIehuouW6g0mOtGhZNQAjkDvrYtPrkZWpSWLi7V1XuYKhfP794zVXIAMEIxS4BIDYxdBx4ssYQE7E5sOVz7HoJemBALcNelv3gMn/EExfeqbEPpqkFYrjhfbB27dmCZ9lys6CRfmrI8j6xm0s+zXnIPotjghmIhICZ0akSsEjawmtiGOA+/FA554rkEU+8/l5bpk7LCTqm7ewx3mMWgZSvhPqro/V9BHH3lA31OognCoU4Tw3PeYhMc8OzljS3CyZiFQ8dnKR2eRlQmGtUM/M7CF4umQB0kCZa5esS+dZynpaM6jW6NcavTiBu20Mvl7oDqBmoJ0VB2Y6aoQTc3/MEqXwvunJybVNuA3W4Yni6Tu02KAIscKFDKgQoNo5OecKh4xUBWA32aiXL9q/ii/9gTSedCcbxIUhhj2zbREHu+D1Pa9g9b1LfnArxjLEJOoVGCsdVqg8WIzX8kB6+9Cl1mPTWtcbIKapcEL1OmyhnyvGqcGJyQjQY9tLnuACIV7HTNaGnPOOyA9yI0pbbdvS2PGCp3lbVn0WOVpYjuAfBVcJjaI9rBI/O6rGWoXqwV5WAxa+IvH2iUJtcY0WfNj2E+TTtzb8r2k/Gx+96Er6salCTbQXh4we68VBsh+OPksq6DiQj5LU8jsAE7PwsG0n50ekztdongjqjIB1BfXNYvS6qDDrUJEyzaFq4TPvKPnVKAgGQNEB1AUlPgnm3fluHoyQeeqBhUkpk6Fi6f5d2ftV6Tci1XCui94SuUWfoKdfa+fPnAQotPeolIQ6C00n/Tw6Kyv4CtCMl3Y6A1I6UeoPXSeaTDcvUT5CmI9xbAg35hbNMeOLRd223vpDSPlnrB+OA4spMgT3LXv35SJTN/Kd662/e0mlonDL5j+H4uXQnxrS77Kgc30E9ZR/hF8U4KyAAfdB5MXRYfEA8ZQ2tCvdI7cOvl/sjDHeI4G6+KDnvPqCK+2POdglyWFlXEyLbbbC2hYfai6i2XlLlakAZGJ+GXBbKJgzPnDSGTr0dz3meMK+E5XgzwtPAA8KUYCj+USqh/BxZVWlmzoS0gTNSzZqNmIk71gAilcCgHex22BE5j2rjiDCCHvAABQHPLQbChtnCgFM6RcwJ8U21Cs+X08O06NqlazCvwByI3uOWeu8+w5UEiQGo+bkhXwzhgv91oq9dIG8vs2HoYZzhcP08QoEk0Sgb6z5AaGR9WEGBJsziaSR9Tc02Y1B8ai8pvSZe2E/LNc1d0wtFpYwI+f2ZS4sgg9tB+yrFW8uJyS0iaFBxnNwxaV/V5fdE+t9DSH/GloJnqT8GbDGPh7PDmPlD3/8HJPiV9W9K/aFkDzGdsA/pEKoXoiPMLGfh5vYMfZmLiYmySTkeF6dvysjXsLYh7hl/3ZRjEWGE6LuUGaZhmgkbntSOl18MuXARoxaa/Q/OkFapNZe9td3k1wgDzVdx47uM8mrxfIxcmHbukWtLICU0jsHsq0XR+FjdX+qDd87rCGFxJmLto8T1AK8MejA8aii4vokCZ9iBx18R6tjOmT3P4cl1ELZQqaXBCnGTEn6QecKwDx9Yq9y5UAJLfVhvcpy0qK8+2rrqhhVEAvrEdfx1/8+9j6Fv1hAOPbK23w9Dwe/ShvzSX1GasViVKj8h54H+Tn/ipkTPBnBHxaufvfLkEpNqURwIMlRAusXUS78X9Czpefn2DdpOv8ofpQ+7yPJePvssHL4YDJBLVwYytqd8FT+k3RlSSFtmw5ds8z/Zm+Cr7Cy+Wle6/pv1suQRvH6anvJwXGCJbp0shQnteHDw8+u9hf2CngFQtPtjgaINTRx1a0kEAm68botBc7xSGbJDlM0JAtbiTBQPv2Wg/5INnV34Pf3XodxVcNkWmDYtl8pUBk2Oe4Y8OEDZgdfBNj0b/BBXQre6ctv9ihgL3V5RezK+bpGm5qXZRZVrEionjLT6aHtfnZBMOn1H7uCe+6Ts5mKCTi1Tq5OFKcSYjoHzW8FVcVFOQMWMGjWXz4jhZlgAC5omMgQ7xuuPGD3WmM5Sc60O5318AEYto47oGEL/S8W/RKOk0x1Zin2w9UpEHxAErboTl2rrlJIDdDbPJ8R9d0eK8XE41QvVNdmk/7uI1f1nJ5Xf+xP+VLGY/Z3xRZGRMI5r8o96k6vG5J0FPo1MSVusCGNDZAEnhRmwmtZEhDPNx/0aP/pFYb0WRRDyItZqRrc4dq7a0FlBvL8pwKY0qwB1gI+79KIK+X+EJ89F9DyvF/o71GO/MURNVLjjB8rxfq3RWP3Bp/waQjRTyKTOra52CzVgDd43D3O3YQNG7SvrG7TmDO6/tDd3pKwH7PIJxmSXePS1nzAay3lH5rTXcqcYHUUdBB58w2wIXI4QwL2rI8zVkMC8cdrEvmFjMQIH5ZNmRajOPtV7ncgOo+j5dxtyJxrug1qZfo5xpPl5iKBHKlvyKwOHopV95i2m5AvqP3inYVKvmQsRwX7IYLzMD4f99ygg5R51ZgC80a8VX92XSeuQzVEZbiOB+vTlRs6ot4W3zzYha0oEcjypp4ivQrrScgZFe0aPMZnZQFUGsr7W60iUrRk64ImJo+X2KanNIJ5smgqY3n2R5Us6gpUN7HxLxMMhMp0GrGRYNZRZP5IILgyknWpd03W6cByoi5RQEVVeKW8vXEN1FVX6QR+JGHIC87G+qG1CR5BiluPc1YfzlLeba3PaJbwzv3OCi9bIcJHW5O2ukW0p1mnov5POayIzz/iekqg2F1kqOlMtQ2nF7gSL9ID2RwvnFQP0qrcbH7cWspyg5etqVYicEIwlZ+gvxr9DuGjGXn+Z3jRxd5d4tbjwt0DuS+g0UiyJ9YLGths8OQ+k/nwInOrYK0vTWoTpbF0/+dVThO9apfTtDaLsUra0t/8QEQ2C8JzcqyrVCbHyN9ASFw7yNeP4nIPks4yz4DZ5skVLiBLZ0ikezSreDDOuZfUuWMhDUa3GTgDs0QCR60N4024c3YHFy0o8GYz1zzeUx09SsvCm1Omk0gCuMshKGjwzF4LOq3EG5TvzYZ0AL2h5tu+5JM9wu3+ZFeNNI2MprrX9GGvDsqSF/kpaRcTWBbp87LhZxufWWwPSwaq2TYjrxy7yCA7YIAxTmIzuCZMCA5VQVfbM0n1P3sQXM9C1e7fZeXvYJFX9uRs59evh+abx4JvplVwlfzPAdE8NB+vjvnsMKZXfiiAlhZVfyMG2mamSqiWizDK25h03LpYjJvxWG8B2YLpWfyjwMZCwEmzd3KHMVC9mPbs+kZEOVDD6crXeLjBTjySwlanua5/UPi/xQ/0E7nm96hLw5+H1aBMCxKEbtGxlu19EIlBn8ZAwXnaq7NHjXJVk3YbJa2hGN0kT01S5VOhvRYtEFYrTONGXDFjhqiJND+aQlc22K6sAxq7n+4TRMk6Yu9I17Khn+XIBYK2IO2nupxOg0iI6XRlCXqHurF2ZuBn4ziu02zech3lMLI63sryIKP5UQ2MucF8G3TzU4scKaRmVRop01SQVAMbdRDW86GV7f/kIhKXgcB5Ot3E6AsIKNz6AZlNXSAeGIvNWVUTnC3R8ovwv3xXt3ICeS8grAGdHs5fTkjbw/Ciuvw7jnKpJoFjz7unGq1C45UNTpmEd4bOf8VjzyDfa5dS4FDnCxA4QsHT++Q/xjwkYBCBDu70dg74RVwn7INxxxXr3YLYXMgznhouXpbS4wsCxbB1SxT09jRVBqElTzZiuHNkFUAcQi8AKWOllCCrOfqQVZH7R0y+mAjqKxL6fGDBRxDRVqMrqJIOW5Q3QcHWQTKcB27m2z9UIBCPClu0MfpiR56OJj8LY/upu2MZPTTW3Sdgov90u9EUrxXdFwvSyhpSxIWHaR0yIESe77OP0dGFQydy/tT5VUwd5oFNwo6kDidpWOc89YLNOiennGmtGjMsXdCjeW1IknCaBhzToacIF0pxuWrtVgOgSZA1O0X9wsfZ6DWj4blGeqSfoTe6C+OmXRCdAWB0w+fBA0OB6G3pGf4pRMd8xQxHOzdKsNT/xh7rpO202SQkUrXMEDZqm18yWlNDO7BEXJhW/cN8eGZtrRW/l9R7x/WmcQWF2OQ3zc8QIqwDtu55R+L/cTt4q9BgW9Iiw4YzSMDcOrQmTyCEQFA6mvYwcT8x+Vvr0r+fIkX+pXs/A68jQG2AuW26/BDNSOwdaS2OLDn2oSBSOukzF5/WeaZHlib0vlq1u5MiMtmIg9WX6+r7Nd7nNoVYx0X31Y2ad5K1mn5tMVUzPfENxSfBF9YSS9hZ0MdzhgJs/dZoVH6UYwSkHm6qDyz0xlhDoCTLTEIyEl95UdH9eik8KxZAwPLgR8N/WJQuv/rm7mICEjbYdAOnxabxovgIWl9yOVQf7NvC1C8hbXkfJBLuThNXhHvr9WlNP9+WfqZgib+1X4Qwzsbs/6AYTx4TFLCLgJaMwUGySeR2v++rVN3nLeskCRehyVnjgLVqs/9xR2foBgbfJad0thx/i1B0zn+0jP8dnnWn7/815Z/r2zm9//qZ4cP7bOVfaYb8HXgDm+uVxM0FAAJnlgIJVCv75VnRD8ZT0oNoOawPqlRsIYpnwg8ibdLGYGKcs1jXSyRtOQKQO6sU6EQIXZfoyjOfxQpGQXf1JppBDv2RIFVMzHfQLLQGQFzsL59vUQ8sSU1m+/9HtNddvqMB9pSfKsLtVLqqPESmiWk2b2QeLtPl1ox+qAPWY7yCWH5t1SjNYGyn+NzAEDeB/sLDbWVm4BiriltMruKf46gkIhQtEDGrRezF0TtEzmxoCuXs1cFU/Nxts6OsY6bSzFNtyd3DeybnFUnKfUhZLypelXUmel5Fhz82NghCGQ+op5z0cQK6NP8YoX0mt5EDEmtOVREttKgJvo5QoxNqATfqPTofFuWtiWz4qCz7v/hJXIriV8xTaG7WT8VWr2dD0brSxYqAbcrrfvIsaLS/c+mUZMyWmBSjK145b73Yvij/cW7+kFUWbFsfbjy5KUpltiEFONUws7iF6P7McaCbAfCAw6xI+Gq1JKTxd/IdBcCJfGoBMAB6z6H+en8Ml2qn/NFcZr8vOpjfd/wfws3j/kw5H2YfS1lu55akPhPf70sSYTIhjJbs7lNDCRCuXwIpguqVckygIwAZH01eMS1O+zefxcOG4xT/AvIOkl7B2WkldwUNMvmq2bAlHWECEFxhBWqnWGOO5oyilsp845gRdvzsJlfoYypJ8rEpruMfL7LzCNwgEAVJJDXLUnm1zYcUTnvh7hBb1JgsuEBDZlRG7FSazwJxZoG0x2rPmFzjORRFVjAhWdL3cqpMDjZVrC6leaQtE2gPvdbi+BU3rJ2JkSGGDStAT8PECxoyAIvRGdj0HzuCk1F4rvXh8y4+iy5QuTzTMcUDqD4IFJvSZPWeb+g8uJjvn7E1BuSAqtbx5L+PZxckXGMa1hHG6AwkENUSwvRCExD5hFvncbPuTfu5wmIMubcgcpZ2ab1WrnEPbW1BokRLlAs3BQKckrTnA5iVPF+LsaofQAghi+8dXyL84z8J/cb/mTucIVXaO29e93rYqIwX+j3qfQ4UD3e8vYnbHuKEqMjZH8PsDL5JjQ/1nk85h8kf/7oHWHvKb1OeVaeaT7c+in9M2zFs4Lk0iRg74c4CcdTkGc6aaUj+MjbSAU9lKpsPf5EcKyN0isvwFGXEFdjWZy5RjG3dDP53oQQklcckQd2ps+FlIanrws8f3za4eDFFw7LUz4vR1iUUeZ+ti/fXk2MOGbZTlzVv5CAUqT/zG2SSvdcPiuSN7ZI8dX7AUl1GepG/ncxHHw8XkmNxXw9gmISHe71kRAAFLNhfqIGWXSO7jzgKyADlX4UYP15lH+w3zuU4rP33u0SnsVlgYTPyuL1oVS5F8ES8f6Uai7iJis5MZLjkHCDtQO2tFpOsASeCKmeNH2z3yhcyxRm28p98ElDze+7ohlj7WeM1ODacAgPziet6NHLeYHzp+6XBQAyoBCzYspK4silF84pCGsvWgssII/LjaDwRZB7BZ7VjrVugZYLktsVMZ7kS1/5L7BZ5S8BX6xjcRjwARoBAEFwFIdBR9lpF6Ra9ZjJACZ+zoco6lNT6eInGdtOjHPO0T9kP1v2+USCZo2w0Jn5g8Fsk4/TG0DqDujT43qviwAnL0+HbVlobSF6wsTC1MdL+d7oNRfZEDs7dS7c6lQ7WTsEiTumfJqquW8hYX9g7b+IuT32WkeebQAD1FE6Fe8MqIPvIdVp3rOk1LeXsQ+8LYGrbv9PdfXygPrtKLTcdgb5PQbYg0vLLrBWIf3sFknSYoE5rIApb9LS4qCkk+wVm26Bq8k+/nA1GL5lqT7n9AKNTVjRHwswEnFhpB8qRjSxKPG/tz2PL9aWNlSPETcPBJlWAkuqSppMFDMzjfk1hHNs3tzZRn++12WDKt6GlER9hC1qaK5Yfoe/SKqZ2cDqAWB2kUZcJ9fDTbZJe0cHVTdV3FLtxzOPM5NJ12Nbxxlti6J53bKbhYt4TnN910dLqKaG9TmZLm/sKmw2oB9fOQJQ8hEyLIVsHtyTfyTDWDN9hHmk1MXyYHnTiX2Cpp2M2iAKXvpQNpfVE9HouMeuUtK58sw7dtQ69Z9+nvJXmxqWxcvWWiwVzNPbrWAuHR3ncL3GKihFLL5HwQDIG7WdopaTAsLth2BwQqUg7km1U4eFFgrvkiSQWftx2dSokaH8zKVMC6A0DRe7ca8jAmyaSo4E2eCi6028nQYyCRKIlpm0kXXWj2k9mCyRBKyIoelZOjdDgZWLXbj+d2vM0n3Gh0IIoJJbn6evv3Ue58J0MiaushnI7FeyGjZZiOMCsTlF6ui781fVtieUrR2hC7aSiMat6ClakBpMtA0p4MnyAQqRL71lBLOHtHRuLhs+q/D9r/acfIxO+HRy+9oTWKX1rgS3kniCZWu6cP4WGJGiX3tKlLmnc+O00Zl7ydmnBcaUDtGQ64iVqkWDkjmZkXH+i+IfahCO1OyG6238F5xdvLXdxfYCgsDUKxyPdPp1Up7RQySo4xyoOCWGzkSQgq+Jj/6La4V3a6FTuuqQvBNqWc1fYTE4EyP+M8Lihmfsn/zQsBWpPYo3tKFqch1n3Vq4zC2DtotC5g/DO/0R+OkzqEXp6hutiMQbiJwl8LiHHo/HNUChU0zkxV66gIHsyP4ZG1jgJEHMfn1iBOA7eMV9gANAJ3qHdDQ3N5ST4t0cljQL5u3gET2rWLW0nSy2hnFqyDxQCu4ZgYhP+xlwzRV/dAMUOCDP0LVnOJ+TbUiUPj5kHUgblkHiVp/KXpC8zmryMWRlimwZGaUvL5kHidovtU+rrBvf4XHu8R+zT0vGtmPLcWuVejdNHYig3plkpL7i8Y2donCY8uXjgaI2VrXxn8zqXAnllY4y6j7JYllBuYzf4aWnBwMSqCyIpdD5BGri2D+eYm5wKg6XOhTm8g4dFWuiRpoo1ldraVK1r6uEYVYNkMh6hm2L5zj87M7Ga780+hFNysg6XismrX2PVABlaLD8h3eEgsaJrrlycA9WphXN1T+n55Z8Qz16/i9apVjykOfh+KS1Q99c/cNz8WNg+tDFPEY5y96BlgDaKJL9ZqTBnaHWPs5kKSKilPp/bIEdVz7KlUpuYZJAcHsTJBRKoYDhp9H3dOaSLqefpCsCgUW/URKalrfs7ohKNqAySD9/UTHagh223LrpHn7f68PAe4xjOvQTZZcyRtYRvEG2eiIX+DklXSIasZbjq8KMvGlWsUCYB7BoxmlWPasu7+IJSI2an8ju2XH08NVDjqFHGqrrFwgMPgj5wgO2LF4niDC94S1Qf2OoAoagBIxKkYS547KV1BcHHNTT1CpH1XJ8R786hXYqHCBVkN1Fb534/KPRwT/Zp1GTvKlFLwu2HxSO1Bj58f2VloDr7/9SmDoKwSRFeFOXDhxCQ1E4aYCoUcwWAhJcBP4oRrpOZfQVz01QnjrLmtoz+aMfelebmDDPJJSt1VwQF1HRHNsxKgXf4NIqKQFPimrP/+USlBANUWB9UlFFQs5ar7KAnPgBsZac0RGnZ0NQhmoK2rtRf/JkTbdIbTRkulN+S1np10mvLcPXj9NulW3ZvFo2A8k/bjThZO80LX9rmgBS7/b7aET2T748Xf87y15/DeAQYqRyV7jJhORuKzcSg89hNW6eZKGRU5Mm9wn4Y7/uJR8KQE3KDxBEmACAhXphL9f8Z+UtqnigtYWQk2Pq6v/IfOwsXAkrTYrLEwtAwHJgLDrysZPWNI00Y12SlnP4jzxt6+ZJEUqrAHdvGqgJYtTDXM3QlKunPWEek8A+NLC2VvYgQi3Sd9fZfz3CdFQTbf3lQgy5ICikkRg8hhInHJk/kJZFgytk6bvILPcw5X8atOxzBKZo+S5QWSOEDm4Ri+p34LLjnha1jGLZJMgBbZ7fn9Xn9Q5TZEeUVerTzosEToeWiVwxbPPTwTeEzngWqhL07ijOy3z9bJ9lOj1WOoGp/AuT31YtbgE/UvgUgItvUtHtNrkM2LPusQx3+ZPMqEGXLJPUSGXEMHwnP6rXMw/pHJeNqFyVaVqwXnd2Kemq02QGZlhad/YGjoJeJDt/IIB9kKJpomiQmxl7sW9kJ6fbTf/XIxy/pCKrJ2k9aWbx+0NV90bUq2ZVlbX3Sp4OrUYnoDlVRDKx/nFY+sogqo+MsP2ee3jLdHuZXQ3KlpR42AgiHDA/9JpzRjFSePg5eHHIehkg+CKOHksEczOH5TeKjnMIanU2HKsQJcQ8ynQLB7zptk2jzMNkC5EzTorcoL2b5cZTUbRi43n4BzGpdWT//KA+qRxyPbjkuFCvFScaAhJhBpUaXS0RQLe7gGa0k/PdIoGIDrSInIX8bwxCjifCq98MiRfErsyyw0Z003l9mTvx+jryYz/42WW6ah+YKebe6Z6nARGisa/FHdRTln1Kc+nJkqfWHIrAgYDgSa9xM9ySZr6jTHk8AG1mdM4YgBaJ5hq7pfEkzVl9ktXVC99JMbyYimkQGSsD+JMO5IYeg02iDFoiK29ajVIHiDECeZ5lPXU/VDCLJOQG4gD+RREl74iZL6gMPDPxNGc6YiWz+D4ncHBkAcSp+6ryb9ebPChpAeMPYW+rVe6WvfPmByGlmtdXZ7xH14wtpOZn6QftlgirOWZfQXL3GC2R+6optkxuTS+H+75hLvVPtO4HaILoIESCt7/6L/1xrxUGFsi6Teyi45fEFjs/iOYgRa9LuBDswFG8AX9rXuMRf/6JaM0eXGmD5Wx1mOBQ9S4eD24U3A9FFVs2LxzKMY0rV6AnOuiPPC8JnPXvRQ5vf8DrQy0a0ajkIqXq/UgMPN0PDN56c9P4JhHgXYIkn5lcNm0Azkhn+AuOUXDnKq2RSnzk0XDqT7C6bTZFGagZBlBaU8V1ZzCEQvbjW+DZbmdZZoWfJnO8MjZj82MWeWG3mzE47R9ZXQVuXpPdGNddvxONTg6tmMXioAegTg/JRnzrWEaObCvkNrmiYArM0ZgyH84Is5M0NbrawqDjcUxZdO04xfyzcPJAcU2yA4vf4U6mlCGiOkFwHiv2cVakRKBZ88eczCvAWnFM+dNmmimTwAvq0RVN6230rV7t51z9loomiFVKge3I92os8VtiyEKlqvKYjHR7rxBbnMQb0O21LIy4BHjwdrxcD40JdRDlHee8xHH6Ex8NAC7sX04rLDvnqnLZRvEjxdQrK6vHp/X83qiHvKjF41e4nWNtm+RZCbV2e2xpChX6ZK0sTMTQRwA/UCBM1Gp2jClEvnNmwPdAUhCLKWNJ8jLawV17SOWK20mFATG0QJ70B0de3lLlC49mK5jyJ8BzYhhC6WDZ53m58uzz4YZCuAqpRwCGNlTU7GsVjLF/fna2xLWQaCDC5gq6yLhzNlVAPvW1aht9IIAou5IW7PxGf0G8i+qnst1PCS8t+csq8Hwsm2EP8fkALL9Hrk0EkhHf9OSKmjaTExq1+7kuvvvmWpol20cr/KveKkBxnt6N15ZL3+4/SMVj+ANg5wDEr+7PNp2Opcw1IS+j1X/Ak6jESttv/NrkWqUcEWyG9Q/mFCNqja1miua8ruzj55NZfyRZObv1/LsLPN47+i9jg0gxcgB2m+NJUwuR7ilfOVbmOS9+T9/WNOjxa7U+fxCco196+Ap76h9+e+SyPlPNmml6Mlgq4y/8+mxiqYPPwQNqyABXp5GSIbQr9ZwQdFbfwTpVxvSJdTey5l3Ev5UcTptWYirsuHscmUGKvfdBC2yjccqJPRrAsO2sLCmSUoiRYO6dtOT/FgzZWvlDYrvo/l5LAy1t6Bv7Uf03o27pqY+JRnSAmpIrnagY9ZSz3vUAQrddil7VPoHFtjaXB1xs9u8HTRF9cZLuINmH7OTPzPag8R8E6G1GjrJd/Uv9/vMwGXpOIs5zIAswAbKm4HUZmmwRer5aQIOkNiuS4e17fZnUelgQCRxFYYLNr2IfLCGU041brM0CinhUsQwU5V40Pj9gl9uFXShzuAAe+fxV0UJAmFeF7cXbbJXDX2SS7UJ7pZJINQ+npK56y+7PLQeq3qz0ZgWm8bKgoRwkycQSfwzwSos5QGymPFpGyC/AOYmKV57nDKyKfvoJWn5CB1a41mvVB2f0amScEkLmGzRNawd8jQ9ljWKMJVKUngT4F9njJsqoljwzXWzP3zNe18/vPMAms+uBsaXp2HLi4LmRRmrsIT57MmskHssZI1qYgd27+V05Uu+gHbi6cwFwbpT6RM5P0wNBiyK7ngxQWIeze1xGHYcHmp0Skv8gXFScAUpB6TLFh+bPAqNA4SJGXN2USciTcTKAQ4TbbttbOyaartWCF+cgH6YL0gM6Zm1atphGgk7Kh32aQbVc3n2FE0zpJI9Jk6IXO10w1eJmgvyx7JyoyoY/lwj7qBU+bCIzBb6FG8S+DQknQq2o6Q6NFJ6egWW/VXT/Zgt3nwVYJcTQyIf1dQ3waXUs0D+Frhy06K1ZRcRok3kTN7OJ50vXZYR9hhheHXw7DMcgeNZknGlruBXX+adtDgJCGypTip8CCk0TKHhCsIuFqwEDnGqzjja9vm23a79XhdpA6XmZ0+lwotL7TmbS7nME+ZRtR0c13ld3Geyjsz/FK00RoaHfpddef0nScUcnmxpxDKPRxG3UMztmwND7EQJPBs1bG66JYEoZBtWlu/Iu1xylwh4bnJmueZWfT+HQpTc2/k+OEhqhvTQfkHoDI/iQ9D4UXSwzO2S7M/LZVtGtYY6Zk8TVb2sVg4jqxOSzthSg2WvR2RcbN8rif5c7NMM2LJXDPie5Qy7imwABaqVbFjsMHPSj87va3RRpnJqpscXTeFqvlx04MqWU0L43FaSad7GVqMKC3WIX+rmV2I28QFyZ+0AXKjuG0kebsfVqiP/POOTCa8TmW0Mrnl+tantruKvIl4l2O+2tp8p7Bo+GPyUkMfYXvsanX7UUb4QXbEaMcFqmUyQcUfchvoT7qdVuGgZLcSNU5RaceCAM4bEuowUcV9RhJ105lgK/RtB/cMYINiZpuO799gwqNsXUSoTRLMrOLKYbc6pfi7DdgoEttxzG5F+8aQu/0aL08G7VUCxdy7ZR8Yu5WcfbqwOl5dH4gPKch/ZVkeOb2HXVv3FSvZl+d/7f6uzjkX5STmJ4twT4qZo6UlV1QpvaIgg20jCbKE9fiKg1BgcV0kgm7A2ADku9vxznfgRKT5ifPz2enLVuzTmjaOwoyfzgHtiWAE2d4R3qzYcJz9eYrNftFodPikHAa9mkO5gebU4piurOt8N+8ZTynTFDWGnXSfemLF0QpPy1K41Z7do+ZUM6b2jioOebgdyRt9eLUX3pU/UtsJrpDIN0DteuO5SWR0GEMVaUR8nJIGs/yXPLiCKQhbhd8czT/jXj24e9CYK272VGP5g52F1c5xoDOmZCVMAxs4Q6U1/JZUkQ2nQGmvPTLzFAUcYz3MpEAdv8XySZ6uB6Nj9BcQpVFmIFc2QC+sTmKCM84JSVpGazRqF0Gxl1AHLCu6gM1ZYXdensREKO2HzUjGCxFErhrA+JTY39N+2g16bS+hf0E9Bnx21+MNGuMHp+eTXKCabXi9R6SXOUqxNjPzgPnNPWaWjM9NbPDK7ckYUMBV9+3fMTFo67j9wTAkr8TQsZuzw+MCx5ttnb+/c4Gm6igxUYjX6tr1hdZAKtooW12XnrCsTTrDYHCMgGkNswPc4AbkI5qEivV9sOcE+jo7Fvm6NP8Fw6SJq7Ts9j1xKmJJEMXhdkLXu4eNXsE0YpkPQWnCGhoIBIz9S/Ei+rTygGccbs1wof/T50Yqv6ZSlQNtT1E6lxngxS46ssV/3ZPU+KndmTMWrzbk+RAPjpLTN6ikS2ni5weyN4E2X565cfOb6PFEeS4wuiy203g9hMML0gFt5lAWJudeRgqqyi3qsgDO1mIajR3ZzmGJX759iZJwaGIayT0yhDzrkIaN7WUQUXAFZEs/5GfIDwUEBI5YK8CaCPwsdGfzBpow0nDfIGpjrUUzti0dHcKUHYFxsxGH705JzwLCgKLkjjVrSm1GEyWdPTRPxvrYfW34AjoZ0Ze7l7fglWhUO6+AxzTLK8sVaNtZxygy6fuVS9E8WVRmFg5Avcf2XQ/3zjV+BFr87YqEg2dT+vMvRPzlNiB+LwgdR46TAt+dmtt+OAfvpz3iNb2LJzKQBpQu9MieKq6nOZLhkLEQCPfHJkSkQsj9PHJK9ZSe4Q4FnBXnO7OCzrJhrFsFN9MhAUOOtIw+LyZDyRfIsBTryWTtqAp6U7zVxsnY+PsA4fUtEPQy6e6OpmaBPwfuVMSkxZqtclDhQOyxxMJnOkedwbln88B1NTZ49HtVxSmRxpLyS0Y+sSAig7VLC1gy65e/5k7pVb6Gpmbu+r8yvKcnlZrnT0zTS5ioQ6jttiOP00a5QITqlcUIBKRIGmB5sI8VFab+7q4TrshNz0x1GrnWL1r3tf7RxQogQBCx91Lnkih1vcHWXlB6O1QOhq6VRFV22mrqimvwERReen+EnX8nyDZ0BXM1ooXB4phMsQOHaWW9kxRwuxqZ1/f6JmZtNFSHKW+1tQkMPbumecai555a7JrzxkxjGFD4HivXklEkB/iKP5tTmDYMCXv7+iyL8YRPWsHjwpNiDrYQzZFOsw/DOptrJITpxNfFe3nX8UOhUigtEpTDjsnzzKQaVbKKfQGUJjzdEDiFUW8RfoPnYWLAfpI4FnH5/aMnt3iUOZ3qW1eK6aSZmsxODxIVRmAhqRUwswQvfdX20qxNnpZB4Jh3ruosC+M7eeXEQTDWV1IZ224ZRe3Jz4I0m6UkUa8KqgTTCDlyP/fKcIQuYW8YCQuMo2ub5XLyyaQDhOhkE30cA1ZF+SnPDvzeDfAMA/qU5B+1bhpCEGkAJpPmTWEuuyvrMePHQC5XsmFZ5VV0vfIih95XmivGEQQ/OOVB5iVhS/03Mn4yQG2DphdAfC58wIlr7hEcTu1ODmdT5EBX3pDrLAZzFuqPUF0yb6n24NitPS/4apNhVSMfgipJr6YP+aPPxFVl0udN513l7nzyg2KlVu4c6P49JTiItwtkRNP8jwcfDYvhmJqo1R80DFwPu9f7THls31XdE0+ncQOYoavKYpAuQb2Zb+gIdx2y20SSS3PxPhZ7wGrpBdkDvq12dIT6r1gNEeh7/rvtZMZe5E10w2pYVrL2Up7jQ6M0H61zuiTyErfxmf4VjDsmGrPfp4QNXrnT5Mwl+JScZqLUZPU9VfDJVm72LerMtxS4q24zyphdErDWICEGRHJhWeJx7gDmKTfwz6tQdOEG23nPfeQ+cW8BOWs7WUF5AwOqzJYCyJEzvGnDQn9vNfWpYTYsStX86DQLcEWBJ6qRYPnjIL4rm9sEqaECRYMIm7NBwNDHA5WmwpD750O/0TCx0+Cz+E+o4t9kMF9Qvh1pImqBnHEtv3yWPUM/fMzGM326QRUgXKi6Cnjar6zIme9Ex+R6aPvCWTGVOJAYmrgqWfNggQNwflPl6E2o3W5/Sg8Wv11HQLp3yYzi0CJB39Fvm9x8+ndaPijCXDZ2a7cOoFRUiNJ2PFc1aMHanImyX3BVd+hSGp5P2QpOqkbjgR5rOQI1siyfcHjvZ9EC1dXdgN286iBtyA+iTzsVYMRMTb30dqABWTJfaFjGd8MCzX8QPyjjwdPr8DlfuC4QB8piFQ0/J0nD1UVY6rplcGzVQK2JxTKxi8EX0U17kKlRHluDsjNFmT6FP2njWKQQiTxM0xWKihgTMvaBXwQZMj2kASSAqMqBGdrfUpvDrUlkK8lm1sPqYxKGV+FWjkNQ1JvXfFTaQMB1CbZbw7i+gxtl2zUc7dGh+Zr7cTbu341NsqU3V8WUR5ScSsrRGnFRfNIQ2PLv/yUdVwZibGxH3QXL70WeRce3mOOlxj1Yz8dPuIhaQbMl1h4GNDfaWS9BMkhP3n9sM69b1uxwbgr0kDG8Q2gURI5xnDT1YWCL4p7Fx8cWlNJKQ5ePvF+fEUGa7nAPDIv9QQMZfV3GH9Ro1qJqDHidKNiJyTvkO+qNhDlvXmvsuVkojtwd8vdrpOsrsgQnvJ7vrvkloMLiSmUIVHxeaKBz9G7pq5tKnp5tJL6p6pQvcgTYQI9d6iRvG7ZMXs9aH43Onbq8mg1dTDwsuPlaSDAUm8ySzPlguhhIfT/KngeOC2rpb6kTiIzDn4p3bwIGYEkShSNrBlDc+4LTDMF3q81RR7Eb4UiyVrrI5bktimGddSHWZ0HaspWiSKPqu6EmGw4hsAWXF2IhVhRVp7pzjvTobgPAVSc5dUl8jDZbjM1HcSza4dI1Fz0IQUx0Ah4x0BG5Se7RPpEPvBq3PXEWuUvMeYle5+r5KYakcrlGSyTZxqyN2YA0eomnRoALCNdcDUd3gAwJ/i9DjAw0prRkiQrzbfsURj2MbBSMcYE6sxKRA55hzLUfrDzzwwKfOrbG/XAA0zXN87w5F3wDVqX3TjwYagy5y2U2MSXrJNcvzPtH3frNLWowPuFiJI+oS9/2nt1tVsBiwKCXB9EKBxpgCzCf8/h+wFfUHE/2XIdvjt/hefq8vJQ3Hkye02P3LWMezQpwkZUhXoyz/GeIMkSqSNsG69QWsfgZkMzIXnGVtYClqo9kbj0DWdOLNwA0XXPJfPAWnWb/prRDQkXC2CziQocxy0p5GO2hxkNtnpp6CMuWy+gH78InVvksKAfacYwY2evd7YkAfXJGGN5yqYNwQN3rL4q25VVI1lvudLFVPXBS6Xc+XlTHoUmeOi8c+zX4BohzTbgMU8yjtxauBHVlGJMToW7siRZfjiDHl95tx3cGe4vdABqlxSu60E1C8Za94iXTW1kPOQm1Kez5HHYBjQCEX77e3ztMXhAwV201CgCbJWK599Mvw+WXHlxBwxtmpkTe1WPh1cAqhTND8BgS+rZs0Tb/UdQKmyqCpp1mgqkwLdJF83ydwJ3LC0LEK0xt/vmU5sqS3yatVTE5p1+Rys6N0rTN7+AG5DUZiStNhCYcOhj92xyLlossiy32iAkACH9U6vgiLCQG7CTR2HIwcnTqvSEde4mssSrT9UJyQqCZMCDioBhEowF9lSiPBXd9V+FGSNkOE55vzcG/3lzh7NNg7vcT990qU1SKjeGwhIA8sIvMKY/8FvK6yObbMSrU5YOBcr0nXSJpqZfQ5PR/krg92DIfHs15RJzMXPdflg0BnUkNgS0IdOlL0WsTQJ44o+eo94LZIubGDCK66NwspNnlYRRTUQsjljQ4x839fwI8WGb1hAFbjsD3Ggjn/VEkDjc+EL60M8sph4f9F5YIcK3C6jfHoaag42Q36JU2yZdzUcc3gKrT9+Hkq8iIxCWimhlZVzkjUqt1vE7N11uatbj0AaxdvKwwUjwZ2TjwwKGAqbneEcyaBHQzgpukICjEGFneIIkZE3WS3uowhH9jd/wGUE0upuefYxbFkDR8ItsDfodvKMeW5JU/N9uAj9YzU9bC1iI2OrNV3RwoIHW35+784pe1ijjp5uKSFzPPu7T6HV4oWMLFVNhfrS0HXj2YGTz566NTbXwN7ZZnyyw8neB419NDUHF9HpyKTyO3vNtBPmktrvB4sZmT4mnoM6iU7D9NpBC8reQiiUq8v09QeNBEw0JksT4BAaWiAx/miXpcLxJj5q7tTJ0mSN1JKi1ZKB2YVUEvFw6gx71eIi3SuJg5wMasOn5GkH4muZK+80UtJez1+8VkCA/Gix9HDR2OJShT/giyr3QDpt+JhS+afKVciKKxrRfBAqLp6cm1fjIj4V+9NHiuiILVz5NjGBd7tp9XmuHODUmlz2cLbRDJCSCS/Z8+gTAKZMoTBnYyTv/RKTFWlwoZtOBFnpXDBmM+PaFpDVld7yr4Fs0zpEM/1Udl1bALJO25xOY32t8ZAA4RhldMZjjEYYqyTyFYdwikppq06BLguz1zSAQlaQLW3fn3D5a8omV9931beGekx/p7yVMYTG3DEKuQsy4DBn8RzX0AYd5JCMSK+SdspcXYrIp5TQhsd8biYNy0cc7rr+k54M6X0vd8e9o1RJZW0J7PMsim5PZ7pSytGYRsJsZJHQ9DO47uxpmiUHFKWPgG2E5BgsxVqyXa5wBIwPz5/pNbV6Sgf2lHL2gnKNY6p/IN3gxDXf7I6MyNTp+NbYvChYGlxxaXKoB0ADBIpqE2Ryz0DTeav0HABxGiaPBb+z/valpuMk1P9mBmy4NtE7af6d2FIOP+rEI7ZSjbSAGUiLF4BbUAhJHAig+TnVqBN1kV0n3zAptgeOZrVzbH++cCxIaC1FSjPDtKTQNj63f1SK2L0m/5d5ftdJ5kcVr+2LdSkS36Io9Nge+oS4fXzcFyRp425M9TNWblSkBcW2ylA2V0qZKcgzDTvXEPjDY2XGZdkyVnRDSlO9ELiDl2k+3pXtaXeLc3U8x5W213pbPaYoT2EKZmHFYd1qS+vEL5udjkfdP+bikVpmj9b9gFO5ZIly3Q5w0xm+uPhXrlTO6SNPlj5f88xzMZe1Obb54CqC2pohuURsu0DTHUO0pOHK3m5Z4yZ2SdM5t8iDQlr7n9alh6C9LeUXySyRh7MguwhUGmVIqcSfQAnlhFxBz+Fbx0x4cQQiFMPB4cF7VfJBskvljkG8DCnnC9J13WXkNRN6qvK2HjN+dWQUdzpVcJpocfrq770E/wesgtm3CvOL3e2lmcaA/56042jq/wOj6FJKrUh6pTeMnrCSAS7aLAqBZcU4vmqRBOEPz0zkCOdvsN2mLeVHCmsIw1dhldYhzj/9D8sCwHCph9txUCH/BU3GqdyWH92i+yMQsU7ASXtI+tRMPihthzSZvi8MPToEe2ymfDFJjUNl9ctP6wEnrf41XWxXmUiZ6GLHG49QuR94l2poxTupcyFaNQU71BjPEo4x2tkZbz0L63iS26m+278hxplXR1iFhqqXm5Ee+/jpbJHTN3JC0ecv94tgy3Gm0CbxtYQBIKIPjzLxPtvpsC2ExxBmLVrxurhJ7qRkYB0+HJjnioR2kUAng5CQtIWa+rZhqDUCxtWS81ZenvFAsvu1HiCmhK2e5zWDbRRvjmaSaVkWlP406fd+K4lQL8vW2cGu9may2uW0HIkGTzqcYlZpCkdmBUIGKE8wXvJ6noeNsiWdHHSGpxb+YfdLHwd5+qm1FYiPfuZHhF0xxrosUCoDk+Ctu5bYXYIARgurbAArcG1KTDoymWfl6nKP6sIPNpW+NGhRchtRLy8eVb5p1d6sOAI9OgUkZ3Sj1jlr6FmHAimP4vlOcJr2obHUJdyGXRb+2vJxsMZXbrm3TfeUP5CUvaOmcwd4Z2DCsHsJjRXl9rPdsirpU25DjoYsA8x9JoYELMACuHovyxk5yKhpIUVVWklBjBD2z/NsSVItuVaCEEx/CwH2V3D6eBUzAiY1XGDSVg2xpiuYTVI9KQ3P5ftoqwV0oLypE2/6ZsIaeZd/RxEE6VTJ3a2pg28AHw4wwQzBBXoqJ+RLWDGo7JMfKXzlhAqJEm6N/K+mWjAafgrVagzfVPRMTpYUNdSxicPZBqfaJ1fJtD4LlK2W8PRnoBswrS63a0eWOyJukl2Az26gDgijEzi8qkVH811afdPweJ/GA+15sK/R3wNXV2a4JAl0FtQ/YoNRZvtXPGAlrq8dfCv4vXyNS+DOxvRBlh5X6F7ArWu300wX/zjZO7A1m3UHQFpJ8Zv7qzO7fHvxgZQwiqq1xNIOH16soT5s0X2hfIpCaeVMy1k+EDgvtZoGB/7uv9uJCVDeHwq5jAzd3Vkzp0xuvFqWwTm1khtcPSy1mfjMM832p8H7IorGWKkU5RQydtMe92Jk+t3uw0QlyeFXhbEqFpehAnPq1tSf1Hvse3QjwH8xUqorVAu3gq1BuvuW2zjyoIp0ZDsNKl+bDr+64I2kV2v8PS4RKQgCAmjzlK/bMaGeMJAjl1lRzJc5ts667ItjfJRLG9KjI2QyWylo7GfL53vFxB4Kyd2HwQE6hCfyLfMbJafvgfnGi6lDowNz2O4SIaO+52m0yYWQMhgJ1og5ZD0o4xA0JIQ6eh+5v53IUrqr46jjDuLXsADUsdXnzt2KFMjayArD8kGmVzfLTiCXwxIpUExfQ4CR4jGi19DKUkeS1S1ckKzr19Tni/nqf0tYKf7I4NBOVBNln29MCq8NigJTq2/zHqVK8N25nQ1AyRSB+U2lGcgcduQitJUSvvG+oG8Hq1ZUQWbBAeaW9qGum2YsiFv6cRtk/N327e1Iy8Mien9kVOrZeNIFL1JkfdcdYp3Lsy77v1YadljGKj6e/je53fMSSjw9xaqSMTC+6gSSOPaXPfYoc5HFU1R+PA6DxhcKWE8NeFad46wkkoQz+azoYsvzN0mG5vg5WV81i4ImRIxC9c+hXa0MuLCbXpRoDDKYW0PFWrPfq8nwA9WEKZHJ9sMUcYekS2twuyT/stIY2263UKPYuGdSHx0H0XgXdVyklls6svV1d4feB5GIU39Hx8nHuLpaNiAkmRscPV6FHe5yW7QW3mnQf9QdSwaN0gPgxiNfnAecnxRDenNgw0O3jlPAIt703+9jGrN6Jk/7bjPTvugXaH8hZ/iJuXK7P55h1e/8Ji/7eCWQbcdQ/SBLBJ+vLeIzXz3tlvjokuOyGLKdxZhCk8lDEmRxkoyy/uxZivmTtD960Ol4J7PZIjyfkhXi+uVCqOKhR4h4v/kT2b4278oRPlRILDd/Sc510ejH5D7goVlJmdGFkKLPmu6NkA2GTJ/kA1WWtyWHH+e+AQCV9RUM10FZoThh4TjU+6l4hTbuVLXgmRoyWYbEG67rZdSKW0fK2OidZcYjybfaiNYecnhKTZ5TzIuLZG8+x7cPGyFLvSvDJ5DlhPzSEbSy1z+bP/NuDDG2I6TKPKmedRnooq5aQGtExLkXjxyzG8Tt/tnxidqpOPRDq8NhbwRGVpFHvf25+QYdhLPT5m0lnRYm0SFAXaJgmDGRRfK5m0kfcAiGGn5KN9k8gjtVV4npCsglS92Z34Yd5HrgRv3GpxXtvSBlqfm4T7g2e1rXdiOHBPHPpqRHROfRrjTkqJh8eoMrp1GlpkQU5M+JYYnkDur2zv5/pQcg8ckBLUUCjwUrvye9M3RwOPes4j9ARiDDkiiXauMsm7roH/eojq1nC74L96ud8+ZrSyeHcBs+WetIqDWinTys2oRkptOukplU94vVpN3tSI20pVCPoRcgIt+Oyb56h07837TdNwKuut1UyD+b98XLngFUAvHM2QqTcF9MDZ4HUCf2g4RD7Nk8ir6XGnMe7adCly0/ej/qu2RZeoeAJjao50iJKUzv1iLbwccci5OcrjknH9oEaU3AGkuhaJmEJxc1QP3jYgyZ79Uyf12jA9jm5/2h1uW3jOYVhdl7AUetS22+GZ8h/c8bn9USKqefNMgo26XySqAdjeJJ60zGc8TXgwVZncLF+aAtfNJPSGsOhjqBfPZ9sx6cfDS2hFOSRnXvcSpGd94paHJTPHuY1JVdEv/kEGY/Kfin63SI4Cn7VQXjknOaXPWOF+9kleRsVhNDPigqFY4M/dtBfXpzuB2RH4UZdCm0x1R+A/JgytB6aqXUIgECASnCRQ0oQo/c/MbHJ78h5U2n1zhqstQh/ubcAQLLgaCjAjoy2m2p2tRvOE4ke8C9fC+ZkK6IRdob4mwbRgop+IpBRZ5Cu4wZuGN+1g/XrW/UVK7kVrBKom67thDxD56AFT94gow03u2DPB/6A0sLFCVvzRZ4nSneMOW6peVcVmU7lXQY2Mu30yyg6WmeWMMDvyEHOZ5mZ5c5hLx3/AFEJdeKLjPOGErrxmC93HO9B9b7L9YOhmv+pYp+Ja+O9W2cW9RyP9ABSNOjZ1nms6JWjyLfY96xfrUAtFsDZD3xK1i1WTOK9aVMZHkovF8ePf5v3Pvpd/veMD4TownKXvp7+fnWuDMTHp+pwnGcecnuOSelQ/U1rOqvjNkUpdACtHhZD1tvtCZ9v36Epjpef8/fSBdK3Zo393B2OeN11h+yMt5BkLL7d+21mRdU+cnGIqLvbpo0riiNG6J3MfTFa/QJ1C217Lszxf2w3w0qptwd2S1sXO/jnCtXYdjJr084tyGkvA1izYs50r2gT/9AAKsHGgAChPwqt6mi9vsJS2OlyXzAN5OOUFc5H6JY6NqZqEpxbyTISfenPefyTgwJ27wzmVH3Fa3lZxNRAxOh6a6hMa0ITiUluIKd9Q+e1WgpbfSUESW/2fEq6HM/vaxnrBXeerpzvfun4fAlzsTMQAbViXangRmBfrV9pXNxcm1Kn3byxgcg9qCVa1unc2rLEv7CR9K/41Z5o+loOFAysxlIKkb79cgwoaYBGyu7BtVRXnmQnZwobPRiet5xnwHybmuqWGnjlep2TqLdnbfopStjVP3BEHDfxf6JJ/HO2+sY/uT8Fl7Xv1qjt6E9oqDWdCQ+b2Xv9hPveEjnUddWGs94MWLXZHlzEyTUjwBK2yl6nhdsp1e5r41zCRrBGw1qvFNLepA/5Ikko9EBQFFZhlKhvPiy+B6Mc7BX3L7nKdLepKt9PhJrpAbQGySv44uuGHnVTveaTnrGn9gMMd1JeFaLXXQ9SeGj+cRu8GKNronV2dmJApgg24IUpsun9F7Co9Ina108U3Kc1hnjCxAs6D/OYkNosdenOc4SYJ6D+6mu6XrUhNOYcinsu6k3J30/9yN7o/cADdoQVZUZIPv90Ra5i7FtI4HgDXKWrpgffmKvYevTQQyitbjYdnai8w2Z+scgzc2bvEIAcrhpUWErNYyL70ey28OJDImqsXfvQCAU3ah5nm8AAFD6cBdZKUWgJaXnh20PYsnaDDUUiecmGr6EkFzJWCALX1SYCLgSHQ9ChNU4xdEkodzTBEb1JxxDInC6rpKgKTWeGYchOve4bYfFbiox1HCHs864WJWsJw+gErrdHX798bFikTCuuYSwaA9dTxEkkguTU/JFWEtk7Oe9FGlErnghj7ZjU9IIiaZzQCKlyLyXZD4xDbAU6SdmjBdU4RRULqXn1Kiej4mBPevTOPYxKfJNj/gcebLispin8qOXXmaOLkp75ygDcD6jc7AC+tDt4sHwzd55q9HQC7fg9fKy5PIB8/raCTuaCREjrQTW2c3sW46Ermso9P78XSTnDQoFqk1kAfLpN8sHWUBMfMj/ALbkUFK7lqmxsUJsK2xhwR6r/9k24CPvWkhxAR0LxOGR/V0uNGLD54epSPe+Zo2uAY+I7YJUSz12G8Ow0uZERTkejA7uKa471EqPM9YIXuJr316OP8StbMcIAtIFvMCZRfj+BnGr/vrJjTwhAzMStRFcQINKOLHo1OuoJ5+els+ususZiGqAWPvc96ioKbOeRYPMnYnU8K5Ih1aO8MupGNu9hwK/Iibpo7B78+yeIvHEbTfDQBnTz4FkShbdlpYxSqWN0K2a6P+eio/u85OsDqWj+zdzr0eeZ8xdkPWOxnaZY02x4K3nSSdoAs0irt2WdfIGEsPw6Gw/Zn1M1yL62kd8Ti4q1vVd51sqGx21jmgl/LHHBxd9Lf1pj2HBXyb0GZNKb9pK57zdcoNDxxloM/CH9WRuDUQqkRpqR8+xsWcwd06KPhurfkZH0wbaS2eMOiXgDvYpUz5/KTsB4DprA2MNWzhqjrC0jzYQVg7Sv/YUHC0jXnuEVUhPaZIMj9hbLp4Oy//OOiI8ixWo3gc0APRkcbu3SvjJ2F4ZtVBkf74bUgOrBISiWClT8Yo74yt4eUT3ehoGUGotrdBte9YEtPu8UewG43Oev9fKVcF9mM5ZQVE68bcrM8ErADSlVAfwdzfBF67mziy5hQtOsqLTRLI0iWDtKsule5gCCoTvtyhj4c5iDhvi1oAfelVWeQNr3ZA/ztU2H9RmkSCxbBg22XRxXzc6Ma7dLXjO1nAp0VSCWEmMbUZnURf4+0B7kkKwPIN8XT2tIff5KKSE8L21uHBiO87R26g17bo816QXN9PeYpcbtGJwpex5rscXZHeGJZiGVUdfL+xQKN/dVJy6UV0ziIAZc94ovzvrhEjbrZKISQBv+CD5G4ijvboTZ9fSqQmNgtobOfcQ5VvH/iXA5+E/JjcPnf34YZT8ozDIYY7rKpJJVboJLbTrvjmS7DyCa2+GAt+4PA7W+WHxSvkVquUUKGftio1Xy2smTwfZYiefMyb/IzNUIdLTl/EwuBntC7FJ+cngZVHhXVoNBLleGLe5xmD0fN7qRi8JVWDjDtL2zZ9zP8+S37pUIK+HPYAVr696i/QKh39INnyc6iUR7eYUBcTjpHJiJPP4XIqJoSew7jy1bp8e9QaVjVJoSGGYIy9v9Zo8bkCULqMvdN5PPLVV9TQRmfh0p4osLwUN7EJk+RmkW6Q+iUY8Wia/Cmf/JcmesY6BYISrhcYoWJCJL2i1JxoUlAIF+qQx09ahO4tN4D3TiE2TAN2UQUB7EhOkrBMyZUV9n4L1c+6WPlGg6eC2QYhisma+0zhR5ex2+s25/kpudJo/hDjc9tdcHN8hiAfmNQ01fCeWRBzaXJJbN0bAH+BGD5hx2frgAAdDbs6TpR8GgR0/6KXvLyu6CH5mJ5bymYlHefhl8ozjL/YQC9tUGEDON5wNyJXmuatKsjkJIK9gpCti+H4e91kqr8m72XMeDQkUW/rqD9mgeORPOBbkZWVe6NBbgNCROp4fQQAulx0naPzCKw7Z/XfxKYFfp+qEmo4AAAAAAio4MpoS+W9He1Zm7pGrM+w9RDQfiXfZg8sZnqkd0XJPQqrYiowHYqsAPKjZvoXlvXLF3qIyOYrBjD/ZPqfgCz32VuZEoi1C2qUcZpuVGAAlp5qImnyKnhE85aHscT6jmDxxWZXcYndxXa4O6fYYNu3fQ/op7QfVXKSK31neS9GUWztlbEd4doCw+deMfpaE1DWmycJ1hsFJ/tyIB3KrCkMCOBfLdvty1cHsJS0jvAH3qkQ5Dk//bgWj2j1cCnVox3qL+hRbdkkWq+vAvUmosGQpnvP87Z4wlcb2RVNVlFUmebBgZKhPzNa7gHgfgMv5EXZgYYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=`
