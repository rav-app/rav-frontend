import { MockHorizonContext } from 'apprise-frontend-core/client/mockhorizon'
import { useMockery } from 'apprise-frontend-core/client/mocks'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { PropsWithChildren, useContext } from 'react'
import { useRecordMockery } from './mockery'


export type RecordProps = PropsWithChildren<Partial<{
   
    mock:boolean
  
  }>>


export const Records = (props:RecordProps) => {

    const beforeHorizon = useContext(MockHorizonContext)

    const { children, mock=beforeHorizon} = props

    const mockery = useMockery()
    const recordMockery = useRecordMockery()

    const activate = () => {

        if (mock)
            mockery.initMockery(recordMockery)

    }

    const { content } = useRenderGuard({

        render: children,
        orRun: activate
    })

    return content
 

    
}