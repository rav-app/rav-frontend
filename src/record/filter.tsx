
import { RecordIdIcon, RecordStateIcon, RecordValidationIcon, VesselIcon } from '#record/constants'
import { RavRecord, RecordState } from '#record/model'
import { DetailContext } from '#submission/context'
import { Submission } from '#submission/model'
import { ValidationState } from '#submission/validator'
import { useVesselOracle } from '#vessel/oracle'
import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { Label } from 'apprise-ui/label/label'
import { OptionMenu } from 'apprise-ui/optionmenu/optionmenu'
import { ContextualFilterProps, useFilterState } from 'apprise-ui/utils/filter'
import { useCallback, useContext, useMemo } from 'react'
import { RecordPatch } from './model'
import { useRecordOracle } from './oracle'


type StateFilterProps = ContextualFilterProps & {

    options: RecordState[]
    data: RecordPatch[]
    initial: RecordPatch[]
}

export const useStateFilter = (props: StateFilterProps) => {

    const { context, contextKey = 'recordstate', options, data, initial = data } = props

    const t = useT()

    // all the distinct states found in the base population.
    const distinctStates: RecordState[] = useMemo(() =>

        utils().dedup(initial.flatMap(r => r.lifecycle.state ?? [])).sort()

        // if the base population changes, we see what's actually used.
        , [initial])


    const state = useFilterState<RecordState[]>(context)

    const active = state.get(contextKey) ?? distinctStates
    const setActive = state.set(contextKey)
    
    const StateFilter = (

        <OptionMenu key={distinctStates.length} active={active} setActive={setActive} label={<Label icon={<RecordStateIcon />} title={t('rec.state_col')} />}>

            {options.map(opt => {

                const title = opt ? t(`rec.state_${opt}`) : t('rec.no_state')

                return <OptionMenu.Option
                    key={opt}
                    disabled={!distinctStates.includes(opt)}
                    value={opt}
                    title={<Label noIcon title={title} />} />
            }
            )}
        </OptionMenu>
    )

    const statefilter = useCallback(

        (s: RecordPatch) => active.some(a => a === s.lifecycle.state)

        // eslint-disable-next-line
        , [active])  // recompute when selection changes

    const filteredData = useMemo(

        () => data.filter(statefilter),

        [data, statefilter])


    return { StateFilter, statefilter, filteredData, active, setActive }
}


type ValidationFilterProps = ContextualFilterProps & {

    submission: Submission,
    data: RecordPatch[],
    initial: RecordPatch[]
}


type ValidationOptions = ValidationState | 'same' | 'flagged'

const validationOptions: ValidationOptions[] = ['valid', 'invalid', 'flagged', 'same']

export const useValidationFilter = (props: ValidationFilterProps) => {

    const t = useT()

    const { submission, context, contextKey = 'submissionvalidation', data, initial } = props

    const oracle = useRecordOracle().given(submission)

    const { sameRecords = [] } = useContext(DetailContext)

    const report = submission.validationReport

    const existingOptions: ValidationOptions[] = useMemo(() => {

        const opts: ValidationOptions[] = utils().dedup(initial.map(r => oracle.fullValidationStatusOf(r))).sort()

        if (sameRecords.length)
            opts.push('same')

        return opts

        // eslint-disable-next-line
    }, [report, initial, sameRecords])

    const state = useFilterState<ValidationOptions[]>(context)

    const active = state.get(contextKey) ?? existingOptions

    const setActive = state.set(contextKey)

    const ValidationFilter = (

        <OptionMenu key={`${sameRecords.length}-${existingOptions.length}`} active={active} setActive={setActive} label={<Label icon={<RecordValidationIcon />} title={t('rec.valid_col')} />}>
            {validationOptions.map(opt => {

                const title = opt ? t(`rec.validation_${opt}`) : t('rec.no_validation')

                return <OptionMenu.Option
                    key={opt}
                    disabled={!existingOptions.includes(opt)}
                    value={opt}
                    title={title} />
            }
            )}
        </OptionMenu>
    )

    const validationfilter = useCallback(

        (r: RecordPatch) => {


            const haveRecords = !!report?.records.hasOwnProperty(r.id)

            const status = oracle.fullValidationStatusOf(r)

            return !haveRecords || active.some(a =>

                (a === 'same' && status === 'valid' && sameRecords.includes(r.id))

                ||

                a === status)
        }
        // eslint-disable-next-line
        , [report,active])  // recompute when selection changes

    const filteredData = useMemo( ()=>
    
        data.filter(validationfilter)

     // eslint-disable-next-line
    ,[data, validationfilter])

   

    return { ValidationFilter, validationfilter, filteredData, active, setActive }
}


type IdFilterProps = ContextualFilterProps & {

    submission: Submission<RecordPatch>
    data: RecordPatch[],
    initial: RecordPatch[]
}



export const useIdFilter = (props: IdFilterProps) => {

    const t = useT()

    const { context, contextKey = 'submissionid', data, initial, submission } = props

    const oracle = useRecordOracle().given(submission)

    const matchReport = submission.matchReport ?? {}

    const selectableOptions: string[] = useMemo(() => {

        return utils().dedup(initial.map(oracle.matchStatusOf))


        //eslint-disable-next-line
    }, [submission, initial])


    const filter = useFilterState<string[]>(context)

    const active = filter.get(contextKey) ?? selectableOptions
    const setActive = filter.set(contextKey)

    const IdFilter = (

        <OptionMenu key={selectableOptions.length} active={active} setActive={setActive} label={<Label icon={<RecordIdIcon />} title={t('rec.id_filter')} />}>

            {['none','pending', 'new', 'matched'].map(opt => {

                const title = t(`rec.id_filter_${opt}`)

                return <OptionMenu.Option
                    key={opt}
                    disabled={!selectableOptions.includes(opt)}
                    value={opt}
                    title={title} />
            })}
        </OptionMenu>
    )

    // filter changes with selection and match report
    const idFilter = useCallback((r: RecordPatch) =>

        active.includes(oracle.matchStatusOf(r))

        //eslint-disable-next-line
        , [active, matchReport])


    // results changes with data and filter
    const filteredData = useMemo(() => data.filter(idFilter), [data, idFilter])


    return { IdFilter, idFilter, filteredData, active, setActive }

}



type ClaimFilterProps = ContextualFilterProps & {

    submission: Submission<RecordPatch>
    data: RecordPatch[],
    initial: RecordPatch[],
    currentRecords: Record<string,RavRecord>
}


const claimOptions = ['claimed','own','foreign']

export const useClaimFilter = (props: ClaimFilterProps) => {

    const t = useT()

    const { context, contextKey = 'recclaim', data, initial, submission, currentRecords } = props

    const oracle = useVesselOracle()

    const isForeign = (p:RecordPatch) => p.uvi && p.tenant && currentRecords[p.uvi]?.tenant && currentRecords[p.uvi]?.tenant !== p.tenant
    const isClaimed = (p:RecordPatch) => isForeign(p) && oracle.canClaim(currentRecords[p.uvi])
    const status = (p:RecordPatch) => isForeign(p) ? isClaimed(p) ? 'claimed' : 'foreign' : 'own'

    const selectableOptions: string[] = useMemo(() => {

        return utils().dedup(initial.map(status))


        //eslint-disable-next-line
    }, [submission, initial, currentRecords])


    const filter = useFilterState<string[]>(context)

    const active = filter.get(contextKey) ?? selectableOptions
    const setActive = filter.set(contextKey)

    const FlagFilter = (

        <OptionMenu key={selectableOptions.length} active={active} setActive={setActive} label={<Label icon={<VesselIcon />} title={t('rec.claim_filter')} />}>

            {claimOptions.map(opt => {

                const title = t(`rec.claim_filter_${opt}`)

                return <OptionMenu.Option
                    key={opt}
                    disabled={!selectableOptions.includes(opt)}
                    value={opt}
                    title={title} />
            })}
        </OptionMenu>
    )

    // filter changes with selection and match report
    const flagFilter = useCallback((r: RecordPatch) =>

        active.includes(status(r))

        //eslint-disable-next-line
        , [active, initial, currentRecords])


    // results changes with data and filter
    const filteredData = useMemo(() => data.filter(flagFilter), [data, flagFilter])


    return { FlagFilter, flagFilter, filteredData, active, setActive }

}

