import { RecordPatch, RecordSlot } from '#record/model';
import { TagReference } from 'apprise-frontend-tags/tag/model';
import { delistingType } from './constants';


export type DelistingPatch = RecordPatch<typeof delistingType>


export type Delisting = RecordSlot & {

    reason: TagReference


    date: string
    note: string

}
