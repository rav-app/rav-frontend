import { appColor, AppIcon, HomeIcon } from '#constants'
import { useCrumbs } from '#crumbs'
import { RavHeader } from '#header'
import { useHelpDrawer } from '#help'
import { IOTCLink } from '#iotc'
import { SearchIcon } from '#search/constants'
import { SubmissionIcon } from '#submission/constants'
import { SubmissionRouter } from '#submission/router'
import { submissionRoute } from '#submission/routing'
import { RecordRouter } from '#vessel/router'
import { recordRoute } from '#vessel/routing'
import { useT } from 'apprise-frontend-core/intl/language'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { useIamSections } from 'apprise-frontend-iam/sections'
import { tenantType } from 'apprise-frontend-iam/tenant/constants'
import { useTenantCrumbs } from 'apprise-frontend-iam/tenant/crumbs'
import { useUserCrumbs } from 'apprise-frontend-iam/user/crumbs'
import { StreamBindingProvider } from 'apprise-frontend-streams/track/state'
import { tagType } from 'apprise-frontend-tags/constants'
import { useTagCrumbs } from 'apprise-frontend-tags/crumbs'
import { useTagSection } from 'apprise-frontend-tags/section'
import { RoutableTargets } from 'apprise-ui/link/routabletargets'
import { Scaffold, Section, SideLink } from 'apprise-ui/scaffold/scaffold'
import { Home } from './home'


export const RavScaffold = () => {

    const t = useT()

    const crumbs = useCrumbs()

    const oracle = useTenancyOracle()
    
    const noTenant = oracle.hasNoTenant()

    const excludes = noTenant ? [] : oracle.managesMultipleTenants() ? [tagType] : [tenantType, tagType]

    const tagSection = useTagSection()

    return <RoutableTargets exclude={excludes}>

        <StreamBindingProvider>

            <Scaffold icon={<AppIcon />} color={appColor} title={t('rav.title')} sidebar header={<RavHeader />}>

                {useTagCrumbs()}
                {useUserCrumbs()}
                {useTenantCrumbs()}

                {crumbs}

                <Section icon={<HomeIcon />} title='Home' exact route='/'>
                    <Home />
                </Section>

                <Section icon={<SubmissionIcon />} title={t('sub.plural')} route={submissionRoute}>
                    <SubmissionRouter />
                </Section>

                <Section icon={<SearchIcon />} title={t('search.title')} route={recordRoute}>
                    <RecordRouter />
                </Section>

                {useIamSections()}

                {noTenant && tagSection}


                <SideLink>
                    <IOTCLink />
                </SideLink>


                <SideLink>
                    {useHelpDrawer()}
                </SideLink>

            </Scaffold>
        </StreamBindingProvider>

    </RoutableTargets>

}


