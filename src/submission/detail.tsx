

import { MatchedRecordIcon, RecordValidIcon } from '#record/constants'
import { useMode } from 'apprise-frontend-core/config/api'
import { useT } from 'apprise-frontend-core/intl/language'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { TenantLabel } from 'apprise-frontend-iam/tenant/label'
import { UserLabel } from 'apprise-frontend-iam/user/label'
import { Button } from 'apprise-ui/button/button'
import { ButtonMenu } from 'apprise-ui/button/buttonmenu'
import { LabelRow } from 'apprise-ui/label/label'
import { LifecycleSummary } from 'apprise-ui/lifeycle/lifecyclesummary'
import { NoSuchRoute } from 'apprise-ui/link/nosuchroute'
import { RouteGuard } from 'apprise-ui/link/routeguard'
import { Page } from 'apprise-ui/page/page'
import { SidebarContent, SidebarProperty } from 'apprise-ui/page/sidebar'
import { Column, Table } from 'apprise-ui/table/table'
import { Tab, useTabs } from 'apprise-ui/tabs/tab'
import { Titlebar } from 'apprise-ui/titlebar/titlebar'
import { Topbar } from 'apprise-ui/topbar/topbar'
import { AddIcon, DotIcon, DownloadIcon, EditIcon, ListIcon, OpenIcon, RemoveIcon, RevertIcon, SaveIcon } from 'apprise-ui/utils/icons'
import format from 'date-fns/format'
import formatDistance from 'date-fns/formatDistance'
import { cloneElement, useContext, useMemo } from 'react'
import { useParams } from 'react-router-dom'
import { useAssetResolver } from './assetresolver'
import { SubmissionAssets } from './assets'
import { AssetIcon, SubmissionPublishedIcon, SubmissionRejectedIcon, SubmissionRestoreIcon, SubmissionSubmittedIcon, assetTab, generalTab, recordTab } from './constants'
import { DetailContext } from './context'
import { DownloadPanel } from './downloadpanel'
import { useDetailEffects } from './effects'
import { useSubmissionFields } from './fields'
import { SubmissionLabel } from './label'
import { Asset, Submission, useSubmissionModel } from './model'
import { useSubmissionOracle } from './oracle'
import { GeneralForm } from './profile'
import { DetailProvider } from './provider'
import { RecordIssues } from './recordissuepanel'
import { SubmissionRecords } from './records'
import { RejectPanel } from './rejectpanel'
import { ResourceIssues } from './resourceissuepanel'
import { submissiondDetailParam, useSubmissionRouting } from './routing'
import { SubmissionStats } from './stats'
import { useSubmissionStore } from './store'


export const SubmissionDetail = () => {

    const { id } = useParams<{ id: string }>()

    const store = useSubmissionStore()
    const routing = useSubmissionRouting()

    // keeps generated sub until id changes, to stabilise eagel;y minted identifier.     
    // stabilise the new submission we pass down, so we can tell reliably when it changes.
    // eslint-disable-next-line 
    const newSubmission = useMemo(routing.nextSubmission, [id])

    const submission = id === 'new' ? newSubmission : store.lookup(id)

    if (!submission)
        return <NoSuchRoute backTo={routing.listRoute()} />

    return <DetailProvider submission={submission}>
        <DetailPage />
    </DetailProvider>
}


const DetailPage = () => {

    const t = useT()

    const mode = useMode()

    const logged = useTenancyOracle()
    const oracle = useSubmissionOracle()

    const { edited, dirty, set, isNew, readonly } = useContext(DetailContext)

    const btn = useButtons()

    const assetResolver = useAssetResolver(edited.type)

    const restoreAssetReferences = (assets: Asset[]) => {

        set.using(s => assetResolver.restore(s.records, assets))
    }



    // tabs

    // operation tab icon gets error/warning marker.
    const recordTabIcon = edited.stats.errors > 0 ? <DotIcon color='orangered' /> : edited.stats.warnings > 0 ? <DotIcon color='orange' /> : <ListIcon />


    const { tabs, selectedTabContent } = useTabs([

        <Tab defaultTab icon={<EditIcon />} id={generalTab} label={t("sub.general_tab")}>
            <GeneralForm />
        </Tab>,

        <Tab enabled={!!edited.records?.length} icon={recordTabIcon} id={recordTab} label={t("sub.record_tab")}>
            <SubmissionRecords />
        </Tab>,

        ...(oracle.canUploadAssets(edited) && !edited.live ? [

            <Tab enabled={!!edited.records?.length} icon={<AssetIcon />} id={assetTab} label={t("sub.asset_tab")}>
                <SubmissionAssets onRemove={restoreAssetReferences} />
            </Tab>

        ] : [])

    ]
        , { param: submissiondDetailParam })


    let title: React.ReactNode = isNew ? t('sub.no_title') : t(`${edited.type}.title`)



    return <Page readonly={readonly} defaultOpen={mode.development}>

        <RouteGuard when={dirty} />

        <Titlebar title={title}>
            <LabelRow mode='tag' bare noTruncate>
                <SubmissionLabel displayMode='stateaccented' submission={edited} />
                {logged.managesMultipleTenants() && !!edited.tenant && <TenantLabel tenant={edited.tenant} />}
                <SubmissionLabel displayMode='start' bare submission={edited} />
            </LabelRow>
            <SubmissionStats sub={edited} />
        </Titlebar>

        <Topbar tabs={tabs}>
            <ButtonMenu>
                {btn.save}
                {btn.revert}
                {btn.validate}
                {btn.match}
                {btn.submit}
                {btn.submitForApproval}
                {logged.hasNoTenant() && btn.rejectOrRestore}
                {logged.hasNoTenant() && btn.publish}
                {btn.remove}
                {btn.download}
                {btn.add}
            </ButtonMenu>
        </Topbar>


        <SidebarContent>

            {btn.save}
            {btn.revert}
            {btn.validate}
            {btn.match}

            <br />

            {btn.submit}
            {btn.submitForApproval}
            {logged.hasNoTenant() && btn.publish}

            <br />

            {logged.hasNoTenant() && btn.rejectOrRestore}
            {btn.remove}

            <br />

            {btn.download}

            <br />

            {btn.add}

            <br />

            {btn.blockingErrorCount}

            <br />

            {btn.resourceIssues}
            {btn.recordIssues}

            <br />

            {useLifecycle()}

            <br />

            <SidebarContent.Affix>
                {useSideList()}
            </SidebarContent.Affix>

        </SidebarContent>

        {selectedTabContent}


        <ResourceIssues />
        <RecordIssues />
        <DownloadPanel />
        <RejectPanel />

    </Page>
}


const useSideList = () => {

    const model = useSubmissionModel()
    const store = useSubmissionStore()
    const routing = useSubmissionRouting()

    const { edited, isNew } = useContext(DetailContext)

    const tenant = edited.tenant

    const allSubmissions = store.all()

    const sameTenant = useMemo(() => isNew ? allSubmissions :

        allSubmissions.filter(s => s.tenant ? s.tenant === tenant : s).sort((a, b) => (b?.lifecycle?.created ?? 0) - (a?.lifecycle?.created ?? 0))

        , [isNew, allSubmissions, tenant])


    return <Table.Sider<Submission> name={`sub-${tenant}`} key={edited.id} data={sameTenant} mountDelay={0}>

        <Column text={s => format(s.lifecycle.created!, 'PPP')} render={(sub: Submission) => {

            const Icon = model.validationAwareIconOf(sub)

            return <SubmissionLabel icon={<Icon />} stateDecoration
                linkTo={({ search }) => `${routing.detailRoute(sub.id)}${search}`}
                dateMode='short'
                highlighted={sub.id === edited.id}
                submission={sub} />
        }

        } />
    </Table.Sider>
}


const useButtons = () => {

    const t = useT()

    const model = useSubmissionModel()
    const routing = useSubmissionRouting()
    const oracle = useSubmissionOracle()

    const { edited, dirty, isNew, machine, recordIssueKit, resourceIssueKit, downloadKit, rejectKit, patchRowsInFocus } = useContext(DetailContext)

    const effects = useDetailEffects()

    const fields = useSubmissionFields()

    const resourceIssues = model.resourceIssues(edited).length
    const resourceErrors = model.resourceIssues(edited, { type: 'error' }).length

    const saveErrors = fields.errors() + resourceErrors
    const recordIssues = model.unresolvedIssues(edited)
    const blockingErrors = saveErrors + recordIssues

    const records = edited.records ?? []

    const allRecordsIgnored = edited.stats.ignored === records.length
    const allRecordsRejected = (edited.stats.ignored + edited.stats.rejected) === records.length


    const addSubmission = () => routing.routeToNewDetail()

    const addBtn = < Button noReadonly icon={<AddIcon />} enabled={oracle.canAddNew()} disabled={isNew || dirty} onClick={addSubmission} >
        {t("sub.add", { singular: t('tenant.singular') })}
    </ Button>

    const removeBtn = <Button noReadonly enabled={oracle.canRemove(edited)} icon={<RemoveIcon />} disabled={dirty || !edited.lifecycle.created} onClick={effects.remove} >
        {t("sub.remove")}
    </Button>

    const rejectOrRestoreBtn = edited.lifecycle.state === 'rejected' ?

        <Button noReadonly enabled={oracle.canRestore(edited)} icon={<SubmissionRestoreIcon />} disabled={dirty} onClick={effects.restore} >
            {t("sub.restore")}
        </Button>

        :

        <Button noReadonly enabled={oracle.canReject(edited)} icon={<SubmissionRejectedIcon />} disabled={dirty} onClick={rejectKit.toggle} >
            {t("sub.reject")}
        </Button>

    const saveBtn = <Button icon={<SaveIcon />} dot={saveErrors > 0} enabled={dirty} disabled={saveErrors > 0} onClick={effects.save}>
        {t("sub.save")}
    </Button>

    const revertBtn = <Button icon={<RevertIcon />} enabled={dirty} onClick={effects.revert}>
        {t("sub.revert")}
    </Button>


    const validateBtn = <Button enabled={oracle.canValidate(edited)} icon={<RecordValidIcon />} onClick={() => machine.trigger('validate')} >
        {t("sub.validate")}
    </Button>

    const matchbtn = <Button enabled={oracle.canMatch(edited)} icon={<MatchedRecordIcon />} onClick={() => machine.trigger('match')} >
        {t("sub.match")}
    </Button>


    const submitBtn = <Button enabled={oracle.canSubmit(edited)} dot={blockingErrors > 0} disabled={dirty || blockingErrors > 0 || allRecordsIgnored} icon={<SubmissionSubmittedIcon />} onClick={effects.submit} >
        {t("sub.submit")}
    </Button>

    const submitForApprovalBtn = oracle.canSubmitForApproval(edited) && <Button dot={blockingErrors > 0} disabled={dirty || blockingErrors > 0 || allRecordsIgnored} icon={<SubmissionSubmittedIcon />} onClick={effects.submitForApproval} >
        {t("sub.submit_approval")}
    </Button>

    const publishBtn = <Button enabled={oracle.canPublish(edited)} disabled={dirty || saveErrors > 0 || allRecordsRejected} icon={<SubmissionPublishedIcon />} onClick={effects.publish} >
        {t("sub.publish")}
    </Button>

    const downloadBtn = <Button noReadonly icon={<DownloadIcon color='lightseagreen' />} enabled={!!patchRowsInFocus?.length} onClick={downloadKit.toggle}>
        {t("sub.download_N_records", { count: patchRowsInFocus?.length ?? 0 })}
    </Button >

    const primaryBtn = dirty || isNew ? saveBtn
        : oracle.canSubmitForApproval(edited) ? submitForApprovalBtn
            : oracle.canSubmit(edited) ? submitBtn
                : oracle.canPublish(edited) ? publishBtn
                    : addBtn


    const primaryOf = (btn: false | JSX.Element) => primaryBtn && primaryBtn === btn ? cloneElement(btn, { type: 'primary' }) : btn

    const blockingErrorCount = blockingErrors > 0 ?

        <SidebarProperty type='error'>{t('common.total_error_count', { count: blockingErrors })}</SidebarProperty>
        :
        <SidebarProperty>{t('common.no_errors')}</SidebarProperty>


    const resourceceIssuesBtn = <Button enabled={resourceIssues > 0} icon={<OpenIcon color="deepskyblue" />} onClick={resourceIssueKit.toggle} >
        {t("sub.resissues_open", { count: resourceIssues })}
    </Button>

    const validationReport = edited.validationReport

    // currently selected, or filtered, or all (memo because operations can grow large)
    const focusedRecordsIssueCount = useMemo(

        () => patchRowsInFocus.reduce((acc, e) => acc + (validationReport?.records[e.id]?.length ?? 0), 0)

        , [patchRowsInFocus, validationReport?.records])

    //buttons

    // currently selected, or filtered, or all.
    const recordIssuesBtn = <Button noReadonly enabled={!!patchRowsInFocus.length} icon={<OpenIcon color="deepskyblue" />} onClick={recordIssueKit.toggle} >
        {t("sub.recissues_open", { count: focusedRecordsIssueCount })}
    </Button>

    return {
        add: primaryOf(addBtn),
        save: primaryOf(saveBtn),
        remove: removeBtn,
        revert: revertBtn,
        submit: primaryOf(submitBtn),
        submitForApproval: primaryOf(submitForApprovalBtn),
        publish: primaryOf(publishBtn),
        validate: validateBtn,
        match: matchbtn,
        download: downloadBtn,
        rejectOrRestore: rejectOrRestoreBtn,

        blockingErrors,
        blockingErrorCount,
        resourceIssues: resourceceIssuesBtn,
        recordIssues: recordIssuesBtn
    }
}


const useLifecycle = () => {

    const t = useT()

    const { edited } = useContext(DetailContext)

    return <LifecycleSummary lifecycle={edited.lifecycle}>

        {edited.lifecycle.submitted && <>

            <SidebarProperty name={t("sub.lifecycle_submitted")} tip={format(edited.lifecycle.submitted, 'PPpp')}>
                {formatDistance(edited.lifecycle.submitted, new Date(), { addSuffix: true })}
            </SidebarProperty>

            <SidebarProperty name={t("sub.lifecycle_submittedby")}>
                {<UserLabel noReadonly user={edited.lifecycle.submittedBy!} />}
            </SidebarProperty>
        </>}

        {edited.lifecycle.rejected && <>

            <SidebarProperty name={t("sub.lifecycle_rejected")} tip={format(edited.lifecycle.rejected, 'PPpp')}>
                {formatDistance(edited.lifecycle.rejected, new Date(), { addSuffix: true })}
            </SidebarProperty>

            <SidebarProperty name={t("sub.lifecycle_rejectedBy")}>
                {<UserLabel noReadonly user={edited.lifecycle.rejectedBy!} />}
            </SidebarProperty>

        </>

        }

        {edited.lifecycle.published && <>

            <SidebarProperty name={t("sub.lifecycle_published")} tip={format(edited.lifecycle.published, 'PPpp')}>
                {formatDistance(edited.lifecycle.published, new Date(), { addSuffix: true })}
            </SidebarProperty>

            <SidebarProperty name={t("sub.lifecycle_publishedby")}>
                {<UserLabel noReadonly user={edited.lifecycle.publishedBy!} />}
            </SidebarProperty>
        </>}

    </LifecycleSummary>
}