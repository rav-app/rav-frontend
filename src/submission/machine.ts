
import { useStable } from 'apprise-frontend-core/utils/function'
import { useMachine } from 'apprise-frontend-core/utils/machine'
import { useContext, useEffect } from 'react'
import { DetailContext } from './context'
import { useDetailEffects } from './effects'
import { useSubmissionOracle } from './oracle'

// here we define the state machine that orchestrates the submission workflow, and keep its current state.
// the intended client is the MachineProvider that puts this in a context so as to share it. 

export type DetailMachineState = 'mounted' | 'loading' | 'parsing' | 'validating' | 'matching' | 'edit' | 'resolving'
export type DetailMachineEvent = 'load' | 'parse' | 'validate' | 'match' | 'change' | 'resolve'

export type DetailMachine = ReturnType<typeof useDetailMachine>     // no need to maintain a typedeef.

export const useDetailMachine = () => {

    const { isNew, dirty, assets, edited } = useContext(DetailContext)

    const effects = useDetailEffects()

    const oracle = useSubmissionOracle()

    // captures unmount with a dirty state and makes everything is cleaned up.

    useEffect(()=>{

        if (dirty)
            effects.fetchTrxPlausibilityIfRequired()

    //eslint-disable-next-line
    },[edited.tenant,edited.type])

    // eslint-disable-next-line
    useEffect(useStable(() => {

        return () => {

            if (dirty)
                effects.removeAssets(assets)

        }

    }), [])

    const machine = useMachine<DetailMachineState, DetailMachineEvent>((allow, { trigger, transitionTo }) =>

        allow.from('mounted').to('edit').and(edited.live ? () => effects.fetchCurrentRecords().then(trigger('match')) : undefined)

            .from('mounted').to('loading').on('load').and(() => effects.resetAndLoadIfRequired().then(transitionTo('edit')))

            // after loading an existing submissions, we need to fetch arelevant current records.
            .from('loading').to('edit').and(() => effects.fetchCurrentRecords().then(() => oracle.canPublish(edited) && trigger('validate')()))

            // changes spans parses, ignores/includes, and matchmaking. 
            // after pasing and matchmaking, we may need to reload current records.
            // in all cases, we need to (re-)validate.
            .from('edit').to('edit').on('change').and(() => effects.fetchCurrentRecords().then(trigger('validate')))

            .from('edit').to('parsing').on('parse').and(() => effects.parseResources().then(transitionTo('edit')))

            // match is after parsing, then the implications are like change.
            .from('parsing').to('edit').and(() => effects.matchRecords().then(trigger('resolve')))

            .from('edit').to('validating').on('validate').and(() => effects.validateRecords().then(transitionTo('edit')))

            .from('validating').to('edit').and(effects.updateStatistics)

            .from('edit').to('matching').on('match').and(() => effects.matchRecords().then(transitionTo('edit')))

            .from('matching').to('edit').and(trigger('validate'))

            .from('edit').to(`resolving`).on('resolve').and(() => effects.resolveAssets().then(transitionTo('edit')))

            .from('resolving').to(`edit`).and(trigger('change'))

            .startFrom('mounted').and(isNew ? transitionTo('edit') : trigger('load'))

        , { debug: false })

    return machine


}