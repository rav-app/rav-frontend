import { GenericRecord } from '#record/model';
import { useRecordPlugin } from '#record/plugin';
import { utils } from 'apprise-frontend-core/utils/common';
import { Asset, SubmissionType, appriseReferenceOf, referenceOf } from './model';

export const useAssetResolver = <T extends GenericRecord>(type: SubmissionType) => {

    const plugin = useRecordPlugin<T>(type)

    const {resolve,restore, matches } = plugin.resolveAssets()

    return {

        resolve: (records: T[], assets: Asset[]) => {

            const assetmap = {...utils().index(assets).by(referenceOf), ...utils().index(assets).by(appriseReferenceOf)}

            return resolve(records, assetmap)
    
        }

        ,

        matches

        ,

        restore: (records: T[], assets: Asset[]) => {

            console.log(`restoring asset references for ${type} submission...`)
    
            const assetmap = utils().index(assets).by(referenceOf)
    
            return restore(records, assetmap)
    
        }

    }

}