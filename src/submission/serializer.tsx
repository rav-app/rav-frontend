import { GenericRecord } from '#record/model'
import { useRecordPlugin } from '#record/plugin'
import { JsonSerializationMode } from '#record/jsonserializer'
import { Submission } from './model'
import { PatchAndCurrent } from './records'




export const useSubmissionJsonSerializer = <T extends GenericRecord> (submission: Submission<T>) => {

    const plugin = useRecordPlugin<T>(submission.type)

    const jsonserializer = plugin.jsonserialize()

    return (submission: Submission<T>, records: PatchAndCurrent<T>[], mode: JsonSerializationMode) => {

        return records.map(({ current, ...patch }) => {
        
            const issues = submission.validationReport?.records[patch.id] ?? []
        
            return jsonserializer({ patch: patch as T, current, issues, mode })
        
        })

    
    }


}