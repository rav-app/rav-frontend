import { DetailsIcon } from '#details/constants'
import { MatchedRecordIcon } from '#record/constants'
import { RavRecord } from '#record/model'
import { RecordMatch } from '#submission/model'
import { useVesselRouting } from '#vessel/routing'
import { VidLabel } from '#vid/label'
import { Empty } from 'antd'
import { useT } from 'apprise-frontend-core/intl/language'
import { TenantLabel } from 'apprise-frontend-iam/tenant/label'
import { Button } from 'apprise-ui/button/button'
import { ChoiceBox } from 'apprise-ui/choicebox/choicebox'
import { classname } from 'apprise-ui/component/model'
import { Form } from 'apprise-ui/form/form'
import { Link } from 'apprise-ui/link/link'
import { ReadonlyContext } from 'apprise-ui/readonly/readonly'
import { Tip } from 'apprise-ui/tooltip/tip'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { OpenIcon } from 'apprise-ui/utils/icons'
import React, { useContext, useMemo } from 'react'
import { DetailContext } from './context'
import './matchform.scss'
import { PatchAndCurrent } from './records'


export type MatchFormProps = {

    record: PatchAndCurrent
    matchSelected: RavRecord
    onMatchSelection: (_: RavRecord) => any
}

export const MatchingForm = (props: MatchFormProps) => {

    const { edited } = useContext(DetailContext)

    const { record } = props

    const matchRecord = edited.matchReport?.[record.id]

    return matchRecord ? <InnerForm {...props} /> : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />

}

const InnerForm = (props: MatchFormProps) => {

    const t = useT()

    const { record, onMatchSelection, matchSelected } = props

    const { edited, set, machine } = useContext(DetailContext)

    const matchRecord = edited.matchReport?.[record.id]!

    const records = edited.records

    // other uvis in submission (excluding this very recordbled, ot it may be disabled if it has been previously matched.)
    const submissionOtherUvis = useMemo(() => records.filter(r => r.id !== record.id && !!r.uvi).map(r => r.uvi), [record.id, records])

    const isForeign = (d: RavRecord) => d.tenant !== edited.tenant
    const isDuplicate = (d: RavRecord) => submissionOtherUvis.includes(d.uvi)

    const status = matchRecord.unmatched ? 'unmatched' : matchRecord.matched ? 'matched' : 'pending'


    const commit = (match: RecordMatch) => {

        set.using(s => {

            s.matchReport![record.id] = match

            const original = s.records.find(r => r.id === record.id)!

            if (match.matched)
                original.uvi = match.matched

            else original.uvi = undefined!


        })

        machine.trigger('change')
    }

    const readonly = React.useContext(ReadonlyContext)

    const vessels = useVesselRouting()


    return <Form>

        <ExplainerPanel className={`matching-panel summary-${status}`}>

            <Explainer icon={MatchedRecordIcon} action={<div>{t(`rec.match_${status}`)}</div>}>
                {t('rec.matching_explainer')}
            </Explainer>


            <div className='matches-title'>
                {t('rec.matches_section')}
            </div>

            {matchRecord ?


                <div>{

                    matchRecord.matches.map((match, i) => {

                        const foreign = isForeign(match)
                        const duplicate = isDuplicate(match) //&& Math.random() > .8 // MVP only
                        const disabled = foreign || duplicate

                        const selectedMatch = matchSelected?.id === match.id

                        return <div key={i} className={classname('vessel-match', selectedMatch && 'match-selected', matchRecord.matched === match.uvi && 'match-matched', disabled && 'match-disabled')} >

                            <Tip className='match-tip' tipType='error' tip={foreign ? t('rec.match_foreign_tip') : duplicate ? t('rec.match_duplicate_tip') : undefined}>
                                <div className='match-main'>
                                    <div className='match-icon'>
                                        <DetailsIcon />
                                    </div>

                                    <div className='match-descriptor'>

                                        <div className="match-name">
                                            <Link linkTo={vessels.detailRoute(match.uvi)}>
                                                {match.details.name}
                                            </Link>
                                        </div>

                                        <div className="match-data-first">
                                            <VidLabel vid={match.uvi} />
                                        </div>

                                        <div className="match-data-second">
                                            <TenantLabel tenant={match.tenant} />
                                        </div>


                                        <div className="match-data-compare">
                                            <Button disabled={selectedMatch} type='link' icon={<OpenIcon />} iconPlacement='left' onClick={() => onMatchSelection(match)}>
                                                {t('rec.match_compare_btn')}
                                            </Button>
                                        </div>

                                    </div>

                                </div>
                            </Tip>

                            <div className="match-controls">

                                <ChoiceBox disabled={readonly || disabled} className='match-box' value={matchRecord.matched} onChange={v => commit({ ...matchRecord, matched: v, unmatched: undefined })}>
                                    <ChoiceBox.Option noTitle value={match.uvi} />
                                </ChoiceBox>

                            </div>

                        </div>
                    })


                }

                    <div className={classname('vessel-match', 'reject-all')} >

                        <div className='match-main'>
                            <div className='match-icon'>
                                <DetailsIcon />
                            </div>
                            <div className='match-descriptor'>

                                <div className="match-name">
                                    {t('rec.match_reject')}
                                </div>

                                <div className="match-data-first">
                                    {t('rec.match_reject_explainer')}
                                </div>

                            </div>

                        </div>

                        <ChoiceBox disabled={readonly} className='match-box' value={matchRecord.unmatched} onChange={v => commit({ ...matchRecord, matched: undefined, unmatched: v })}>
                            <ChoiceBox.Option noTitle value={true} />
                        </ChoiceBox>



                    </div>

                </div>


                :

                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />


            }

        </ExplainerPanel>

    </Form>

}