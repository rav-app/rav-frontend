import { useRecordOracle } from '#record/oracle'
import { Empty } from 'antd'
import { useT } from 'apprise-frontend-core/intl/language'
import { useLogged } from 'apprise-frontend-iam/login/logged'
import { Button } from 'apprise-ui/button/button'
import { Form } from 'apprise-ui/form/form'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { DotIcon } from 'apprise-ui/utils/icons'
import { useContext } from 'react'
import { RecordIgnoredIcon, RecordInvalidIcon, RecordValidIcon, RecordValidWithWaningIcon } from '../record/constants'
import { DetailContext } from './context'
import { PatchLabel } from './patchlabel'
import { PatchAndCurrent } from './records'
import './validationform.scss'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { GenericRecord } from '#record/model'



export type ValidationFormProps =  {

    record: PatchAndCurrent
    close: () => any
}

export const ValidationForm = (props: ValidationFormProps) => {

    const t = useT()
    const logged = useLogged()
    const hasNoTenant = useTenancyOracle().hasNoTenant()

    const { edited, set, machine } = useContext(DetailContext)
    
    const { record, close} = props

    const oracle = useRecordOracle().given(edited)

    const issues = edited.validationReport?.records?.[record.id]
    const status = oracle.validationStatusOf(record)

    const Icon = oracle.hasValidationIssues(record) ? status === 'invalid' ? RecordInvalidIcon : RecordValidWithWaningIcon : RecordValidIcon
    const validationColor = oracle.hasValidationIssues(record) ? status === 'invalid' ? 'orangered' : 'orange' : 'lightseagreen'

    const submitChanges = () => {
        machine.trigger('change')

        close()
    }

    const ignoreRecord = () => {

        set.using(s => {

            const match = s.records.find(rr => rr.id === record.id)!
            match.lifecycle.state = 'ignored'
            match.lifecycle.lastModified = Date.now()
            match.lifecycle.lastModifiedBy = logged.username

        })

        submitChanges()
       
    }

    const toggleReject = (r: GenericRecord) => r.lifecycle.state = r.lifecycle.state === 'rejected' ? 'submitted' : 'rejected'

    const rejectRecord = () => {
        set.using(s => toggleReject(s.records.find(rr => rr.id === record.id)!))

        submitChanges()
    }

    const toggleInclude = (r: GenericRecord) => r.lifecycle.state = r.lifecycle.state === 'submitted' ? 'rejected' : 'submitted'

    const includeRecord = () => {
        set.using(s => toggleInclude(s.records.find(rr => rr.id === record.id)!))

        submitChanges()
    }

    const actionBtn = hasNoTenant ? 
            record.lifecycle.state === 'submitted' ? 
                <Button type="danger" iconPlacement='left' className='validation-action-btn' onClick={rejectRecord} icon={<RecordIgnoredIcon />}>{t('rec.validation_reject_btn')}</Button>
            :
                record.lifecycle.state === 'rejected' ? 
                    <Button type="primary" iconPlacement='left' className='validation-action-btn' onClick={includeRecord} icon={<RecordIgnoredIcon />}>{t('rec.validation_include_btn')}</Button>
                :
                    <Button type="primary" iconPlacement='left' className='validation-action-btn' enabled={record.lifecycle.state === 'uploaded'} onClick={ignoreRecord} icon={<RecordIgnoredIcon />}>{t('rec.validation_ignore_btn')}</Button>
        :
        <Button type="primary" iconPlacement='left' className='validation-action-btn' enabled={record.lifecycle.state === 'uploaded'} onClick={ignoreRecord} icon={<RecordIgnoredIcon />}>{t('rec.validation_ignore_btn')}</Button>



    return <Form>

        <ExplainerPanel className={`validation-panel status-${status}`}>

            <Explainer icon={Icon} action={<PatchLabel titleStyle={{border:`1px solid ${validationColor}`}} fill={validationColor} noIcon displayMode='validation' patch={record} />} >
                {t(`rec.explain_${status}`)}
            </Explainer>

            <div className='validation-issues-title'>
                {t('rec.error_section')}
            </div>

            {(issues?.length ?? 0) === 0 ?

                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={t('rec.no_issues')} />

                :

                <div className='explainer-body'>{
                    issues?.map((issue, i) =>
                        <div key={i} className={`validation-issue issue-${issue.type}`} style={{ display: 'flex' }} >
                            <div className='validation-issue-icon'>
                                <DotIcon color={issue.type === 'error' ? 'red' : 'orange'} />
                            </div>
                            <div className='validation-issue-text'  >{issue.message}</div>
                        </div>)
                }
                </div>
            }

            {actionBtn}
           

        </ExplainerPanel>

    </Form>
}