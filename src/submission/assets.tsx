import { useRecordPlugin } from '#record/plugin'
import { useLanguage, useT } from 'apprise-frontend-core/intl/language'
import { Bytestream } from 'apprise-frontend-streams/model'
import { Button } from 'apprise-ui/button/button'
import { mimeIcons } from 'apprise-ui/filebox/constants'
import { FileDroppable } from 'apprise-ui/filebox/filedroppable'
import { Label, UnknownLabel } from 'apprise-ui/label/label'
import { useSelection } from 'apprise-ui/table/selection'
import { Table } from 'apprise-ui/table/table'
import { useTableUtils } from 'apprise-ui/table/utils'
import { FileSizeLabel } from 'apprise-ui/utils/filesizelabel'
import { RemoveItemIcon, UploadIcon } from 'apprise-ui/utils/icons'
import React, { useContext } from 'react'
import "./assets.scss"
import { AssetArchiveIcon, assetExtensions, AssetIcon, AssetMatchHeaderIcon, AssetReferenceIcon, AssetSizeIcon } from './constants'
import { DetailContext } from './context'
import { useDetailEffects } from './effects'
import { Asset, referenceOf } from './model'
import { PatchLabel } from './patchlabel'
import { useSubmissionRouting } from './routing'


type SubmissionAssetsProps = {

    onRemove: (assets: Asset[]) => any
}


export const SubmissionAssets = (props: SubmissionAssetsProps) => {

    const t = useT()

    const lang = useLanguage().current()

    const { edited, assets, machine } = useContext(DetailContext)

    const effects = useDetailEffects()
   
    const { onRemove } = props

    const resolver = useRecordPlugin(edited.type).resolveAssets()

    const routing = useSubmissionRouting()

    const dialogRef = React.createRef<HTMLInputElement>()

    const openDialog = () => dialogRef.current?.click()

    const uploadFiles = async (files: File[]) => {

        await effects.uploadAssets(files.filter(f => f.size && f.type))

        machine.trigger('resolve')

    }

    const removeFiles = async (streams: Bytestream[]) => {

       await effects.removeAssets(streams)

        selection.clear()

        onRemove(streams)

        machine.trigger('resolve')

    }

    const { Column, compareStringsOf, compareNumbersOf } = useTableUtils<Asset>()

    const records = edited.records

    const matched = React.useMemo(() =>


        resolver.matches(records)

        //eslint-disable-next-line
        , [records])


    const selection = useSelection<Asset>()

    const tableKey = `asset_${lang}`


    return <FileDroppable className='assets-droppable filebox-drappable' onDrop={uploadFiles} >

        <Table  filterProps={{ filterWhat: s=> `${JSON.stringify(s)} ${matched[s.id].uvi }` }} name='assets' key={tableKey} touch={matched} selection={selection} data={assets} initialSort={[{ key: 1, mode: 'asc' }, { key: 'rec', mode: 'asc' }]} emptyPlaceholder={t('sub.asset_empty_placeholder')}>

            <Table.Control>
                <Button enabled={selection.isNotEmpty()} iconPlacement='left' icon={<RemoveItemIcon />} onClick={() => removeFiles(selection.selected)} type='danger'>{t("sub.asset_remove_many", { count: selection.selected.length })}</Button>
            </Table.Control>

            <Table.Control>
                <Button iconPlacement='left' icon={<UploadIcon />} onClick={openDialog} type='primary'>{t("sub.asset_upload")}</Button>
                <input key={assets.length} ref={dialogRef} accept={assetExtensions.join(",")} multiple type="file" hidden onChange={e => {
                    try {
                        uploadFiles(Array.from(e.target.files ?? []))
                    }
                    finally {
                        e.target.value=''
                    }}} />
            </Table.Control>

            <Table.Buttons>{(stream: Bytestream) =>

                <Button icon={<RemoveItemIcon />} onClick={() => removeFiles([stream])}>{t("sub.asset_remove")}</Button>

            }</Table.Buttons>

            <Column name="name" sort={compareStringsOf(bs => bs.name)} title={<Label icon={<AssetIcon />} title={t('sub.asset_name_col')} />} render={bs => bs.name ? <Label icon={mimeIcons[bs.type]} title={bs.name} /> : <UnknownLabel />} />

            <Column name="rec" width={190} sort={compareStringsOf(bs => matched[bs.id]?.uvi)} title={<Label icon={<AssetMatchHeaderIcon />} title={t('sub.asset_match_col')} />}  renderIf={()=>true} render={bs => matched[bs.id] && <PatchLabel linkTo={() => routing.innerRecordRoute(matched[bs.id].id)} patch={matched[bs.id]} />} />

            <Column name="archive" sort={compareStringsOf(bs => bs.properties?.archive)} title={<Label icon={<AssetArchiveIcon />} title={t("sub.asset_archive_col")} />} render={bs => bs.properties.archive ? <Label icon={mimeIcons['application/zip']} title={bs.properties.archive} /> : undefined!} />

            <Column name="ref" sort={compareStringsOf(referenceOf)} title={<Label icon={<AssetReferenceIcon />} title={t("sub.asset_reference_col")} />} render={referenceOf} />

            <Column name="size" sort={compareNumbersOf(bs => bs.size)} title={<Label icon={<AssetSizeIcon />} title={t("sub.asset_size_col")} />} render={bs => <FileSizeLabel size={bs.size} />} />

        </Table>

    </FileDroppable>
}