


import { RavRecord, RecordSlot } from '#record/model'
import { utils } from 'apprise-frontend-core/utils/common'
import { useForm } from 'apprise-frontend-core/utils/form'
import { Bytestream } from 'apprise-frontend-streams/model'
import { useRoutableDrawer } from 'apprise-ui/drawer/drawer'
import { unavailableData, useTableData } from 'apprise-ui/table/provider'
import { useResetFilters } from 'apprise-ui/utils/filter'
import { useMemo, useState } from 'react'
import { recordIssueParam, resourceIssueParam } from './constants'
import { Submission } from './model'
import { useSubmissionOracle } from './oracle'
import { PatchAndCurrent, filterContext, recordTable } from './records'
import { downloadDialogParam, rejectParam } from './routing'




// here we hold form state, auxiliary state (state outside the edited model), and derived data.
// the intended client is the DetailProvider that puts this in a context so as to share it.

export type DetailData = ReturnType<typeof useDetailData>  // no need to mantain a type def.

export const useDetailData = (submission: Submission) => {

    const oracle = useSubmissionOracle()

    const form = useForm(submission)

    const { edited } = form

    // additional detail state: tracks matching vessels, unchanged records, and media assets.
    // these are either read or set by multiple descendants.

    const initialAuxState = () => ({

        currentRecords: {} as Record<string, RavRecord>,
        assets: [] as Bytestream[],
        trxPlausibility: true
    })

    const [auxState, setAuxState] = useState(initialAuxState)



    // resets filters as the relevant data sources change (eg. new resources uploaded, or we move to a new submission.)
    // why? filter options are derived from population: new options may appear, previous options may disappear.
    // if we don't reset selected options, some rows may start hidden and the visuals are always surprising.
    // why do it here? it's all effect-based, this hides the first flash of adjustement if we're not rendering the filters (eg another tab).

    const filterDependencies = useMemo(() => {

        return [edited.records, auxState.currentRecords]

    }, [edited.records, auxState.currentRecords])

    useResetFilters(filterContext, filterDependencies)

    // ---------------------------------------------------------------------------------------------------------------------

    // state mutators to share.
    const setCurrentRecords = (currentRecords: Record<string, RavRecord>) => setAuxState({ ...auxState, currentRecords })
    const setTrxPlausibility = (trxPlausibility: boolean) => setAuxState({ ...auxState, trxPlausibility })
    const setAssets = (assets: Bytestream[]) => setAuxState({ ...auxState, assets })
    const resetAuxState = () => setAuxState(initialAuxState())

    // anytime we fetch new records, we identify the patches that carry no actual changes.
    // we then use this info for visuals, filtering, and validation.
    const sameRecords = useMemo(() => {

        return (edited.records ?? []).filter(r => !r.patchedSlots.some(slot => {

            const patchedSlot = r[slot]
            const currentSlotMinusTimestamp = { ...auxState.currentRecords[r.uvi]?.[slot], timestamp: patchedSlot?.timestamp } as RecordSlot

            return !utils().deepequals(currentSlotMinusTimestamp, patchedSlot)

        })).map(r => r.id)


        // eslint-disable-next-line
    }, [auxState.currentRecords])


    const resourceIssueKit = useRoutableDrawer(resourceIssueParam)
    const recordIssueKit = useRoutableDrawer(recordIssueParam)
    const downloadKit = useRoutableDrawer(downloadDialogParam)
    const rejectKit = useRoutableDrawer(rejectParam)

    // selected data first, then filtered if none selected, then all if none filtered or if table not mounted yet.
    const patchRows = useTableData<PatchAndCurrent>(recordTable)
    const patchRowsInFocus = (
        
        patchRows === unavailableData ?

        // we merge in current records for partial patch types that carry no vessel names.
        // this covers the case where the table hasn't been visited yet and there are no rows reported. 
        (edited.records ?? []).map(r => ({ ...r, current: auxState.currentRecords?.[r.uvi] }))

        :

        patchRows.selected.length ? patchRows.selected : patchRows.data
    
    
        ).filter( t => t.lifecycle.state !== 'ignored')


    const readonly = !oracle.canEdit(edited)

    return {
        submission,
        ...form,
        ...auxState,
        readonly,
        patchRows,
        patchRowsInFocus,
        resourceIssueKit, recordIssueKit, downloadKit, rejectKit,
        sameRecords,
        isNew: !edited.lifecycle.created,
        resetAuxState,
        setCurrentRecords,
        setTrxPlausibility,
        setAssets
    }

}
