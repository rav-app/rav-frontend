import { MockHorizonContext } from 'apprise-frontend-core/client/mockhorizon'
import { useMockery } from 'apprise-frontend-core/client/mocks'
import { StateProvider } from 'apprise-frontend-core/state/provider'
import { useBusyState } from 'apprise-frontend-core/utils/busyguard'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { PropsWithChildren, useContext } from 'react'
import {  SubmissionRoutingContext, SubmissionStoreContext } from './context'
import { useSubmissionMockery } from './mockery'
import { SubmissionRoutingState } from './routing'
import { SubmissionStore } from './store'




const initialStore: SubmissionStore = { all: undefined!}

const initialRoutingState: Partial<SubmissionRoutingState> = {}

type SubmissionProps = PropsWithChildren<Partial<{

    mock: boolean

}>>

export const Submissions = (props: SubmissionProps) => {

    const { children } = props

    return <StateProvider initialState={initialStore} context={SubmissionStoreContext}>
        <StateProvider initialState={initialRoutingState} context={SubmissionRoutingContext}>
            <Initialiser {...props}>
                {children}
            </Initialiser>
        </StateProvider>
    </StateProvider>

}


const Initialiser = (props: SubmissionProps) => {

    const beforeHorizon = useContext(MockHorizonContext)

    const { children, mock = beforeHorizon } = props

    const busy = useBusyState()

    const mockery = useMockery()
    const submockery = useSubmissionMockery()

    const activate = () => {

        if (mock) {

            busy.toggle("stage-submission", "staging submissions...")
            mockery.initMockery(submockery)
            busy.toggle("stage-submission")

        }
    }

    const { content } = useRenderGuard({

        render: children,
        orRun: activate
    })

    return content

}