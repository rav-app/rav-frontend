
import { NoSuchRoute } from 'apprise-ui/link/nosuchroute'
import { Route, Switch } from 'react-router'
import { SubmissionDetail } from './detail'
import { SubmissionList } from './list'
import { SubmissionLoader } from './loader'
import { useSubmissionRouting } from './routing'




export const SubmissionRouter = () => {

    const routing = useSubmissionRouting()

    return <Switch>

        <Route exact path={routing.listRoute()}>
            <SubmissionList />
        </Route>

        <Route exact path={routing.detailRoute(':id')}>
            <SubmissionLoader>
                <SubmissionDetail />
            </SubmissionLoader>
        </Route>

        <NoSuchRoute />

    </Switch>
}