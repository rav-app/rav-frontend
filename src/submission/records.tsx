
import { newAuthorizationSlot } from '#authorization/model'
import { baseComboType } from '#basecombo/mode'
import { detailsType } from '#details/constants'
import { newDetailSlot } from '#details/model'
import { newPhotoSlot } from '#photographs/model'
import { RecordIgnoredIcon } from '#record/constants'
import { useClaimFilter, useIdFilter, useStateFilter, useValidationFilter } from '#record/filter'
import { GenericRecord, RavRecord, RecordPatch, SlotType } from '#record/model'
import { useRecordOracle } from '#record/oracle'
import { useLanguage, useT } from 'apprise-frontend-core/intl/language'
import { useLogged } from 'apprise-frontend-iam/login/logged'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { useRoutableDrawer } from 'apprise-ui/drawer/drawer'
import { useSelection } from 'apprise-ui/table/selection'
import { Table } from 'apprise-ui/table/table'
import partition from 'lodash/partition'
import React, { useContext, useMemo } from 'react'
import { useLocation } from 'react-router'
import { useRecordPlugin } from '../record/plugin'
import { DetailContext } from './context'
import { Submission } from './model'
import { useSubmissionOracle } from './oracle'
import { usePatchColumns } from './patchcolumns'
import { PatchDetail } from './patchdetail'
import './records.scss'
import { recordDetailParam } from './routing'
import { RecordValidationContext } from './validator'
import { newTrxAuthzSlot } from '#trxauth/model'
import { trxauthzType } from '#trxauth/constants'

export const filterContext = `submissionrecords`
export const recordTable = 'submission-record'

// the patch augmented with the current record (if any)
export type PatchAndCurrent<T extends RecordPatch = GenericRecord> = T & { current?: RavRecord }


export const SubmissionRecords = () => {

    const t = useT()
    const lang = useLanguage().current()
    const location = useLocation()

    const { edited, set, currentRecords, machine } = useContext(DetailContext)

    const { records, matchReport } = edited

    const logged = useLogged()
    const oracle = useSubmissionOracle()

    const recordOracle = useRecordOracle().given(edited)

    const canEdit = oracle.canEdit(edited)

    const { patchFilters } = useRecordPlugin(edited.type)

    const { given } = useRecordOracle()

    const { routeAt, match } = useRoutableDrawer(recordDetailParam)

    const transition = (rec, state) => {

        rec.lifecycle.state = state
        rec.lifecycle.lastModified = Date.now()
        rec.lifecycle.lastModifiedBy = logged.username
    }

    // actions

    const toggleIgnore = (r: GenericRecord) => transition(r, r.lifecycle.state === 'ignored' ? r.lifecycle.previousState ?? 'uploaded' : 'ignored')

    const ignore = (r: GenericRecord) => () => {

        set.using(s => toggleIgnore(s.records.find(rr => rr.id === r.id)!))

        machine.trigger('change')
    }

    const toggleReject = (r: GenericRecord) => r.lifecycle.state = r.lifecycle.state === 'rejected' ? 'submitted' : 'rejected'

    const reject = (r: GenericRecord) => () => set.using(s => toggleReject(s.records.find(rr => rr.id === r.id)!))

    const toggleSelected = (selected: GenericRecord[]) => () => {

        const selectedIds = selected.map(s => s.id)

        set.using(s => {

            s.records.forEach(r => {

                if (selectedIds.includes(r.id))

                    if (oracle.canPublish(edited))
                        transition(r, r.lifecycle.state === 'rejected' ? 'submitted' : 'rejected')
                    else
                        transition(r, r.lifecycle.state === 'ignored' ? 'uploaded' : 'ignored')


            })

            selection.select([])

        })


        machine.trigger('change')

    }

    // buttons


    const selection = useRecordSelection(edited)

  
    const selected = selection.selected
    
    const [ignored, notignored] = partition(selected, r => r.lifecycle.state === 'ignored')
    const [rejected, notrejected] = partition(notignored, r => r.lifecycle.state === 'rejected')

    const toggleSelectedDraft = <Button noReadonly={canEdit} disabled={!selected.length} type='danger' iconPlacement='left' icon={<RecordIgnoredIcon color='inherit' />} onClick={toggleSelected(ignored.length ? ignored : notignored)}>
        {t(ignored.length ? 'rec.toggle_many_include' : `rec.toggle_many_ignore`  , { count: selected.length })}
    </Button>

    const toggleSelectedSubmitted = <Button noReadonly={canEdit} disabled={!!ignored.length} type='danger' iconPlacement='left' icon={<RecordIgnoredIcon color='inherit' />} onClick={toggleSelected(rejected.length ? rejected : notrejected)}>
        {t(rejected.length ? 'rec.toggle_many_include' : `rec.toggle_many_reject`, { count: rejected.length || notrejected.length })}
    </Button>

    const toggleSelectedBtn =  edited.lifecycle.state === 'draft'  || edited.lifecycle.state === 'pending' ? toggleSelectedDraft : edited.lifecycle.state === 'submitted' ? toggleSelectedSubmitted : null
    
    const rowClasses = (r: GenericRecord) => classname(r.lifecycle.state === 'ignored' && 'row-ignored', r.lifecycle.state === 'rejected' && 'row-rejected')


    //  filters

    // stabilises empty default until we have records.
    const unfilteredRecords = React.useMemo(() => records ?? [], [records])

    const { data: pluginFilteredRecords, filters: pluginFilters } = patchFilters({ submission: edited, data: unfilteredRecords, context: filterContext })

    const { StateFilter, filteredData: stateFilteredRecords } = useStateFilter({

        data: pluginFilteredRecords,
        options: oracle.recordStatesFor(edited),
        initial: unfilteredRecords,
        context: filterContext

    })

    const { ValidationFilter, filteredData: validationFilteredRecords } = useValidationFilter(
        { submission: edited, data: stateFilteredRecords, initial: unfilteredRecords, context: filterContext }
    )

    const { IdFilter, filteredData: idFilteredRecords } = useIdFilter(
        { data: validationFilteredRecords, initial: unfilteredRecords, submission: edited, context: filterContext }
    )

    const { FlagFilter, filteredData } = useClaimFilter(
        { data: idFilteredRecords, initial: unfilteredRecords, submission: edited, context: filterContext, currentRecords }
    )

    const augmentedRecords: PatchAndCurrent[] = useMemo(() =>

        filteredData.map(row => ({ ...row, current: currentRecords?.[row.uvi] }))


        // eslint-disable-next-line
        , [filteredData, currentRecords, matchReport])      // touches data when filter, vessels, or match report change

    const recordMatch = augmentedRecords.find(r => r.id === match)


    let buttons = (r: GenericRecord) => {

        let btns = [] as JSX.Element[]

        // before submitting, some valid records may be rejected.
        if (!oracle.alreadySubmitted(edited))
            btns.push(

                <Button noReadonly key='ignore' enabled={selection.selected.length === 0} icon={<RecordIgnoredIcon />} onClick={ignore(r)}>
                    {/* we need to name the option even in published state */}
                    {t(`rec.toggle_${r.lifecycle.state === 'ignored' ? 'include' : 'ignore'}`)}

                </Button>
            )

        else btns.push(

            <Button key='reject' enabled={selection.selected.length === 0 && recordOracle.canReject(r)} disabled={given(edited).isInvalid(r)} icon={<RecordIgnoredIcon />} onClick={reject(r)}>
                {t(`rec.toggle_${r.lifecycle.state === 'rejected' ? 'include' : 'reject'}`)}
            </Button>
        )

        return btns
    }

    const columns = usePatchColumns(edited, routeAt)

    const tableKey = `${recordTable}_${lang}`


    // a dummy record to
    const dummy = { id:'dummy', lifecycle: {}, details: newDetailSlot(), authorization: newAuthorizationSlot(), [trxauthzType]: newTrxAuthzSlot(), photograph: newPhotoSlot(), patchedSlots:[] as SlotType[]  } as GenericRecord
    const recordOrDummy = recordMatch ?? { ...dummy, current:undefined} as PatchAndCurrent<GenericRecord>


    return <>

         <RecordValidationContext.Provider value={edited.validationReport?.records[recordOrDummy.id]}>

                <PatchDetail record={recordOrDummy} />

        </RecordValidationContext.Provider>

        <Table name={recordTable} key={tableKey} touch={location.search} context={filterContext} data={augmentedRecords} total={unfilteredRecords.length} mountDelay
            emptyPlaceholder={t('sub.rec_empty_placholder')}
            selection={selection}
            rowClassName={rowClasses}>

            <Table.Filter >
                {ValidationFilter}
            </Table.Filter>

            <Table.Filter>
                {StateFilter}
            </Table.Filter>

            {(edited.type === baseComboType || edited.type === detailsType) && <Table.Filter>
                {IdFilter}
            </Table.Filter>}

            <Table.Filter>
                {FlagFilter}
            </Table.Filter>

            {pluginFilters.map((pluginFilter, i) =>

                <Table.Filter key={i} name="">
                    {pluginFilter}
                </Table.Filter>
            )}

            {edited.lifecycle.state === 'published' ||

                <Table.Control>
                    <div style={{ marginLeft: 15 }}>{toggleSelectedBtn}</div>
                </Table.Control>
            }

            {columns}

            <Table.Buttons>{buttons}</Table.Buttons>

        </Table>


    </>
}


export const useRecordSelection = (submission: Submission) => {


    return useSelection<PatchAndCurrent>({

        // select all appears only when a selection already exists: preserves consistency of selection.
        selectAll: selected => selected.length > 0,

        // same-state selection, never published.
        isSelectable: (r, selected) => selected.length === 0 || r.lifecycle.state === selected[0].lifecycle.state

    })

}