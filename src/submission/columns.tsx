import { MatchedRecordIcon, NewRecordIcon, RecordStateIcon } from "#record/constants"
import { useT } from "apprise-frontend-core/intl/language"
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { TenantColumn } from 'apprise-frontend-iam/tenant/tenantcolumn'
import { Label } from "apprise-ui/label/label"
import { useTableUtils } from "apprise-ui/table/utils"
import format from 'date-fns/format'
import { SubmissionDateIcon, SubmissionStateIcon, SubmissionStatsIcon, SubmissionTypeIcon } from "./constants"
import { SubmissionLabel } from "./label"
import { Submission, useSubmissionModel } from "./model"
import { useSubmissionOracle } from "./oracle"
import { submissiondDetailParam, useSubmissionRouting } from "./routing"
import { SubmissionStats } from "./stats"


export const useSubmissionColumns = () => {

  const t = useT()

  const logged = useTenancyOracle()
  const oracle = useSubmissionOracle()

  const model = useSubmissionModel()
  const routing = useSubmissionRouting()

  const { Column: SubmissionColumn, compareDatesOf, compareStringsOf, columnsOf } = useTableUtils<Submission>()

  return columnsOf(

    <SubmissionColumn name="created" defaultLayout width={140}
      filter="year"
      text={(s) => format(s.lifecycle.created!, 'PPP')}
      sort={compareDatesOf(s => s.lifecycle.created)}
      title={<Label icon={<SubmissionDateIcon />} title={t('sub.start_col')} />}
      render={s => {

        const tabAwareRoute = `${routing.detailRoute(s.id)}?${submissiondDetailParam}=records`

        const Icon = model.validationAwareIconOf(s)

        return <SubmissionLabel icon={<Icon />} noDecorations readonly={!oracle.canEdit(s)} displayMode='start' dateMode='short' linkTo={tabAwareRoute} submission={s} />

      }} />,



    logged.managesMultipleTenants() &&

    <TenantColumn defaultLayout tenantOf={s => s.tenant} width={170} filter='party' />


    ,

    <SubmissionColumn name="published" width={130} title={<Label icon={<SubmissionDateIcon />} title={t('sub.end_col')} />}
      filter="year" text={(s) => !!s.lifecycle.published && format(s.lifecycle.published, 'PPP')} 
      sort={compareDatesOf(s => s.lifecycle.published)}
      render={s => <SubmissionLabel noIcon displayMode='end' dateMode='short' submission={s} />} />

    ,

    <SubmissionColumn defaultLayout name="state" width={130} filter="state" text={(s) => t(`rec.state_${s?.lifecycle.state}`)} sort={compareStringsOf(s => s.lifecycle.state ?? '')} title={<Label icon={<SubmissionStateIcon />} title={t('sub.state_col')} />} render={s => <SubmissionLabel mode='tag' displayMode='state' submission={s} />} />

    ,

    <SubmissionColumn defaultLayout name="type" width={180} filter="type" sort={compareStringsOf(s => s.type)} title={<Label icon={<SubmissionTypeIcon />} title={t('sub.type_col')} />} render={s => <SubmissionLabel displayMode='type' submission={s} />} />


    ,

    <SubmissionColumn defaultLayout name="stats" width={200} title={<Label icon={<RecordStateIcon />} title={t('sub.stats_col')} />} render={s => <SubmissionStats mode='core' sub={s} />} />

    // ,

    // <SubmissionColumn name="resource_count" width={150} title={<Label icon={<AssetIcon />} title={t('sub.resource_count_col')} />} render={s => s.resources.length} />

    ,

    <SubmissionColumn defaultLayout name="new_count" width={120} align='center' title={<Label icon={<NewRecordIcon />} title={t('sub.new_count_col')} />}

      render={s => <Label mode='tag' icon={<SubmissionStatsIcon />} title={s.matchReport ? Object.keys(s.matchReport).length : 0} />} />

    ,

    <SubmissionColumn defaultLayout name="match_count" width={120} align='center' title={<Label icon={<MatchedRecordIcon />} title={t('sub.match_count_col')} />}

      render={s => <Label mode='tag' icon={<SubmissionStatsIcon />} title={s.stats.new} />} />


  )

}