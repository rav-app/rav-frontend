import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { PropsWithChildren } from 'react'
import { useSubmissionStore } from './store'


type SubmissionLoaderProps = PropsWithChildren

export const SubmissionLoader = (props: SubmissionLoaderProps) => {

   const { children } = props

    const store = useSubmissionStore()

   const { content } = useRenderGuard({

        when: !!store.all(),
        render: children,
        orRun: store.fetchAll
    })

    return content

}