import { RecordInvalidIcon, RecordValidationIcon, RecordValidWithWaningIcon, VesselIcon } from '#record/constants'
import { GenericRecord } from '#record/model'
import { useRecordPluginDescriptors } from '#record/plugin'
import { useAppSettings } from '#settings/settings'
import { useVID } from '#vid/model'
import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { useTenantUtils } from 'apprise-frontend-iam/tenant/utils'
import { Button } from 'apprise-ui/button/button'
import { Label } from 'apprise-ui/label/label'
import { useTableData } from 'apprise-ui/table/provider'
import { useSelection } from 'apprise-ui/table/selection'
import { Table } from 'apprise-ui/table/table'
import { useTableUtils } from 'apprise-ui/table/utils'
import { Tip } from 'apprise-ui/tooltip/tip'
import { useAsyncTask } from 'apprise-ui/utils/asynctask'
import { TextIcon } from 'apprise-ui/utils/icons'
import format from 'date-fns/format'
import { saveAs } from "file-saver"
import { useCallback, useContext, useMemo, useRef, useState } from 'react'
import { BiLayer } from 'react-icons/bi'
import { DetailContext } from './context'
import './issuepanel.scss'
import { PatchAndCurrent } from './records'
import { RecordIssue } from './validator'
import { OptionMenu } from 'apprise-ui/optionmenu/optionmenu'


// full patch, issue, rowid
type IssueRow = [GenericRecord, RecordIssue, string]

export const RecordIssues = () => {

    const t = useT()

    const { recordIssueKit } = useContext(DetailContext)

    const { Drawer } = recordIssueKit

    return <Drawer className='issuepanel' unmountOnClose renderOnMount={false} fixedHeight width={1000} icon={<RecordValidationIcon />} title={t('sub.recissues_title')}>
        <Inner />
    </Drawer>

}

const Inner = () => {

    const t = useT()

    const { edited, recordIssueKit, patchRowsInFocus } = useContext(DetailContext)

    const task = useAsyncTask()

    const { toggle } = recordIssueKit

    const issuemap = edited.validationReport?.records
   
    const issueSelection = useSelection<IssueRow>()

    const recordmap = utils().index(patchRowsInFocus).by(r => r.id)

    // we must keep table stable and a useRef will do as issues don't change while the drawer is open.
    const issues = useRef(

        Object.entries(issuemap ?? {}).filter(([id]) => recordmap[id]).flatMap(([id, issues], id1) => issues.map((issue, id2) => [recordmap[id], issue, `${id1}-${id2}`] as IssueRow))

    )

    const { Column, compareStringsOf } = useTableUtils<IssueRow>()

    const rowClassName = ([patch]: IssueRow) => { return `row-${(Object.keys(issuemap ?? {}).indexOf(patch.id)) % 2}` }

    const tenants = useTenantUtils()

    const formatString = useAppSettings().resolveFormat()

    const plugin = useRecordPluginDescriptors().lookup(edited.type)

    const tablename = "recordissues"

    const tabledata = useTableData<IssueRow>(tablename)

    const vid = useVID()

    const vesselname = (patch: PatchAndCurrent) => `${patch.details?.name ?? patch.current?.details?.name ?? t('rec.unknown_vessel')} (${vid.valueOf(patch.uvi) ?? 'NEW'})`

    const downloadableIssues = issueSelection.isEmpty() ? tabledata.data: issueSelection.selected

    const download = task.make(async () => {

        const tenant = tenants.codeOf(edited.tenant)
        const filenameparts = [t(plugin.plural), format(new Date(edited.lifecycle.lastModified!), formatString)] as string[]

        if (tenant)
            filenameparts.unshift(tenant)

        saveAs(new Blob(

            [
                `${[t('sub.recissues_id'), t('sub.recissues_msg'), t('sub.recissues_type')].join(';')}\n`,
                ...downloadableIssues.map(([patch, issue]) => [vesselname(patch), issue.message, issue.type].join(';')).join("\n")

            ],

            { type: 'text/csv' })

            , `${t('sub.recissues_filename', { name: filenameparts.join('-'), count: tabledata.data.length })}.csv`)


        toggle()
    })
        .with(task => task.wait(100).show(t("sub.recissues_downloading")))
        .done()

    const downloadBtn = <Button type='primary' onClick={download}>
        { issueSelection.isEmpty() ? t('sub.recissues_download') : t('sub.recissues_download_count',{count:downloadableIssues.length})}
    </Button>

    const {IssueFilter, filteredData} = useIssueTypeFilter({data: issues.current})

    return <Table name={tablename} layout='fixed' rowClassName={rowClassName} data={filteredData} selection={issueSelection} rowId={([, , id]) => id}>

        <Table.Control>
            {downloadBtn}
        </Table.Control>

        <Table.Filter>{IssueFilter}</Table.Filter>

        <Column name='issue-type' width={120} title={<Label icon={<BiLayer />} title={t('sub.recissues_type')} />}
            text={([, { type }]) => t(`parse.${type}`)}
            sort={compareStringsOf(([, { type }]) => t(`ui.field_status_${type}`))}
            render={([, { type }]) => <Label icon={type === 'warning' ? <RecordValidWithWaningIcon /> : <RecordInvalidIcon />} mode='tag' title={t(`ui.field_status_${type}`)} />}
        />


        <Column name='issue-id' width={200} title={<Label icon={<VesselIcon />} title={t('sub.recissues_id')} />}

            text={([patch]) => patch.uvi}
            sort={compareStringsOf(([patch]) => patch.uvi)}
            render={([patch]) =>

                <Label noIcon title={vesselname(patch)} />}
        />
        <Column name='issue-msg' width={300} title={<Label icon={<TextIcon />} title={t('sub.recissues_msg')} />}
            text={([, issue]) => issue.message}
            sort={compareStringsOf(([, issue]) => issue.message)}
            render={([, issue]) => <Tip tip={issue.message}>{issue.message}</Tip>}
        />

    </Table>
}



const useIssueTypeFilter = (props: {data : IssueRow[]}) => {

    const { data } = props

    const t = useT()

    // eslint-disable-next-line
    const distinctTypes = useMemo(() => utils().dedup(data.reduce((acc, cur) => ([...acc, cur[1].type]), [] as string[])).sort(), [data])

    const [ active, setActive ] = useState(distinctTypes)

    const IssueFilter = (

        <OptionMenu active={active} setActive={setActive} label={<Label icon={<RecordValidationIcon />} title={t('sub.type_col')} />}>
            {distinctTypes.map(type => {

                return <OptionMenu.Option key={type} value={type} title={<Label icon={type === 'warning' ? <RecordValidWithWaningIcon /> : <RecordInvalidIcon />} mode='tag' title={t(`ui.field_status_${type}`)} />} />
            }
            )}
        </OptionMenu>
    )

    const issueFilter = useCallback(

        (s: IssueRow) => active.some(a => a === s[1].type)

        // eslint-disable-next-line
        , [active])  // recompute when selection changes                                                        

    const filteredData = useMemo(

        () => data.filter(issueFilter),

        [data, issueFilter])

    return { IssueFilter, filteredData }
}