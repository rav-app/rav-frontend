

import { DetailMachine, useDetailMachine } from '#submission/machine'
import { StreamBindingProvider } from 'apprise-frontend-streams/track/state'
import { TableDataProvider } from 'apprise-ui/table/provider'
import { PropsWithChildren, useContext } from 'react'
import { DetailContext } from './context'
import { DetailData, useDetailData } from './formdata'
import { Submission } from './model'



// here we mount and share 1) form state and 2) state machine.

// we use two providers: 
// 1. DataProvider: with form state, perhaps additional state, and derived data (cf. useDetailData).
// 3. MachineProvider: with all of the above plus the state machine (cf. useDetailMachine).
// we split it so that the machine and its closure can lookup form data.  
// most crucially, the machine can use and trigger effects, which are conveniently based on context lookups (cf. useDetailEffects).
// NOTE: we use the same context type for both provider, buth we mount it twice, first with form data only then with data and machine.

// GOTCHA: useDetailData and its closure can neither run effects or use the state machine.

// NOTE: we remount the entire tree when the submission changes, primarily when browsing the sidelist.
// (the alternative is to reset the form with an effect, the UI would flicker just a tad less. but remounting is simpler and cleaner).
// to do this, we base the key of the topmost component to the identifier of the incoming detail.

// GOTCHA: different submission types => different plugins => different hook sequences. 
// So when the type changes we must remount most of the tree too. 
// case 1 : we navigate to another submission. we already remount the tree there, so nothing to do.
// case 2 : we change the submission type in a '/new' form. Here we need to make additional arrangements.
// crucially, we don't need to reset the form, which may lose some data already provided (eg tenant).
// GOTCHA: useDetailData and its close can't do plugin-specific work (shouldn't use usePlugin & Co.)

// NOTE: we also use two general-purpose provider:
// 1. StreamBinding keeps tracks of uploaded files until they're flushed with saves.
// 2. TableProvider keeps track of table data (primarily records) for sharing across tabs/components.
// we mount them above the DataProvider, so they are available throughout.

export type DetailState = DetailData & { machine: DetailMachine }

export const DetailProvider = (props: PropsWithChildren<{

    submission: Submission

}>) => {

    const { children, submission } = props

    // (read above) use case: navigate to another submission.
    const key = submission.id

    return <StreamBindingProvider key={key} >
        <TableDataProvider>
                <DataProvider {...props}>
                    {children}
                </DataProvider >
        </TableDataProvider>
    </StreamBindingProvider>
}

const DataProvider = (props: PropsWithChildren<{

    submission: Submission

}>) => {

    const { children, submission } = props

    const data = useDetailData(submission)

    // (read above) use cases: switch type in '/new' form.
    const key = data.edited.type

    const value = { ...data } as DetailState

    return <DetailContext.Provider key={key} value={value}>
        <MachineProvider >
            {children}
        </MachineProvider>
    </DetailContext.Provider >
}


const MachineProvider = (props: PropsWithChildren) => {

    const { children } = props

    const machine = useDetailMachine()

    const value = { ...useContext(DetailContext), machine }

    return <DetailContext.Provider value={value}>
        {children}
    </DetailContext.Provider>


}