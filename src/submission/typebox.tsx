import { Label } from 'apprise-ui/label/label'
import { SelectBox, SingleSelectProps } from 'apprise-ui/selectbox/selectbox'
import { useRecordPluginDescriptors } from '../record/plugin'
import { SubmissionType } from './model'


export type SubmissionTypeBoxProps = SingleSelectProps<SubmissionType>

export const SubmissionTypeBox = (props: SubmissionTypeBoxProps) => {

    const plugins = useRecordPluginDescriptors()

    const render = (type: SubmissionType) => {
    
        const {name,Icon} = plugins.lookup(type)
        return <Label icon={<Icon />} title={name} />
    }


    return <SelectBox noClear options={plugins.allTypes()} render={render} {...props} />


}