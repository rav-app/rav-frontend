
import { newPhotoBytestreamFrom } from '#photographs/model'
import { useRecordCalls } from '#record/calls'
import { ExcelSerializationProps } from '#record/excelserializer'
import { GenericRecord } from '#record/model'
import { SubmissionParseContext } from '#record/parser'
import { useRecordPlugin } from '#record/plugin'
import { useSubmissionStore } from '#submission/store'
import { trxauthzType } from '#trxauth/constants'
import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { useStable } from 'apprise-frontend-core/utils/function'
import { ParseContext } from 'apprise-frontend-parse/model'
import { useMultiParser } from 'apprise-frontend-parse/parser'
import { useBytestreams } from 'apprise-frontend-streams/api'
import { Bytestream } from 'apprise-frontend-streams/model'
import { useBytestreamTracker } from 'apprise-frontend-streams/track/tracker'
import { useAsyncTask } from 'apprise-ui/utils/asynctask'
import { useDeferredRouting } from 'apprise-ui/utils/routing'
import { useContext } from 'react'
import { useAssetResolver } from './assetresolver'
import { DetailContext } from './context'
import { useSubmissionMailer } from './mailer'
import { useMatchService } from './matching'
import { Asset, MatchReport, referenceOf, useSubmissionModel } from './model'
import { useSubmissionRouting } from './routing'
import { useSubmissionValidator } from './validator'


// this groups most of side effects required by the detail UI, including:
// 1. event handlers: usually button-triggered, consent-asking,  form-sincying wrappers around pther APIs, primarily the store api.
// 2. state transition tasks, used by the machin upon entering a state: loading, validating, matching, etc.

// note: this api is stateless. it can and should be called in close proximity to its use.


export type DetailEffects = ReturnType<typeof useDetailEffects>


export const useDetailEffects = () => {

    const t = useT()

    const task = useAsyncTask()
    const resourceBindings = useBytestreamTracker()

    const routing = useSubmissionRouting()
    const deferredRouting = useDeferredRouting()
    const bytestreams = useBytestreams()

    const { submission, edited, set, reset, currentRecords, isNew, assets, setCurrentRecords, setAssets, setTrxPlausibility, resetAuxState } = useContext(DetailContext)

    const model = useSubmissionModel()
    const store = useSubmissionStore()

    const recordcalls = useRecordCalls()

    const validate = useSubmissionValidator()

    // picks resource parser from plugin then 
    // wraps a multi parser around it to work
    // with multiple resources
    const plugin = useRecordPlugin(edited.type)

    const parse = useMultiParser(plugin.parse())

    const match = useMatchService()

    const resolver = useAssetResolver(edited.type)

    const serialize = plugin.excelserialize()

    const mailer = useSubmissionMailer()

    const self = {

        resetAndLoadIfRequired: useStable(task.make(async () => {

            const target = submission.records ? submission : await store.fetchOne(submission.id)

            reset.to(target).quietly()

        })
            .with($ => $.show(t("sub.loading_one_msg")))
            .done())

        ,

        fetchCurrentRecords: useStable(task.make(async () => {

            const records = edited.records
            // all records with UVIs whose vessels haven't been fetched yet.
            const unmatchedUvis = records?.map(r => r.uvi).filter(uvi => !!uvi).filter(uvi => !currentRecords?.[uvi]).sort() ?? []

            // opt out, if we have nothing to fetch.
            if (unmatchedUvis.length === 0)
                return

            console.log("fetching current records...")

            await recordcalls.fetchForSubmission(unmatchedUvis).then(fetched => {

                console.log(`fetched ${Object.values(fetched).length}/${unmatchedUvis.length} current records...`)

                const newcurrent = Object.values(fetched).length > 0 ? { ...currentRecords, ...fetched } : currentRecords

                setCurrentRecords(newcurrent)

            })


        }).done())

        ,

        fetchTrxPlausibilityIfRequired: useStable(task.make(async () => {


            if (edited.type !== trxauthzType || !edited.tenant)
                return

            console.log("fetching count for fishing vessels eligible for transhipment...")

            await recordcalls.canTranship(edited.tenant).then(plausible => setTrxPlausibility(plausible))


        }).done())

        ,

        parseResources: useStable(task.make(async () => {

            const ctx: ParseContext<SubmissionParseContext> = {

                tenant: edited.tenant
            }

            const resources = resourceBindings.joinWith(edited.resources).map(b => ({ id: b.id, name: b.name, file: b.file }))

            // nasty case: we may have removed a new resource and have no bindings left, but still bave older resources (saved previously).
            // if we go ahead we do it for nothing, plus we reset all ops to empty because we won't have found an error.
            // detect here and short circuit.
            if (resources.length === 0)
                return

            const { data: records, issues } = await parse(resources, ctx)

            console.log({ records, issues })

            set.using(s => {

                if (!Object.values(issues).flat().find(i => i.type === 'error'))
                    s.records = records

                s.validationReport = { records: {}, resources: issues }
            })

        })
            .with(cfg => cfg
                .wait(edited.resources?.length ? 150 : 0)
                .log(`parsing ${edited.resources?.length ?? 0} resource(s) (${edited.id}) ...`)
                .show(edited.resources?.length ? 'parse.msg' : undefined)
            )
            .done())


        ,

        validateRecords: useStable(task.make(async () => {

            const records = edited.records.length

            const report = { ...edited.validationReport, ...(records ? validate(edited) : {} as any) }

            set.using(s => s.validationReport = report)

        })
            .with($ =>
                $.log(`validating records (${edited.id})...`)
                    .show(edited.records?.length ? "sub.validating" : undefined)
            ).done())

        ,

        matchRecords: useStable(task.make(async () => {

            const report = edited.records?.length ? await match(edited) : {} as MatchReport

            set.using(s => s.matchReport = report)

        })
            .with($ =>
                $.log(`matching records (${edited.id})...`)
                    .show(edited.records?.length ? "sub.matching" : undefined)
            ).done())

        ,

        uploadAssets: useStable(async (files: File[]) => {

            const streamAndFile = (file: File): [Asset, Blob] => [{ ...newPhotoBytestreamFrom(file) }, file]

            const commit = (uploaded: Bytestream[]) => {

                // override files with same reference
                const merged = utils().indexAndDedup([...assets, ...uploaded]).by(referenceOf)

                setAssets(merged)
            }

            try {

                const uploaded = await bytestreams.upload(files.map(streamAndFile), { deflateArchives: true, reportUploadErrors: commit })

                commit(uploaded)

            }
            catch { }  // we handle it in reportUploadErrors
        })

        ,

        removeAssets: useStable(async (streams: Bytestream[]) => {

            const map = utils().index(streams).by(s => s.id)

            await bytestreams.delete(streams)

            setAssets(assets.filter(a => !map[a.id]))

        })


        ,

        resolveAssets: useStable(task.make(async () => {

            set.using(s => resolver.resolve(s.records, assets))


        })
            .with($ =>
                $.log(`resolving assets (${edited.id})...`)
                    .show(edited.records?.length ? "sub.resolving" : undefined)
            ).done())

        ,

        updateStatistics: useStable(async () => {

            console.log(`computing statistics ${edited.id}...`)

            set.using(s => {

                s.stats = model.computeRecordStats(edited)

            })
        })

        ,

        save: useStable(async () => {

            const saved = await store.save(edited)

            const redirect = isNew  // capture now, before reset changes it.

            reset.to(saved).quietly()

            if (redirect)
                deferredRouting.routeAfterRenderTo(routing.innerDetailRoute(saved.id))

            return saved
        })

        ,

        revert: useStable(async () => {

            reset.toInitial.quietly()

            if (assets.length)
                self.removeAssets(assets)

            resetAuxState()

        })

        ,


        remove: () => store.remove(edited, routing.routeToList)

        ,


        submit: useStable(task.make(async () => {

            const submitted = await store.submit(edited)

            mailer.onSubmit(submitted)

            reset.to(submitted).quietly()

            return submitted

        }).withConsent({
            title: t('sub.submit_title'),
            body: t('sub.submit_msg'),
            okText: t('sub.submit_ok')
        })
            .done()
        )

        ,

        reject: useStable(task.make(async (props: { comment: string | undefined, sendMail?: boolean }) => {

            const { comment, sendMail } = props

            const rejected = await store.reject(edited, comment)

            if (sendMail)
                mailer.onReject(rejected)

            reset.to(rejected).quietly()

            return rejected

        }).done())


        ,

        restore: useStable(task.make(async () => {

            const restored = await store.restore(edited)

            reset.to(restored).quietly()

            return restored

        })
            .withConsent({
                title: t('sub.restore_approval_title'),
                body: t('sub.restore_approval_msg'),
                okText: t('sub.restore_approval_ok')
            })
            .done())


        ,

        submitForApproval: useStable(task.make(async () => {

            const submitted = await store.submitForReview(edited)

            mailer.onSubmitForApproval(submitted)

            reset.to(submitted).quietly()

            return submitted

        })
            .withConsent({
                title: t('sub.submit_approval_title'),
                body: t('sub.submit_approval_msg'),
                okText: t('sub.submit_approval_ok')
            })
            .done())

        ,

        publish: useStable(task.make(async () => {

            const published = await store.publish(edited, currentRecords)

            mailer.onPublish(published)

            reset.to(published).quietly()

            return published

        })
            .with($ => $.clockTimingAs('publish'))
            .withConsent({
                title: t('sub.publish_title'),
                body: t('sub.publish_msg'),
                okText: t('sub.publish_ok')
            })
            .done())

        ,

        serialize: useStable(task.make(async <T extends GenericRecord>(props: ExcelSerializationProps<T>) => {

            return serialize(props)

        }).done())

    }

    return self


}