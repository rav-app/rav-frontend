import { useSubmissionCalls } from "#submission/calls"
import { MatchReport, RecordMatch, Submission } from "#submission/model"
import { useT } from "apprise-frontend-core/intl/language"
import { useAsyncTask } from 'apprise-ui/utils/asynctask'





export const useMatchService = () => {

  const t = useT()
  const calls = useSubmissionCalls()

  const task = useAsyncTask()

  return  task.make( async (submission: Submission) => {

       // sends out only records to match.
       var minimalSubmission = {...submission, records: submission.records.filter(p => !p.uvi)}

       var matchmap =  await calls.match(minimalSubmission)

      // creates an unmatched entry also for new records that have no matches, 
      // so even after publication we can use "unmatched" to tell which records were originally new, 
      // regardless of wether there were no matches or all matches were rejected. 
      var report  = minimalSubmission.records.reduce( (acc,newpatch) => {
      
        const matches = matchmap[newpatch.id]
        
        let entry = matches?.length ? {matches} : {matches:[]} as RecordMatch

        if (!matches?.length)
          entry.unmatched=true
        
        return {...acc, [newpatch.id]: entry}
      
      
      }, {} as MatchReport)
         
      //console.log(report)

      return report
  })
  .with($ => $.throw(t('sub.match_error')))
  .done()

}
