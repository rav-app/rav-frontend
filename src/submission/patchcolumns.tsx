import { RecordIdIcon, RecordInvalidIcon, RecordStateIcon, RecordValidIcon, RecordValidWithWaningIcon } from "#record/constants"
import { GenericRecord, RecordPatch } from "#record/model"
import { useRecordOracle } from "#record/oracle"
import { useRecordPlugin } from "#record/plugin"
import { useT } from "apprise-frontend-core/intl/language"
import { Label } from "apprise-ui/label/label"
import { useTableUtils } from "apprise-ui/table/utils"
import { Tip } from 'apprise-ui/tooltip/tip'
import { IconBaseProps } from 'react-icons'
import { Submission } from "./model"
import { PatchLabel } from "./patchlabel"
import { PatchAndCurrent } from "./records"


export const usePatchColumns = (sub: Submission, routeAt: (match?: string | undefined) => string) => {

  const t = useT()

  const { currentColumns, patchColumns } = useRecordPlugin(sub.type)

  const { Column: PatchColumn, compareStringsOf, columnsOf } = useTableUtils<PatchAndCurrent>()

  const patchCols = patchColumns(sub)

  const currCols = currentColumns()

  return columnsOf(

    <PatchColumn defaultLayout name="vessel-uvi" width={160}
      text={patch => patch.uvi}
      sort={compareStringsOf(s => s.uvi)} title={<Label icon={<RecordIdIcon />} title={t('rec.uvi_col')} />}
      // note: linkTo uses the label's connection to routing to use a fresh search path 
      render={r => <PatchLabel current={r.current} icon={<PatchValidationStatusIcon submission={sub} patch={r} />}
        patch={r} linkTo={routeAt(r.id)} />} />
    ,


    <PatchColumn defaultLayout name="vessel-state" width={200}
      text={patch => t(`rec.state_${patch?.lifecycle.state}`)}
      sort={compareStringsOf(s => s.lifecycle.state)} title={<Label icon={<RecordStateIcon />} title={t('rec.state_col')} />} render={(r: GenericRecord) => <PatchLabel mode='tag' displayMode='state' patch={r} />} />

    ,

    ...(Array.isArray(currCols) ? currCols : [currCols])

    ,

    ...(Array.isArray(patchCols) ? patchCols : [patchCols])

    ,

  )
}




export const PatchValidationStatusIcon = (props: IconBaseProps & {

  submission: Submission,
  patch: RecordPatch

}) => {

  const t = useT()

  const { submission, patch, ...rest } = props

  const issues = submission?.validationReport?.records?.[patch.id]
  const count = issues?.length ?? 0
  const oracle = useRecordOracle().given(submission)
  const status = oracle.validationStatusOf(patch)
  const Icon = status === 'invalid' ? RecordInvalidIcon : (count > 0 ? RecordValidWithWaningIcon : RecordValidIcon)

  const icon = <Icon style={{ marginRight: 4 }} {...rest} />

  return issues?.length ? <Tip tip={t('rec.issue_count', { count })}>
    {icon}
  </Tip> : icon
}