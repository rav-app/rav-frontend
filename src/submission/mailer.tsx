import { subchangePendingTopic, subchangePublishedTopic, subchangeRejectedTopic, subchangeSubmittedTopic } from '#modules/mail'
import { noTenant } from "apprise-frontend-iam/tenant/model"
import { useTenantStore } from 'apprise-frontend-iam/tenant/store'
import { useUserStore } from 'apprise-frontend-iam/user/store'
import { useMail } from "apprise-frontend-mail/api"
import { Button } from 'apprise-ui/button/button'
import { Submission, useSubmissionUtils } from "./model"


const template = 'subchange'


export const useSubmissionMailer = () => {


    const mail = useMail()
    const users = useUserStore()
    const tenants = useTenantStore()

    const commonParamsOf = (submission: Submission) => ({

        tenant: tenants.safeLookup(submission.tenant).name,
        status: submission.lifecycle.state!,
        submissionId: submission.id
    })

    const self = {

        onSubmitForApproval: async (submission: Submission) =>

            mail.sendTo(submission.tenant)
                .template(template)
                .params(commonParamsOf(submission))
                .on(subchangePendingTopic)

        ,

        onSubmit: (submission: Submission) =>

            mail.sendTo(noTenant)
                .template(template)
                .params(commonParamsOf(submission))
                .on(subchangeSubmittedTopic)

        ,

        onReject: (submission: Submission) =>

            mail.sendTo(submission.tenant)
                .template(template)
                .params({ ...commonParamsOf(submission), rejectioncomment: { key: submission.lifecycle.rejectedWith ?? 'template.subchange.rejected.nocomment' } })
                .on(subchangeRejectedTopic)

        ,

        onPublish: (submission: Submission) => {

            const submitterTenant = users.safeLookup(submission.lifecycle.submittedBy)?.tenant

            const recipients = submitterTenant === submission.tenant || submitterTenant === noTenant ? submission.tenant : [submission.tenant, submission.lifecycle.submittedBy!]

            return mail.sendTo(recipients)
                .template(template)
                .params({ ...commonParamsOf(submission), ...(submission.stats.rejected > 0 ? { rejections: { key: 'template.subchange.pubnote.rejections' } } : {}) })
                .on(subchangePublishedTopic)


        }


    }

    return self

}


export const SubmissionMailTest = (props: Partial<{

    submission: Submission

}>) => {

    const mailer = useSubmissionMailer()

    const { submission: clientsub } = props

    const utils = useSubmissionUtils()

    const defaultSubission = () => {

        const sub = utils.newSubmission()

        sub.lifecycle.state = 'pending'
        sub.tenant = 'T-AUS'

        return sub as Submission
    }

    const submission = clientsub ?? defaultSubission()

    return <Button onClick={() => mailer.onSubmitForApproval(submission)}>{`Test Mail (${submission?.lifecycle.state})`}</Button>
}