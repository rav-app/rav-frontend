
import { SlotViewer } from '#record/slotviewer'
import { MatchingForm } from '#submission/matchform'
import { PatchAndCurrent } from '#submission/records'
import { recordDetailParam } from '#submission/routing'
import { trxauthzType } from '#trxauth/constants'
import { useVesselCache } from '#vessel/cache'
import { useVesselCalls } from '#vessel/calls'
import { Vessel } from '#vessel/model'
import { useVesselRouting } from '#vessel/routing'
import { useVID } from '#vid/model'
import { useT } from 'apprise-frontend-core/intl/language'
import { Button, ButtonRow } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { useDrawer, useRoutableDrawer } from 'apprise-ui/drawer/drawer'
import { Topbar } from 'apprise-ui/topbar/topbar'
import { OpenIcon } from 'apprise-ui/utils/icons'
import { NamedPropertyContext } from 'apprise-ui/utils/namedproperty'
import isAfter from 'date-fns/isAfter'
import React, { useContext, useMemo, useState } from 'react'
import { BsToggle2Off, BsToggle2On } from 'react-icons/bs'
import { MatchedRecordIcon, RecordValidationIcon } from '../record/constants'
import { GenericRecord, RavRecord, RecordPatch, SlotType } from '../record/model'
import { useRecordOracle } from '../record/oracle'
import { useRecordPlugin } from '../record/plugin'
import { DetailContext } from './context'
import './patchdetail.scss'
import { ValidationForm } from './validationform'


export type PatchDetailProps<T extends GenericRecord = GenericRecord> = {

    record: PatchAndCurrent<T>
}

export type PatchCardProps<T extends RecordPatch = GenericRecord> = {

    record: T
    previous: T | undefined
    current: boolean
}

export const PatchDetail = <T extends GenericRecord>(props: PatchDetailProps<T>) => {

    const t = useT()
    const vid = useVID()
    const vessels = useVesselRouting()

    const { record } = props

    const { edited } = useContext(DetailContext)

    const { Drawer } = useRoutableDrawer(recordDetailParam)

    const { Drawer: ValidationDrawer, open: validationOpen, openSet: openValidation, toggle:toggleValidation } = useDrawer()


    const { Drawer: MatchDrawer, open: matchesOpen, openSet: openSetMatch } = useDrawer()

    const [match, matchSet] = React.useState<RavRecord>(undefined!)

    const closeSubDrawers = () => {
        openValidation(false)
        openSetMatch(false)
    }
    
    const toggleMatch = () => {

        if (matchesOpen) {

            openSetMatch(false)
            matchSet(undefined!)
        }
        else

            openSetMatch(true)
    }


    const oracle = useRecordOracle().given(edited)

    const { Icon, patchedTypes } = useRecordPlugin<T>(edited.type)

    const validationDrawerWidth = 450
    const matchDrawerWidth = 500

    // this is either a new record with a name, or else patches the current record.
    // as we fetch the current record, or if fetching fails (eg patch is invalid), we need a placeholder for the name.
    // we use the uvi if we have it, or a precanned message (no uvi, no name? definetely an invalid 'new record').
    // in all cases, we dim the placeholder because, if we then replace it, we want the least noticeabkle flash. 
    const drawerTitle = record.details?.name ?? record.current?.details?.name ??

        <span style={{ color: 'lightgrey' }}>{

            record.uvi === undefined ? t('rec.new_record_title') : vid.valueOf(record.uvi)

        }</span>


    const status = oracle.validationStatusOf(record)
    const matchStatus = oracle.matchStatusOf(record)
    const validationColor = matchesOpen ? undefined : status === 'invalid' ? 'orangered' : oracle.hasValidationIssues(record) ? 'orange' : 'lightseagreen'
    const matchColor = validationOpen ? undefined : matchStatus === 'pending' ? 'orange' : matchStatus === 'new' ? 'lightseagreen' : 'dodgerblue'

    // note: we load vessel histories here, but we can make that seamless, and avoid a spinner altogether (noBusuGuard on drawer).

    const vesselLink = <Button noReadonly disabled={!record.uvi} tipPlacement='left' tip={t('rec.vessel_link_tip')} noLabel type='ghost'
        iconPlacement='left' icon={<OpenIcon size={22} color={record.uvi ? undefined : 'dodgerblue'} />}
        onClick={() => vessels.routeToDetail(record.uvi, 0, undefined, true)} />

    const cache = useVesselCache()
    const calls = useVesselCalls()


    const uvi = record.uvi
    const cached = cache.get(uvi)

    // if we haven't cached it before, we make an initial mini-history just with the current record, if we have it.
    // this means we can load the full history "backstage", ie. hide a spinner altogether.
    const initialVessel: Vessel = cached ?? { uvi, history: (record.current ? [record.current] : []) }

    const [fetched, fetchedSet] = React.useState(initialVessel)

    // go fetch more history if we haven't one in the cache already (unless record is new). 
    React.useEffect(() => {

        if (!cached && uvi)
            calls.fetchVessel(uvi).then(fetchedSet)

        // eslint-disable-next-line
    }, [cached, uvi])

    // if the patch is timestamped, goes back in time and considers history at the time of this patch.
    // so we can always compare a patch long published with the current record at the time.
    const historyBeforePatch = record.timestamp ? fetched.history.filter(r =>r.timestamp === record.timestamp || isAfter(Date.parse(record.timestamp), Date.parse(r.timestamp))) : fetched.history

    // augment history with this patch, camouflaging it as a full record for our purposes.
    const augmentedVessel: Vessel = { ...fetched, history: [record as RavRecord, ...(match ? [match] : historyBeforePatch)] }

    const classes = classname('patch-detail', match && 'match-mode')

      
    return <Drawer onClose={closeSubDrawers} className={classes} noBusyGuard width='auto' translation={validationOpen ? validationDrawerWidth : matchDrawerWidth} icon={<Icon />} title={drawerTitle} titleSuffix={vesselLink}>

        <Topbar>
            <ButtonRow>

                <Button noReadonly disabled={matchesOpen} iconPlacement='left' icon={<RecordValidationIcon color={validationColor} />} onClick={toggleValidation}>
                    <span style={{ color: validationColor }}>{validationOpen ? t('rec.validation_issues_close') : oracle.hasValidationIssues(record) ? `${t('rec.validation_issues')}...` : t('rec.validation_noissues')}</span>
                </Button>

                {oracle.hasMatches(record) &&

                    <Button noReadonly disabled={validationOpen} iconPlacement='left' icon={<MatchedRecordIcon color={matchColor} />} onClick={toggleMatch}>
                        <span style={{ color: matchColor }}>{matchesOpen ? t('rec.matching_matches_close') : t('rec.matching_matches')}...</span>
                    </Button>

                }

            </ButtonRow>
        </Topbar>

        {
            patchedTypes.map(type =>

                <PatchViewer key={type} vessel={augmentedVessel} type={type} match={match} />

            )
        }

        <ValidationDrawer noMask width={validationDrawerWidth} title={t('rec.validation_drawer')} icon={<RecordValidationIcon />}>
            <ValidationForm {...props} close={() => openValidation(false)} />
        </ValidationDrawer>

        <MatchDrawer onClose={toggleMatch} noMask width={matchDrawerWidth} title={t('rec.matching_drawer')} icon={<MatchedRecordIcon />}>
            <MatchingForm {...props} onMatchSelection={matchSet} matchSelected={match} />
        </MatchDrawer>


    </Drawer >
}

type PatchViewerProps = {

    vessel: Vessel
    type: SlotType
    match: RavRecord
}



const PatchViewer = <T extends GenericRecord>(props: PatchViewerProps) => {

    const t = useT()

    const { vessel, type, match } = props

    const { Patch } = useRecordPlugin<T>(type)

    // patch slot and current slot, if any.
    let minimalHistoryForType = vessel.history.filter(r => r[type]).slice(0, 2) as T[]

    const now = Date.now()

    const trxPatch = type === trxauthzType

    if (trxPatch) {


        const prev = minimalHistoryForType[1]

        const prevauthz = prev?.trxauthz?.authzs[minimalHistoryForType[0].tenant]?.filter(a => isAfter(Date.parse(a.to), now)) ?? []
        
        const unfolded = prevauthz.map( a => ({ ...prev, trxauthz: {...prev.trxauthz, timestamp: a.timestamp, authzs: { [minimalHistoryForType[0].tenant]: [a] } }}))
    
        minimalHistoryForType = [minimalHistoryForType[0],...unfolded]
    }


    const [history, historySet] = React.useState(false)

    // if we're matching, start highlighting matches.
    const [matchMode, setMatchMode] = useState(!!match)

    const toggleTip = t(matchMode ? 'rec.matching_highlight_diff' : 'rec.matching_highlight_match')

    const ToggleIcon = matchMode ? BsToggle2Off : BsToggle2On


    const toggleModeBtn = <Button noReadonly disabled={minimalHistoryForType.length < 2 || trxPatch} key='highlights' type='ghost' noLabel iconPlacement='left' tip={toggleTip} onClick={() => setMatchMode(s => !s)}>
        <ToggleIcon />
    </Button>

    const namedprops = useMemo(() => ({ highlightMode: matchMode ? 'match' as const : 'change' as const }), [matchMode])

    return <NamedPropertyContext.Provider value={namedprops}>

        <SlotViewer current={minimalHistoryForType[0] as T} type={type} noControls={!!match} extraControls={[toggleModeBtn]}
            historyForType={minimalHistoryForType}
            onModeToggle={() => historySet(s => !s)}
            historyMode={!!match || history}        // show history if we're matching or user toggled.
            render={({ record, previous, age }) => <Patch current={age === 0} record={record} previous={previous} />}
        />
    </NamedPropertyContext.Provider>


}