
import { useT } from 'apprise-frontend-core/intl/language'
import { Button } from 'apprise-ui/button/button'
import { Form } from 'apprise-ui/form/form'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { useContext, useState } from 'react'
import { RiForbid2Fill } from "react-icons/ri"
import { DetailContext } from './context'
import './rejectpanel.scss'

import { useTenantUtils } from 'apprise-frontend-iam/tenant/utils'
import { ChoiceBox } from 'apprise-ui/choicebox/choicebox'
import { TextBox } from 'apprise-ui/textbox/textbox'
import { SubmissionRejectedIcon, rejectedColor } from './constants'
import { useDetailEffects } from './effects'
import { useSubmissionOracle } from './oracle'

export const RejectPanel = () => {


    const t = useT()

    const { rejectKit, edited } = useContext(DetailContext)

    const { Drawer } = rejectKit

    const oracle = useSubmissionOracle()

    return <Drawer readonly={!oracle.canReject(edited)} icon={<SubmissionRejectedIcon />} width={450} title={t("sub.reject_title")}>
        <Inner />
    </Drawer>

}

export const Inner = () => {

    const t = useT()

    const effects = useDetailEffects()

    const [comment, setComment] = useState<string | undefined>()

    const tenants = useTenantUtils()

    const { rejectKit, edited } = useContext(DetailContext)


    const [sendMail, setSendMail] = useState<boolean|undefined>(true)
    
    const reject = async () => {
        
        await effects.reject({comment, sendMail})
    
        rejectKit.openSet(false)
    }


    const party = tenants.nameOf(edited.tenant)

    return <ExplainerPanel className='reject-panel'>

        <Explainer icon={<RiForbid2Fill color={rejectedColor} />} >
            {t("sub.reject_explainer")}
        </Explainer>


        <Form centered>

            <TextBox label={t('sub.reject_comment_lbl')} msg={t('sub.reject_comment_msg')} multi={{start:3,end:5}} onChange={c => setComment(c)}>
                {comment}
            </TextBox>

            <ChoiceBox value={sendMail} msg={t('sub.reject_sendmail_msg',{party})}  onChange={setSendMail}>
                <ChoiceBox.Option title={t('sub.reject_sendmail_lbl',{party})} value={true} />
            </ChoiceBox>

        </Form>

        <Button className='reject-btn' type='danger' icon={<SubmissionRejectedIcon />} onClick={reject}>
                {t("sub.reject_now")}
        </Button>


    </ExplainerPanel >
}
