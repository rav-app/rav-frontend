import { baseComboType } from "#basecombo/mode"
import { MatchedRecordIcon, NewRecordIcon, RecordInvalidIcon, RecordPublishedIcon, RecordRejectedIcon, RecordValidWithWaningIcon } from "#record/constants"
import { useT } from "apprise-frontend-core/intl/language"
import { Label, LabelRow } from "apprise-ui/label/label"
import { Tip } from 'apprise-ui/tooltip/tip'
import React from 'react'
import { SubmissionStatsIcon } from './constants'
import { Submission } from "./model"

export const SubmissionStats = ({ sub, mode = 'all' }: React.PropsWithChildren<{

  sub: Submission

  mode?: 'core' | 'all'

}>) => {

  const t = useT()


  return <LabelRow noTruncate>

    <Tip tip={t(t('sub.parsed_tip'))}><Label mode='tag' icon={<SubmissionStatsIcon />} title={sub.stats?.parsed ?? 0} /></Tip>

    {( mode==='all' || sub.lifecycle.state !== 'published') &&

      <React.Fragment>

        <Tip tip={t('sub.error_tip')} ><Label mode='tag' icon={<RecordInvalidIcon />} title={sub.stats?.errors ?? 0} /></Tip>

        <Tip tip={t('sub.warning_tip')} ><Label mode='tag' icon={<RecordValidWithWaningIcon />} title={sub.stats?.warnings ?? 0} /></Tip>

      </React.Fragment>
    }



    {sub.lifecycle.state === 'published' &&

      <React.Fragment>

        <Tip tip={t('sub.published_tip')} ><Label mode='tag' icon={<RecordPublishedIcon />} title={sub.stats?.published ?? 0} /></Tip>

        <Tip tip={t('sub.rejected_tip')}><Label mode='tag' icon={<RecordRejectedIcon />} title={sub.stats?.rejected ?? 0} /></Tip>

      </React.Fragment>
    }

    {mode === 'all' &&

      <React.Fragment>

        {sub.type === baseComboType &&

          <React.Fragment>

            <Tip tip={t('sub.new_tip')} ><Label mode='tag' icon={<NewRecordIcon color='limegreen' />} title={sub.stats?.new ?? 0} /></Tip>

            <Tip tip={t('sub.matchpending_tip')}><Label mode='tag' icon={<MatchedRecordIcon color='orange' />} title={sub.stats?.matchPending ?? 0} /></Tip>

          </React.Fragment>}

      </React.Fragment>

    }


  </LabelRow>
}

