
import { useRecordPlugin } from '#record/plugin'
import { useT } from 'apprise-frontend-core/intl/language'
import { useTenantUtils } from 'apprise-frontend-iam/tenant/utils'
import { Button } from 'apprise-ui/button/button'
import { ChoiceBox } from 'apprise-ui/choicebox/choicebox'
import { Form } from 'apprise-ui/form/form'
import { Explainer, ExplainerPanel } from 'apprise-ui/utils/explainer'
import { DownloadIcon } from 'apprise-ui/utils/icons'
import format from 'date-fns/format'
import { useContext, useState } from 'react'
import { ImDownload } from 'react-icons/im'
import { DetailContext } from './context'
import './downloadpanel.scss'
import { useDetailEffects } from './effects'

export const DownloadPanel = () => {


    const t = useT()

    const { downloadKit, patchRowsInFocus } = useContext(DetailContext)



    const { Drawer } = downloadKit

    return <Drawer icon={<DownloadIcon />} width={450} noReadonly title={t("sub.download_records", { count: patchRowsInFocus.length })}>
        <Inner />
    </Drawer>

}

export const Inner = () => {

    const t = useT()

    const tenants = useTenantUtils()

    const { edited, patchRowsInFocus } = useContext(DetailContext)

    const effects = useDetailEffects() 

    const [mode, modeSet] = useState<'codes' | 'names'>('codes')

    const plugin = useRecordPlugin(edited.type)

    const download = async () => {

        const output = await effects.serialize({data:patchRowsInFocus, mode, sheetname: t('sub.download_sheet')})

        const filename = [
        
            tenants.nameOf(edited.tenant),

            plugin.name,
            
            format(edited.lifecycle.created ?? Date.now(), "P")
    
        ].filter(n => !!n).join('_').replaceAll(' ', '-')


        output.save(`${filename}.xlsx`)

    }


    return <ExplainerPanel className='download-panel'>

        <Explainer icon={ImDownload} >
            {t("sub.download_explainer", { count: patchRowsInFocus.length })}
            <br />
            {t("sub.download_explainer2")}
        </Explainer>


        <Form centered>

            <ChoiceBox radio value={mode} onChange={v => modeSet(v ?? 'codes')}>
                <ChoiceBox.Option value='codes' title={t('search.download_codes')} />
                <ChoiceBox.Option value='names' title={t('search.download_names')} />
            </ChoiceBox>

        </Form>

        <Button className='download-btn' type='primary' icon={<DownloadIcon />} onClick={download}>
                {t("sub.download_now")}
        </Button>


    </ExplainerPanel >
}
