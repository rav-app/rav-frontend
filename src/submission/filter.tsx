import { RecordValidationIcon } from '#record/constants'
import { SlotType } from '#record/model'
import { useT } from "apprise-frontend-core/intl/language"
import { utils } from 'apprise-frontend-core/utils/common'
import { TenantIcon } from 'apprise-frontend-iam/tenant/constants'
import { TenantLabel } from "apprise-frontend-iam/tenant/label"
import { useTenantUtils } from 'apprise-frontend-iam/tenant/utils'
import { Label } from "apprise-ui/label/label"
import { OptionMenu } from "apprise-ui/optionmenu/optionmenu"
import { ContextualFilterProps, useFilterState } from 'apprise-ui/utils/filter'
import format from 'date-fns/format'
import { useCallback, useMemo } from "react"
import { useRecordPluginDescriptors } from '../record/plugin'
import { SubmissionDateIcon, SubmissionStateIcon, SubmissionTypeIcon } from "./constants"
import { Submission, SubmissionLifecycleState } from "./model"
import { useSubmissionOracle } from './oracle'
import { ValidationState } from './validator'


type FilterProps = ContextualFilterProps & {
    data: Submission[]
    initial?: Submission[]
}

export const useYearFilter = (props: FilterProps) => {

    const { context, contextKey = 'submissionyear', data, initial = data } = props

    const t = useT()

    const distinctYears = useMemo(() =>

        utils().dedup(initial.flatMap(r => r.lifecycle.created ? format(r.lifecycle.created, 'yyyy') : [])).sort()

        , [initial])

    const { get, set } = useFilterState<string[]>(context)

    const active = get(contextKey) ?? distinctYears
    const setActive = set(contextKey)

    const YearFilter = (

        <OptionMenu active={active} setActive={setActive} label={<Label icon={<SubmissionDateIcon />} title={t('sub.year')} />}>
            {distinctYears.map(year =>
                <OptionMenu.Option
                    key={year}
                    value={year}
                    title={
                        <Label noIcon title={year} />
                    }
                />
            )}
        </OptionMenu>
    )

    const yearfilter = useCallback(

        (s: Submission) => active.some(a => a === format(s.lifecycle.created ?? new Date(), 'yyyy'))

        // eslint-disable-next-line
        , [active])  // recompute when selection changes

    const filteredData = useMemo(

        () => data.filter(yearfilter),

        [data, yearfilter])

    return { YearFilter, yearfilter, filteredData, active, setActive }
}


type ValidationFilterProps = ContextualFilterProps & {

    data: Submission[],
    initial?: Submission[]
}


type ValidationOptions = ValidationState | 'flagged'

const validationOptions: ValidationOptions[] = ['valid', 'invalid', 'flagged']

export const useValidationFilter = (props: ValidationFilterProps) => {

    const t = useT()

    const { context, contextKey = 'submissionvalidation', data, initial=data } = props

    const oracle = useSubmissionOracle()

    const existingOptions: ValidationOptions[] = useMemo(() => {

        return utils().dedup(initial.map(s => oracle.validationState(s))).sort()

        // eslint-disable-next-line
    }, [initial])

    const state = useFilterState<ValidationOptions[]>(context)

    const active = state.get(contextKey) ?? existingOptions

    const setActive = state.set(contextKey)

    const ValidationFilter = (

        <OptionMenu key={`${existingOptions.length}`} active={active} setActive={setActive} label={<Label icon={<RecordValidationIcon />} title={t('sub.valid_filter')} />}>
          
            {validationOptions.map(opt => {

                const title = opt ? t(`rec.validation_${opt}`) : t('rec.no_validation')

                return <OptionMenu.Option
                    key={opt}
                    disabled={!existingOptions.includes(opt)}
                    value={opt}
                    title={title} />
            }
            )}
        </OptionMenu>
    )

    const validationfilter = useCallback(

        (s: Submission) => {

            const status = oracle.validationState(s)

            return active.some(a => a === status)
        }
        // eslint-disable-next-line
        , [active])  // recompute when selection changes

    const filteredData = useMemo( ()=>
    
        data.filter(validationfilter)

     // eslint-disable-next-line
    ,[data, validationfilter])

   

    return { ValidationFilter, validationfilter, filteredData, active, setActive }
}



export const usePartyFilter = (props: FilterProps) => {

    const { context, contextKey = 'submissionparty', data, initial = data } = props

    const tenants = useTenantUtils()

    const t = useT()

    const distinctParties = useMemo(() =>

        utils().dedup(initial.flatMap(r => r.tenant ?? [])). sort(tenants.refComparator)

        // eslint-disable-next-line
        , [initial])

    const { get, set } = useFilterState<string[]>(context)

    const active = get(contextKey) ?? distinctParties
    const setActive = set(contextKey)

    const PartyFilter = (

        <OptionMenu active={active} setActive={setActive} height={400} label={<Label icon={<TenantIcon />} title={t('sub.tenant_col')} />}>
            {distinctParties.map(party =>
                <OptionMenu.Option
                    key={party}
                    value={party}
                    title={
                        <TenantLabel bare noIcon tenant={party} />
                    }
                />
            )}
        </OptionMenu>
    )

    const partyfilter = useCallback(

        (s: Submission) => active.some(a => a === s.tenant)

        // eslint-disable-next-line
        , [active])  // recompute when selection changes

    const filteredData = useMemo(

        () => data.filter(partyfilter),

        [data, partyfilter])


    return { PartyFilter, partyfilter, filteredData, active, setActive }
}

export const useStateFilter = (props: FilterProps) => {

    const { context, contextKey = 'submissionstate', data, initial = data } = props

    const t = useT()

    const distinctStatesButRejected = useMemo(() =>

        utils().dedup(initial.flatMap(r => r.lifecycle.state ?? [])).sort().filter(s => s!=='rejected')

        , [initial])

    const { get, set } = useFilterState<SubmissionLifecycleState[]>(context)

    const distinctStates = [...distinctStatesButRejected, 'rejected']

    const active = get(contextKey) ?? distinctStatesButRejected
    const setActive = set(contextKey)

    const StateFilter = (

        <OptionMenu highlightOn={distinctStatesButRejected} active={active} setActive={setActive} label={<Label icon={<SubmissionStateIcon />} title={t('sub.state_col')} />}>
            {distinctStates.map(state =>
                <OptionMenu.Option
                    key={state}
                    value={state}
                    title={<Label noIcon title={t(`sub.state_${state}`)} />}
                />
            )}
        </OptionMenu>
    )

    const statefilter = useCallback(

        (s: Submission) => active.some(a => a === s.lifecycle.state)

        // eslint-disable-next-line
        , [active])  // recompute when selection changes

    const filteredData = useMemo(

        () => data.filter(statefilter),

        [data, statefilter])


    return { StateFilter, statefilter, filteredData, active, setActive }
}

export const useTypeFilter = (props: FilterProps) => {

    const { context, contextKey = 'submissiontype', data, initial = data } = props

    const t = useT()

    const plugins = useRecordPluginDescriptors()

    const distinctTypes = useMemo(() =>

        utils().dedup(initial.map(r => r.type)).sort()

        , [initial])

    const { get, set } = useFilterState<SlotType[]>(context)

    const active = get(contextKey) ?? distinctTypes
    const setActive = set(contextKey)

    const TypeFilter = (

        <OptionMenu active={active} setActive={setActive} label={<Label icon={<SubmissionTypeIcon />} title={t('sub.type_col')} />}>
            {distinctTypes.map(type => {

                const plugin = plugins.lookup(type)
                return <OptionMenu.Option key={type} value={type} title={<Label noIcon title={plugin.name} />} />
            }
            )}
        </OptionMenu>
    )

    const typefilter = useCallback(

        (s: Submission) => active.some(a => a === s.type)

        // eslint-disable-next-line
        , [active])  // recompute when selection changes                                                        

    const filteredData = useMemo(

        () => data.filter(typefilter),

        [data, typefilter])


    return { TypeFilter, typefilter, filteredData, active, setActive }
}