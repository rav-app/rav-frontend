import { SubmissionTypeBox } from '#submission/typebox'
import { useT } from 'apprise-frontend-core/intl/language'
import { TenantReference } from 'apprise-frontend-iam/tenant/model'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { TenantBox } from 'apprise-frontend-iam/tenant/tenantbox'
import { NoteBox } from 'apprise-frontend-iam/user/notebox'
import { useBytestreams } from 'apprise-frontend-streams/api'
import { Bytestream } from 'apprise-frontend-streams/model'
import { useBytestreamTracker } from 'apprise-frontend-streams/track/tracker'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { FileBox } from 'apprise-ui/filebox/filebox'
import { Form } from 'apprise-ui/form/form'
import { Label } from 'apprise-ui/label/label'
import { useFeedback } from 'apprise-ui/utils/feedback'
import { DotIcon } from 'apprise-ui/utils/icons'
import { useContext } from 'react'
import { DetailContext } from './context'
import { useSubmissionFields } from './fields'
import { SubmissionType, useSubmissionModel } from './model'
import './profile.scss'
import { useSubmissionOracle } from './oracle'
import { useTenantStore } from 'apprise-frontend-iam/tenant/store'

const max_issue_show = 10

export const GeneralForm = () => {

    const t = useT()

    const tenants = useTenantStore()
    const streams = useBytestreams()
    const tracker = useBytestreamTracker()

    const { edited, set, machine, isNew } = useContext(DetailContext)

    const model = useSubmissionModel()

    const oracle = useSubmissionOracle()
    const logged = useTenancyOracle()
    const { ask } = useFeedback()

    const fields = useSubmissionFields()

    // actions

    // change type and resets derived data.
    const changeType = (type?: SubmissionType) => {

        const change = () => set.using(m => {

            m.type = type!
            m.records = []
            m.resources = []

        })

        // ask consent if we're going to lose information.
        if (edited.resources?.length > 0 || edited.records?.length > 0)
            ask({
                title: t('sub.typechange_warning_title'),
                body: t('sub.typechange_warning_body'),

            })
                .thenRun(change)

        else
            change()
    }

    const resourcesInfo =  {...fields.resources}

    if (Object.keys(edited.validationReport?.resources ?? {}).length > 0) {

        var errors =  model.resourceIssues(edited, {type:'error'}).length

        resourcesInfo.status =  errors > 0 ? 'error' as const : 'warning' as const
        resourcesInfo.msg = t(`sub.has_${errors > 0 ? 'errors' : 'warnings'}_msg`)
    }


    const changeTenant = (tenant: TenantReference | undefined) => {

        set.using(s => s.tenant = tenant!)

        if (edited.resources?.length)
            machine.trigger('parse')

    }

    const allowedTenants = tenants.allSorted().filter(t => oracle.canAddFor(t.id)).sort()

    return <Form>

        {logged.isCrossTenant() &&
            <TenantBox placeholder={undefined} tenants={allowedTenants} noClear readonly={!isNew} info={fields.tenant} onChange={changeTenant}>
                {edited.tenant}
            </TenantBox>
        }

        {/* 
            right now  we don't allow resetting the type on existing details, even when the context isn't readonly. 
            this is because we remount the detail on a type change, so changes would be lost.
        */}
        <SubmissionTypeBox disabled={edited.live} info={fields.type} readonly={!isNew} onChange={changeType}>
            {edited.type}
        </SubmissionTypeBox>

        { edited.live || 
        
            <FileBox  enabled={!!edited.type} readonly={!oracle.canUploadResources(edited)} multi info={resourcesInfo}

            onChange={set.with((m, v) => {

                m.resources = v

                machine.trigger('parse')

            })}

            descriptorOf={(file: File) => {

                const stream = streams.newFromFile(file)

                tracker.add(stream.id, file)

                return stream
            }}

            render={(stream: Bytestream) => {

                const issues = model.resourceIssues(edited, { resource: stream.id })

                const classes = classname(issues?.length > 0 && (issues.find(issue => issue.type === 'error') ? 'parsing-error' : 'parsing-warning'))

                const title = <div className={classes}>{stream.name}</div>

                const maxIssues = Math.min(issues?.length, max_issue_show)

                const issueslabels = issues?.slice(0, maxIssues)?.map((issue, i) =>

                    <Label key={i} icon={<DotIcon color={issue.type === 'warning' ? 'orange' : 'red'} />} title={issue.message} />
                )

                if (maxIssues < issues?.length)
                    issueslabels.push(
                        <Label noIcon key='more' title={t('sub.more_errors', { count: issues?.length - maxIssues })} />
                    )

                const tips = issueslabels ? <>{issueslabels}</> : undefined

                return <Button noReadonly type='ghost' tipWidth={500} tip={tips} onClick={() => tracker.download(stream)}>{title}</Button>

            }}>
            {edited.resources}
        </FileBox> }

        <NoteBox noReadonly info={fields.note} onChange={set.with((m, v) => m.note = v)} >
            {edited.note}
        </NoteBox>

    </Form>
}