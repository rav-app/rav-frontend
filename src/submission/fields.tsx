import { trxauthzType } from '#trxauth/constants'
import { useT } from 'apprise-frontend-core/intl/language'
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant'
import { useTenantUtils } from 'apprise-frontend-iam/tenant/utils'
import { useValidation } from 'apprise-ui/field/validation'
import { useContext } from 'react'
import { DetailContext } from './context'


export const useSubmissionFields = () => {

    const t = useT()
    const { check, is, reportOf } = useValidation()

    const logged = useTenancyOracle()
    const tenants = useTenantUtils()

    const { edited, trxPlausibility } = useContext(DetailContext)

    const self = {


        tenant: {

            label: t('sub.tenant'),
            msg: t('sub.tenant_msg'),
            help: t('sub.tenant_help'),


            ...check(is.empty).on(edited.tenant)
        }

        ,

        type: {

            label: t('sub.type'),
            msg: t('sub.type_msg'),
            help: t('sub.type_help'),

            ...check(is.notDefined).on(edited.type),
            ...check(is.true).provided(edited.type === trxauthzType).on(trxPlausibility || t("trxauthz.trx_implausible",{tenant:tenants.nameOf(edited.tenant)}))
        }

        ,

        resources: {

            label: t('sub.resources'),
            help: t('sub.resources_help'),

            ...check(is.empty).provided(!edited.live).on(edited.resources)
        }

        ,

        note: {

            label: t('sub.note_lbl'),
            msg: t('sub.note_msg'),
            help: t(`sub.note_help_${logged.hasNoTenant() ? '*' : 'tenant'}`)

        }
    }

    return reportOf(self)
}