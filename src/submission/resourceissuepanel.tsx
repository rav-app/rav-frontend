import { RecordInvalidIcon, RecordValidationIcon, RecordValidWithWaningIcon } from '#record/constants'
import { useRecordPluginDescriptors } from '#record/plugin'
import { useAppSettings } from '#settings/settings'
import { useT } from 'apprise-frontend-core/intl/language'
import { utils } from 'apprise-frontend-core/utils/common'
import { useTenantUtils } from 'apprise-frontend-iam/tenant/utils'
import { Bytestream } from 'apprise-frontend-streams/model'
import { Button } from 'apprise-ui/button/button'
import { FileDescriptorLabel } from 'apprise-ui/filebox/filebox'
import { Label } from 'apprise-ui/label/label'
import { useTableData } from 'apprise-ui/table/provider'
import { useSelection } from 'apprise-ui/table/selection'
import { Table } from 'apprise-ui/table/table'
import { useTableUtils } from 'apprise-ui/table/utils'
import { Tip } from 'apprise-ui/tooltip/tip'
import { useAsyncTask } from 'apprise-ui/utils/asynctask'
import { FileIcon, TextIcon } from 'apprise-ui/utils/icons'
import format from 'date-fns/format'
import { saveAs } from "file-saver"
import { useContext, useRef } from 'react'
import { BiLayer } from 'react-icons/bi'
import { DetailContext } from './context'
import './issuepanel.scss'
import { RecordIssue } from './validator'

// full resource, issue, rowid
type IssueRow = [Bytestream, RecordIssue, string]

export const ResourceIssues = () => {

    const t = useT()

    const { resourceIssueKit } = useContext(DetailContext)

    const { Drawer } = resourceIssueKit

    return <Drawer className='issuepanel' unmountOnClose renderOnMount={false} fixedHeight width={800} icon={<RecordValidationIcon />} title={t('sub.resissues_title')}>
        <Inner />
    </Drawer>

}

const Inner = () => {

    const t = useT()

    const { edited, resourceIssueKit } = useContext(DetailContext)

    const task = useAsyncTask()

    const { toggle } = resourceIssueKit

    const issuemap = edited.validationReport?.resources

    const tableName = 'resourceissues'

    const tabledata = useTableData<IssueRow>(tableName)

    const issueSelection = useSelection<IssueRow>()

    const resourcemap = utils().index(edited.resources).by(r => r.id)

    // we must keep table stable and a useRef will do as issues don't change while the drawer is open.
    const issues = useRef(

        Object.entries(issuemap ?? {}).filter(([id]) => resourcemap[id]).flatMap(([id, issues], id1) => issues.map((issue, id2) => [resourcemap[id], issue, `${id1}-${id2}`] as IssueRow))

    )


    const { Column, compareStringsOf } = useTableUtils<IssueRow>()

    const rowClassName = ([resource]: IssueRow) => { return `row-${(Object.keys(issuemap ?? {}).indexOf(resource.id)) % 2}` }

    const tenants = useTenantUtils()

    const formatString = useAppSettings().resolveFormat()

    const plugin = useRecordPluginDescriptors().lookup(edited.type)

    const downloadableIssues = issueSelection.isEmpty() ? tabledata.data : issueSelection.selected

    const download = task.make(async () => {

        const tenant = tenants.codeOf(edited.tenant)
        const filenameparts = [t(plugin.plural), format(new Date(edited.lifecycle.lastModified!), formatString)] as string[]

        if (tenant)
            filenameparts.unshift(tenant)

        saveAs(new Blob(

            [
                `${[t('sub.resissues_id'), t('sub.resissues_msg'), t('sub.resissues_type')].join(';')}\n`,
                ...downloadableIssues.map(([resource, issue]) => [resource.name, issue.message, issue.type].join(';')).join("\n")

            ],

            { type: 'text/csv' })

            , `${t('sub.resissues_filename', { name: filenameparts.join('-'), count: tabledata.data.length })}.csv`)


        toggle()
    })
        .with(task => task.wait(100).show(t("sub.resissues_downloading")))
        .done()

    const downloadBtn = <Button type='primary' onClick={download}>
        {issueSelection.isEmpty() ? t('sub.resissues_download') : t('sub.resissues_download_count', { count: downloadableIssues.length })}
    </Button>

    return <Table name={tableName} layout='fixed' rowClassName={rowClassName} data={issues.current} selection={issueSelection} rowId={([, , id]) => id}>

        <Table.Control>
            {downloadBtn}
        </Table.Control>

        <Column name='issue-type' width={120} title={<Label icon={<BiLayer />} title={t('sub.resissues_type')} />}
            text={([, { type }]) => t(`parse.${type}`)}
            sort={compareStringsOf(([, { type }]) => t(`ui.field_status_${type}`))}
            render={([, { type }]) => <Label icon={type === 'warning' ? <RecordValidWithWaningIcon /> : <RecordInvalidIcon />} mode='tag' title={t(`ui.field_status_${type}`)} />}
        />

        {Object.keys(resourcemap).length > 1 &&

            <Column name='issue-id' width={300} title={<Label icon={<FileIcon />} title={t('sub.resissues_id')} />}

                text={([resource]) => resource.name}
                sort={compareStringsOf(([resource]) => resource.name)}
                render={([resource]) => <FileDescriptorLabel descriptor={resource} />}
            />
        }

        <Column name='issue-msg' title={<Label icon={<TextIcon />} title={t('sub.resissues_msg')} />}
            text={([, issue]) => issue.message}
            sort={compareStringsOf(([, issue]) => issue.message)}
            render={([, issue]) => <Tip tip={issue.message}>{issue.message}</Tip>}
        />



    </Table>
}