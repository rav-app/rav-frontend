
import { useRecordPluginDescriptors } from '#record/plugin'
import { useVesselOracle } from '#vessel/oracle'
import { useVesselRouting } from '#vessel/routing'
import { useMode } from 'apprise-frontend-core/config/api'
import { useT } from 'apprise-frontend-core/intl/language'
import { Button } from 'apprise-ui/button/button'
import { ButtonMenu } from 'apprise-ui/button/buttonmenu'
import { Page } from 'apprise-ui/page/page'
import { SidebarContent } from 'apprise-ui/page/sidebar'
import { useSelection } from 'apprise-ui/table/selection'
import { SortSpec } from 'apprise-ui/table/sorting'
import { Table } from 'apprise-ui/table/table'
import { Titlebar } from 'apprise-ui/titlebar/titlebar'
import { Topbar } from 'apprise-ui/topbar/topbar'
import { useResetFilters } from 'apprise-ui/utils/filter'
import { AddIcon, DevOnlyIcon } from 'apprise-ui/utils/icons'
import { useEffect } from 'react'
import { useSubmissionColumns } from './columns'
import { submissionType } from './constants'
import { usePartyFilter, useStateFilter, useTypeFilter, useValidationFilter, useYearFilter } from './filter'
import { Submission } from './model'
import { useSubmissionOracle } from './oracle'
import { useSubmissionRouting } from './routing'
import { useSubmissionStore } from './store'



export const SubmissionList = () => {

    const t = useT()
    const mode = useMode()

    const store = useSubmissionStore()
    const routing = useSubmissionRouting()
    const columns = useSubmissionColumns()
    const plugins = useRecordPluginDescriptors()
    const oracle = useSubmissionOracle()

    const vessels = { ...useVesselRouting(), ...useVesselOracle() }

    const submissions = store.all()

    useEffect(() => {

        if (!submissions)
            store.fetchAll()

        // eslint-disable-next-line
    }, [submissions])


    // filters

    const filterContext = submissionType

    useResetFilters(filterContext, submissions)

    const unfilteredData = submissions ?? []

    const { ValidationFilter, filteredData: validationFiltered } = useValidationFilter(
        { data: unfilteredData, context: filterContext }
    )

    const { TypeFilter, filteredData: typeFilteredData } = useTypeFilter(
        { data: validationFiltered, context: filterContext }
    )

    const { StateFilter, filteredData: stateFilteredData } = useStateFilter(
        { data: typeFilteredData, initial: unfilteredData, context: filterContext }
    )

    const { PartyFilter, filteredData: partyFilteredData } = usePartyFilter(
        { data: stateFilteredData, initial: unfilteredData, context: filterContext }
    )

    const { YearFilter, filteredData: yearFilteredData } = useYearFilter(
        { data: partyFilteredData, initial: unfilteredData, context: filterContext }
    )


    const filteredData = yearFilteredData

    // actions

    const selection = useSelection<Submission>()

    const add = () => routing.routeToNewDetail()
    const removeMany = () => store.removeMany(selection.selected.map(s => s.id), selection.clear)

    // buttons

    const addBtn = < Button type="primary" icon={<AddIcon />} enabled={oracle.canAddNew()} onClick={add}>
        {t("sub.add", { singular: t('sub.singular') })}
    </ Button>

    const addVesselBtn = <Button enabled={vessels.canAddNew()} icon={<AddIcon />} onClick={vessels.routeToNew}>
        {t('vessel.add_btn')}
    </Button>

    const removeManyBtn = <Button iconPlacement='left' type="danger" enabled={oracle.canRemove()} icon={<DevOnlyIcon />} onClick={removeMany} >
        {t("ui.feedback.consent_removal_many_title", { count: selection.selected.length })}
    </Button>

    const initialSort: SortSpec[] = [{ key: 'created', mode: 'desc' }]

    return <Page>

        <Titlebar title={t('sub.list_title')} />

        <SidebarContent>

            {addBtn}
            {addVesselBtn}

            <br />

            {mode.development && selection.selected.length > 0 && removeManyBtn}

        </SidebarContent>

        <Topbar>
            <ButtonMenu>
                {addBtn}
                {addVesselBtn}
            </ButtonMenu>
        </Topbar>

        <Table name={submissionType}
            filterProps={{ filterWhat: (s) => t(plugins.lookup(s.type).name) }}
            context={filterContext} data={filteredData} total={unfilteredData.length}
            initialSort={initialSort}
            selection={mode.development ? selection : undefined}>

            <Table.Filter >
                {ValidationFilter}
            </Table.Filter>

            <Table.Filter name="party">
                {PartyFilter}
            </Table.Filter>

            <Table.Filter name="type">
                {TypeFilter}
            </Table.Filter>

            <Table.Filter name="state">
                {StateFilter}
            </Table.Filter>

            <Table.Filter name="year">
                {YearFilter}
            </Table.Filter>

            {columns}

        </Table>

    </Page>
}