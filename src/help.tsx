
import { appColor } from '#constants';
import { useT } from "apprise-frontend-core/intl/language";
import { MailIcon } from 'apprise-frontend-mail/constants';
import { Button } from "apprise-ui/button/button";
import { useRoutableDrawer } from "apprise-ui/drawer/drawer";
import { Link } from "apprise-ui/link/link";
import { Explainer, ExplainerPanel } from "apprise-ui/utils/explainer";
import { HelpIcon } from 'apprise-ui/utils/icons';
import { GoLinkExternal } from "react-icons/go";


export const useHelpDrawer = () => {
    
    const t = useT()
    
    const { Drawer, routeAt } = useRoutableDrawer('help');


    return <>

        <Link linkTo={routeAt()}>
            <HelpIcon color={appColor} />
        </Link>

        <Drawer noReadonly width={600} title={t('help.title')} icon={<HelpIcon />}>

            <HelperPanel />

        </Drawer>
    </>
}

const HelperPanel = () => {

    const t = useT()

    return <ExplainerPanel >


        <Explainer icon={<HelpIcon />}>
            {t('help.explainer')}
        </Explainer>


        <Explainer framed title={t("help.guides.title")} action={
            <Button iconPlacement='left' icon={<GoLinkExternal />} type='primary'>
                <a href={t("help.guides.action")} target="_blank" rel="noreferrer">{t("help.guides.action_title")}</a> 
            </Button>
        }>
            {t("help.guides.explainer")}
        </Explainer>


        <Explainer framed title={t("help.general.title")} action={
            <Button iconPlacement='left' icon={<MailIcon />} type='primary'>
                <a href={t("help.general.action")}  target="_blank" rel="noreferrer">{t("help.general.action_title")}</a> 
            </Button>
        }>
            {t("help.general.explainer")}
        </Explainer>


        <Explainer framed title={t("help.technical.title")} action={
            <Button iconPlacement='left' icon={<MailIcon />} type='primary'>
                <a href={t("help.technical.action")} target="_blank" rel="noreferrer">{t("help.technical.action_title")}</a> 
            </Button>
        }>
            {t("help.technical.explainer")}
        </Explainer>


    </ExplainerPanel>
}