import { useMocks } from 'apprise-frontend-core/client/mocks'
import getYear from 'date-fns/getYear'
import { Authorization } from './model'
import { utils } from 'apprise-frontend-core/utils/common'

export const useAuthorizationMocks = () => {

    const mocks = useMocks()

    const self = {

        store: () => mocks.getOrCreateStore<Authorization>('authorizations', { id: m => m.id })

        ,

        mockSlot: (props: { current?: Authorization, timestamp: string}): Authorization => {

            const { current, timestamp } = props

            return {

                ...current,

                id: utils().mint('AZ'),
                timestamp,

                from: timestamp,
                to:  new Date( getYear(new Date(timestamp)), 11, 31).toISOString(),
             
              
            }

        }


    }

    return self
}