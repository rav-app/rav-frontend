import { SlotType } from '#record/model'
import { BsShieldCheck } from 'react-icons/bs'



export const authorizationType = 'authorization'

export const patchedTypes : SlotType[]  = [authorizationType]

export const AuthorizationIcon = BsShieldCheck