import { searchApi } from '#record/calls';
import { defaultPageSize } from '#search/constants';
import { SearchDto } from '#search/model';
import { submissionApi } from '#submission/calls';
import { Preload, Preloader } from 'apprise-frontend-core/client/preload';
import { useLanguage } from 'apprise-frontend-core/intl/language';
import { useIamRefPreloaders } from 'apprise-frontend-iam/preloaders';
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant';
import { PropsWithChildren } from 'react';



export const RavPreloader = (props: PropsWithChildren) => {

    const { children } = props

    const lang = useLanguage()

    const logged = useTenancyOracle()

    const otherloaders = [...useIamRefPreloaders()]

    const preloadQuery: Partial<SearchDto> = { language: lang.current(), sort: [{field:'timestamp', mode: 'desc'}], cursor: { page: 1, pageSize: defaultPageSize } }

    const loaders: Preloader[] = []

    loaders.push({ name: searchApi, task: call => call.at(searchApi).post(preloadQuery) })

    if (!logged.isGuest())
        loaders.push({ name: submissionApi, task: call => call.at(submissionApi).get() })

    if (logged.isGuest() || logged.isTenantUser())
        otherloaders.forEach(loader => loaders.push(loader))

    return <Preload loaders={loaders}>
        {children}
    </Preload>

}