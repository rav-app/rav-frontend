import { Vid } from './model';




export type VidExtension = {


    mintPrimaryUvi: () => Vid

    isValidPrimaryUvi: (uvi: string) => boolean

    postProcessValue: (value?:string) => string | undefined

    formatUvi: (value: string) => string

}
