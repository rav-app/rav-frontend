import { useT } from 'apprise-frontend-core/intl/language'
import { useFieldProps } from 'apprise-ui/field/field'
import { GroupBox, GroupBoxProps } from 'apprise-ui/groupbox/groupbox'
import * as React from 'react'
import { useVID, Vid } from './model'
import { VidBox } from './vidbox'

export type VidGroupBoxProps = GroupBoxProps<Vid>

export const VidGroupBox = (props: VidGroupBoxProps) => {

    const t = useT()
    const vids = useVID()


    const { children = [], onChange, name = t('vid.name'), ...rest } = useFieldProps(props)

    const currentSchemes = children.map(c => vids.parse(c).scheme)
    const schemes = vids.allSchemes().map(s => s.id).filter(s => !currentSchemes.includes(s))

    const currentValues = children.map(c => vids.parse(c).value)
    const completeValues = currentValues.length === 0 || !currentValues.some(v => !v)

    const schemesLeft = schemes.length > 0
    const newScheme = () => schemes[0]

    const render = (vid, index) => <VidBox schemes={schemes} onChange={v => onChange?.(children.map((c, i) => i === index ? v! : c))}>
        {vid}
    </VidBox>

    const info = completeValues ? undefined : { label: props.label ?? props.info?.label, status: 'error' as const, msg: t('vid.some_incomplete') }

    return <GroupBox render={render} name={name} newItem={newScheme} addIf={schemesLeft && completeValues} onChange={onChange} {...rest}

        {...info}  // must follow spread or will be overridden. 
    >
        {children}
    </GroupBox>
}