

import { TestMailButton } from '#utils/testmailbutton';
import { UpgradeButton } from '#utils/upgradebutton';
import 'antd/dist/antd.css';
import { MockHorizonContext } from 'apprise-frontend-core/client/mockhorizon';
import { useRecordButton } from 'apprise-frontend-core/client/session';
import { useMode } from 'apprise-frontend-core/config/api';
import { EventBusConnectionLabel } from 'apprise-frontend-events/label';
import { useTenancyOracle } from 'apprise-frontend-iam/authz/tenant';
import { ProfilePanel } from 'apprise-frontend-iam/user/profilesummary';
import { useUserUtils } from 'apprise-frontend-iam/user/utils';
import { TestMailSection } from 'apprise-frontend-mail/testmail';
import { useSettingsPanel } from 'apprise-frontend-streams/settings/panel';
import { Breadcrumbs } from 'apprise-ui/breadcrumb/breadcrumbs';
import { FocusModeButton } from 'apprise-ui/focusmode/button';
import { ActionMenu } from 'apprise-ui/header/actionmenu';
import { Header } from 'apprise-ui/header/header';
import { useContext } from 'react';


export const RavHeader = () => {

    const settingsPanel = useSettingsPanel({ width: 750 })
    const { logoutMenuItem, userBadge } = useUserUtils()
    const recordBtn = useRecordButton()
    const logged = useTenancyOracle()
    const mode = useMode()


    const belowMockHorizon = useContext(MockHorizonContext)

    return <Header>

        <Header.Left>
            <Breadcrumbs />
        </Header.Left>

        <Header.Right>
            <UpgradeButton  />
        </Header.Right>

        <Header.Right>
            <EventBusConnectionLabel disconnectedOnly />
        </Header.Right>

        <Header.Right>
            <FocusModeButton />
        </Header.Right>

        {mode.production ||

            <Header.Right>
                <TestMailButton />
            </Header.Right>
        }

        {belowMockHorizon &&

            <Header.Right>
                {recordBtn}
            </Header.Right>
        }

        <Header.Right>
            {userBadge}
        </Header.Right>

        <Header.Right>
            <ActionMenu>
                <ProfilePanel>
                    <TestMailSection />
                </ProfilePanel>
                {logged.hasNoTenant() && settingsPanel}
                {logoutMenuItem}
            </ActionMenu>
        </Header.Right>

    </Header>

}