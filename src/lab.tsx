import { useRavTags } from '#stage/tags'
import { useVID, Vid } from '#vid/model'
import { VidBox } from '#vid/vidbox'
import { VidGroupBox } from '#vid/vidgroupbox'
import { TagBoxes } from 'apprise-frontend-tags/tag/tagboxes'
import { Form } from 'apprise-ui/form/form'
import { Lab, LabProvider, LabRow } from 'apprise-ui/lab/lab'
import { Page } from 'apprise-ui/page/page'
import { Tab, useTabs } from 'apprise-ui/tabs/tab'
import { Topbar } from 'apprise-ui/topbar/topbar'



export const RavLab = () => {

    const { tabs, selectedTabContent } = useTabs([

        <Tab label='Tags'>
            <TagLab />
        </Tab>,

        <Tab label='Labels'>

        </Tab>
    ])


    return <Page>

        <Topbar tabs={tabs} />

        {selectedTabContent}


    </Page>

}

const TagLab = () => {

    const vids = useVID()
    const { categories, tags } = useRavTags()

    return <Lab>
        <LabProvider<Vid> initial={undefined}>{state => <>
            <LabRow label='primary UVI'>
                <Form>
                    <VidBox debug canUnlock defaultValue={vids.stringify(tags.iotcnumber.id,"12345678")} readonly  trackChange  schemes={tags.iotcnumber.id} onChange={state.reset}>
                        {state.get()}
                    </VidBox>
                </Form>
            </LabRow>
            {state.summaryRow}
        </>}
        </LabProvider>

        <LabProvider<Vid> initial={undefined}>{state => <>
            <LabRow label='UVI'>
                <Form>
                    <VidBox onChange={state.reset}>
                        {state.get()}
                    </VidBox>
                    <VidBox enabled={false} label={tags.iotcnumber.name.en} onChange={state.reset}>
                        {state.get()}
                    </VidBox>
                    <VidBox readonly label={tags.iotcnumber.name.en} onChange={state.reset}>
                        {state.get()}
                    </VidBox>
                </Form>
            </LabRow>
            {state.summaryRow}
        </>}
        </LabProvider>

        <LabProvider<Vid[]> initial={[]}>{state => <>
            <LabRow label='VID Group'>
                <Form>
                    <VidGroupBox label="Other Identifiers" msg="Select identifiers." onChange={state.reset}>
                        {state.get()}
                    </VidGroupBox>
                </Form>
            </LabRow>
            {state.summaryRow}
        </>}
        </LabProvider>

        <LabProvider<string[]> initial={[]}>{state =>
            <LabRow label='Kind & Type & Gear'>
                <Form>
                    <TagBoxes categories={[categories.kind, categories.type, categories.gear]} onChange={state.reset}>
                        {state.get()}
                    </TagBoxes>
                </Form>

            </LabRow>}
        </LabProvider>

    </Lab>
}
